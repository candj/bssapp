package com.lf.bigdata.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import com.lf.bigdata.dao.mapper.AppUserDao;
import com.lf.bigdata.dao.mapper.UserRoleDao;
import com.lf.bigdata.dto.AppUserDto;
import com.lf.bigdata.mapper.model.AppUser;
import com.lf.bigdata.mapper.model.UserRole;

/**
 * ユーザー情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class AppUserService {
    @Autowired
    AppUserDao appUserDao;

    @Autowired
    UserRoleDao userRoleDao;

    @Autowired
    Mapper mapper;

    /**
     * 条件でユーザーを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectAllByExample(AppUserDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("app_user.user_id", dto.getUserId())
            .andEqualTo("app_user.user_name", dto.getUserName())
            .andEqualTo("app_user.user_code", dto.getUserCode())
            .andEqualTo("app_user.password", dto.getPassword())
            .andEqualTo("app_user.status", dto.getStatus())
            .andEqualTo("app_user.create_user", dto.getCreateUser())
            .andEqualTo("app_user.update_user", dto.getUpdateUser())
            .andEqualTo("app_user.create_time", dto.getCreateTime())
            .andEqualTo("app_user.update_time", dto.getUpdateTime());
        }
        //条件でユーザーを検索する。（連携情報含む）)
        List<AppUser> ret = appUserDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, AppUserDto.class);
    }

    /**
     * 条件でユーザーを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectByExample(AppUserDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("app_user.user_id", dto.getUserId())
            .andEqualTo("app_user.user_name", dto.getUserName())
            .andEqualTo("app_user.user_code", dto.getUserCode())
            .andEqualTo("app_user.password", dto.getPassword())
            .andEqualTo("app_user.status", dto.getStatus())
            .andEqualTo("app_user.create_user", dto.getCreateUser())
            .andEqualTo("app_user.update_user", dto.getUpdateUser())
            .andEqualTo("app_user.create_time", dto.getCreateTime())
            .andEqualTo("app_user.update_time", dto.getUpdateTime());
        }
        //条件でユーザーを検索する。（連携情報含む）)
        List<AppUser> ret = appUserDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, AppUserDto.class);
    }

    /**
     * プライマリーキーでユーザーを検索する。
     * @param userId ユーザーID
     * @return 結果
     */
    public AppUserDto selectByPrimaryKey(Integer userId) {
        //プライマリーキーでユーザーを検索する。
        AppUser ret = appUserDao.selectByPrimaryKey(userId);
        return DozerHelper.map(mapper, ret, AppUserDto.class);
    }

    /**
     * プライマリーキーでユーザーを検索する。（連携情報含む）
     * @param userId ユーザーID
     * @return 結果
     */
    public List<AppUserDto> selectAllByPrimaryKey(Integer userId) {
        //プライマリーキーでユーザー検索する。（連携情報含む）
        List<AppUser> ret = appUserDao.selectAllByPrimaryKey(userId);
        return DozerHelper.mapList(mapper, ret, AppUserDto.class);
    }

    /**
     * ユーザーを新規追加する。
     * @param dto ユーザー
     * @return 結果
     */
    public int insertSelective(AppUserDto dto) {
        AppUser appUser  =  mapper.map(dto, AppUser.class);
        //ユーザーを新規追加する。
        int ret = appUserDao.insertSelective(appUser);

        WhereCondition whereCondition= new WhereCondition() ;
        whereCondition.createCriteria()
        .andEqualTo("app_user.user_code", dto.getUserCode());
        //条件でユーザーを検索する。（連携情報含む）)
        appUserDao.selectByExample(whereCondition).stream().findFirst().ifPresent(u -> {
            UserRole record = new UserRole();
            record.setUserId(u.getUserId());
            record.setRoleId(1);
            userRoleDao.insertSelective(record);
        });

        return ret;
    }

    /**
     * ユーザーを新規追加する。
     * @param dto ユーザー
     * @return 結果
     */
    public int insert(AppUserDto dto) {
        AppUser appUser  =  mapper.map(dto, AppUser.class);
        //ユーザーを新規追加する。
        int ret = appUserDao.insert(appUser);
        return ret;
    }

    /**
     * プライマリーキーでユーザーを更新する。
     * @param dto ユーザー
     * @return 結果
     */
    public int updateByPrimaryKey(AppUserDto dto) {
        AppUser appUser  =  mapper.map(dto, AppUser.class);
        //プライマリーキーでユーザーを更新する。
        int ret = appUserDao.updateByPrimaryKey(appUser);
        return ret;
    }

    /**
     * プライマリーキーでユーザーを更新する。
     * @param dto ユーザー
     * @return 結果
     */
    public int updateByPrimaryKeySelective(AppUserDto dto) {
        AppUser appUser  =  mapper.map(dto, AppUser.class);
        //プライマリーキーでユーザーを更新する。
        int ret = appUserDao.updateByPrimaryKeySelective(appUser);
        return ret;
    }

    /**
     * ユーザーを削除する。
     * @param userId ユーザーID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer userId) {

    	WhereCondition whereCondition= new WhereCondition() ;
        whereCondition.createCriteria()
        .andEqualTo("user_id", userId);
        userRoleDao.deleteByExample(whereCondition);


        //ユーザーを削除する。
        int ret = appUserDao.deleteByPrimaryKey(userId);
        return ret;
    }
}