package com.lf.bigdata.service;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import com.lf.bigdata.dao.mapper.NotificationDao;
import com.lf.bigdata.dto.NotificationDto;
import com.lf.bigdata.mapper.model.Notification;

/**
 * 掲示板情報情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class NotificationService {
	@Autowired
	NotificationDao notificationDao;

	@Autowired
	Mapper mapper;

	/**
	 * プライマリーキーで掲示板情報を検索する。
	 * @param notificationId ID
	 * @return 結果
	 */
	public Notification selectByPrimaryKey(Integer notificationId) {
		//プライマリーキーで掲示板情報を検索する。
		Notification ret = notificationDao.selectByPrimaryKey(notificationId);
		return ret;

	}

	/**
	 * プライマリーキーで掲示板情報を検索する。（連携情報含む）
	 * @param notificationId ID
	 * @return 結果
	 */
	public List<NotificationDto> selectAllByPrimaryKey(Integer notificationId) {
		//プライマリーキーで掲示板情報検索する。（連携情報含む）
		List<Notification> ret = notificationDao.selectAllByPrimaryKey(notificationId);
		return DozerHelper.mapList(mapper, ret, NotificationDto.class);
	}

	/**
	 * 条件で掲示板情報を検索する。（連携情報含む）
	 * @param dto 検索条件
	 * @return 結果
	 */
	public List<NotificationDto> selectByExample(NotificationDto dto) {
		WhereCondition whereCondition = new WhereCondition();
		if (dto != null) {
			whereCondition.createCriteria()
					.andEqualTo("notification.notification_id", dto.getNotificationId())
					.andEqualTo("notification.notification_title", dto.getNotificationTitle())
					.andEqualTo("notification.notification_process_id", dto.getNotificationProcessId())
					.andEqualTo("notification.notification_status", dto.getNotificationStatus())
					.andEqualTo("notification.notification_open_time", dto.getNotificationOpenTime())
					.andEqualTo("notification.create_user", dto.getCreateUser())
					.andEqualTo("notification.update_user", dto.getUpdateUser())
					.andEqualTo("notification.create_time", dto.getCreateTime())
					.andEqualTo("notification.update_time", dto.getUpdateTime())
					.andEqualTo("notification.notification_content", dto.getNotificationContent());
		}

		whereCondition.setOrderByClause("notification.notification_status desc, notification.notification_open_time desc");

		//条件で掲示板情報を検索する。（連携情報含む）)
		List<Notification> ret = notificationDao.selectByExample(whereCondition);
		DateTime threeDayBefore = DateTime.now().minusDays(3).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);


		ret.stream().filter(rec -> Boolean.TRUE.equals(rec.getNotificationStatus())
				&& !rec.getNotificationOpenTime().isBefore(threeDayBefore)).forEach(rec -> {
					rec.setOpenedRecently(true);
				});

//		ret.forEach(n -> {
//			DateTime openTime = n.getNotificationOpenTime();
//			DateTime updateTime = n.getUpdateTime();
//			if (openTime.isBefore(threeDayBefore)) {
//				if (updateTime.isAfter(threeDayBefore) && n.getNotificationStatus() == true) {
//					n.setOpenedRecently(true);
//				} else {
//					n.setOpenedRecently(false);
//				}
//			} else {
//				n.setOpenedRecently(true);
//			}
//		});
		return DozerHelper.mapList(mapper, ret, NotificationDto.class);
	}

	/**
	 * updateTimeで掲示板情報を検索する。（連携情報含む）
	 * @return 結果
	 */
	public List<NotificationDto> selectByUpdateTime() {
		WhereCondition whereCondition = new WhereCondition();
		whereCondition.setOrderByClause("notification.update_time desc");
		//条件で掲示板情報を検索する。（連携情報含む）)
		List<Notification> ret = notificationDao.selectAllByExample(whereCondition);
		return DozerHelper.mapList(mapper, ret, NotificationDto.class);
	}

	/**
	 * updateTimeで掲示板情報を検索する。（連携情報含む）
	 * @return 結果
	 */
	public List<NotificationDto> selectByTime() {
		WhereCondition whereCondition = new WhereCondition();
		whereCondition.setOrderByClause("notification.update_time desc");
		//条件で掲示板情報を検索する。（連携情報含む）)
		List<Notification> ret = notificationDao.selectAllByExample(whereCondition);
		return DozerHelper.mapList(mapper, ret, NotificationDto.class);
	}

	/**
	 * 条件で掲示板情報を検索する。（連携情報含む）
	 * @param dto 検索条件
	 * @return 結果
	 */
	public List<NotificationDto> selectAllByExample(NotificationDto dto) {
		WhereCondition whereCondition = new WhereCondition();
		if (dto != null) {
			whereCondition.createCriteria()
					.andEqualTo("notification.notification_id", dto.getNotificationId())
					.andEqualTo("notification.notification_title", dto.getNotificationTitle())
					.andEqualTo("notification.notification_process_id", dto.getNotificationProcessId())
					.andEqualTo("notification.notification_status", dto.getNotificationStatus())
					.andEqualTo("notification.notification_open_time", dto.getNotificationOpenTime())
					.andEqualTo("notification.create_user", dto.getCreateUser())
					.andEqualTo("notification.update_user", dto.getUpdateUser())
					.andEqualTo("notification.create_time", dto.getCreateTime())
					.andEqualTo("notification.update_time", dto.getUpdateTime())
					.andEqualTo("notification.notification_content", dto.getNotificationContent());
		}
		whereCondition.setOrderByClause("notification.notification_status desc, notification.update_time desc");
		//条件で掲示板情報を検索する。（連携情報含む）)
		List<Notification> ret = notificationDao.selectAllByExample(whereCondition);
		DateTime threeDayBefore = new DateTime().minusDays(3);
		ret.forEach(n -> {
			DateTime openTime = n.getNotificationOpenTime();
			DateTime updateTime = n.getUpdateTime();
			if (openTime.isBefore(threeDayBefore)) {
				if (updateTime.isAfter(threeDayBefore) && n.getNotificationStatus() == true) {
					n.setOpenedRecently(true);
				} else {
					n.setOpenedRecently(false);
				}
			} else {
				n.setOpenedRecently(true);
			}
		});
		return DozerHelper.mapList(mapper, ret, NotificationDto.class);
	}

	/**
	 * 掲示板情報を新規追加する。
	 * @param dto 掲示板情報
	 * @return 結果
	 */
	public int insertSelective(NotificationDto dto) {
		Notification notification = mapper.map(dto, Notification.class);
		//掲示板情報を新規追加する。
		int ret = notificationDao.insertSelective(notification);
		return ret;
	}

	/**
	 * 掲示板情報を新規追加する。
	 * @param dto 掲示板情報
	 * @return 結果
	 */
	public int insert(NotificationDto dto) {
		Notification notification = mapper.map(dto, Notification.class);
		//掲示板情報を新規追加する。
		int ret = notificationDao.insert(notification);
		return ret;
	}

	/**
	 * プライマリーキーで掲示板情報を更新する。
	 * @param dto 掲示板情報
	 * @return 結果
	 */
	public int updateByPrimaryKey(NotificationDto dto) {
		Notification notification = mapper.map(dto, Notification.class);
		//プライマリーキーで掲示板情報を更新する。
		int ret = notificationDao.updateByPrimaryKey(notification);
		return ret;
	}

	/**
	 * プライマリーキーで掲示板情報を更新する。
	 * @param dto 掲示板情報
	 * @return 結果
	 */
	public int updateByPrimaryKeySelective(NotificationDto dto) {
		Notification notification = mapper.map(dto, Notification.class);
		//プライマリーキーで掲示板情報を更新する。
		int ret = notificationDao.updateByPrimaryKeySelective(notification);
		return ret;
	}

	/**
	 * 掲示板情報を削除する。
	 * @param notificationId ID
	 * @return 結果
	 */
	public int deleteByPrimaryKey(Integer notificationId) {
		//掲示板情報を削除する。
		int ret = notificationDao.deleteByPrimaryKey(notificationId);
		return ret;
	}
}
