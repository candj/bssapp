package com.lf.bigdata.service;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.lf.bigdata.batch.businessHourTask;
import com.lf.bigdata.dao.mapper.BusinessAttendanceDao;
import com.lf.bigdata.dao.mapper.BusinessTaskDao;
import com.lf.bigdata.dto.WorkTaskDto;
import com.lf.bigdata.dto.report.ReportBusinessDto;
import com.lf.bigdata.dto.report.ReportWorkHourDto;
import com.lf.bigdata.mapper.model.BusinessTask;
import com.lf.bigdata.mapper.model.WorkDay;
import com.lf.bigdata.mapper.model.WorkTask;
import com.lf.bigdata.utils.JxlsUtils;
import com.lf.bigdata.utils.ReportUtil;
import com.lf.bigdata.utils.ServiceUtil;

/**
 * ファイル出力するクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
public class ReportService {

	private static final String WORK_TEMPLATE = "template/workHourTemplate.xlsx";

	@Autowired
	BusinessTaskDao businessTaskDao;

	@Autowired
	BusinessAttendanceDao businessAttendanceDao;

	@Autowired
	ApplicationContext ac;

	private static final Logger logger = LoggerFactory.getLogger(businessHourTask.class);

	public void outputBusinessDayHourFile(WorkTaskDto dto, HttpServletResponse response, String fileName)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IOException {

		ReportUtil.outputExcel(ac, WORK_TEMPLATE, response, fileName, buildModel(dto));
	}

	public void outputBusinessDayHourFile(WorkTaskDto dto, OutputStream os)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IOException {

		ReportUtil.outputExcel(ac, WORK_TEMPLATE, os, buildModel(dto));
	}

	private Map<String, Object> buildModel(WorkTaskDto dto)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		SimpleDateFormat dfForSheet = new SimpleDateFormat("MMdd");
		SimpleDateFormat dfForDate = new SimpleDateFormat("yyyy/M/dd");
		Map<String, Object> model = new HashMap<String, Object>();
		List<Map<String, Object>> days = new ArrayList<Map<String, Object>>();
		DateTime workEndDate = dto.getWorkEndTime().withHourOfDay(7);
		List<String> sheetNames1 = new ArrayList<String>();
		List<String> sheetNames2 = new ArrayList<String>();
		for(; !dto.getWorkStartTime().isAfter(workEndDate); dto.setWorkStartTime(dto.getWorkStartTime().withFieldAdded(DurationFieldType.days(), 1))) {
			String workDateForSheet = dfForSheet.format(dto.getWorkStartTime().toDate());
			String Date = dfForDate.format(dto.getWorkStartTime().toDate());
			String nextDate = dfForDate.format(dto.getWorkStartTime().withFieldAdded(DurationFieldType.days(), 1).toDate());

			WorkDay wd = ServiceUtil.getWorkDayForReportService(dto.getWorkStartTime());

			dto.setWorkStartTime(wd.getWorkStartTime());
			dto.setWorkEndTime(wd.getWorkEndTime());

			List<BusinessTask> businessTasks = businessTaskDao.selectAllByTime(wd.getWorkStartTime(),
					wd.getWorkEndTime()/**, whereCondition*/
			);

			processBusinessData(businessTasks, wd.getWorkStartTime(), wd.getWorkEndTime());

			// daylyReportDetail
			List<ReportBusinessDto> daylyReportdetailList = daylyReportDetail(businessTasks, wd);

			// daylyrepoRtList
			List<ReportBusinessDto> businessesList = daylyReport(daylyReportdetailList);

			// 合計
			List<ReportWorkHourDto> list = getTotalData(businessesList);
			ReportWorkHourDto parttimeHour = list.get(0);
			ReportWorkHourDto temporaryHour = list.get(1);
			ReportWorkHourDto totalHour = list.get(2);

			businessesList.sort(new Comparator<ReportBusinessDto>() {

				@Override
				public int compare(ReportBusinessDto o1, ReportBusinessDto o2) {
					return o1.getMark().compareTo(o2.getMark());
				}

			});

			Map<String, Object> perDay = new HashMap<String, Object>();
			perDay.put("daylyReportdetailList", daylyReportdetailList);
			perDay.put("businesses", businessesList);
			perDay.put("parttimeHour", parttimeHour);
			perDay.put("temporaryHour", temporaryHour);
			perDay.put("totalHour", totalHour);
			perDay.put("workDate", workDateForSheet);
			perDay.put("date", Date);
			perDay.put("nextDate", nextDate);
			days.add(perDay);
			sheetNames1.add(new String(workDateForSheet + " Detail"));
			sheetNames2.add(new String(workDateForSheet + " 合計"));
		}
		model.put("days", days);
		model.put("sheetNames1", sheetNames1);
		model.put("sheetNames2", sheetNames2);

		return model;
	}

	
	// ここからdaylyReportDetailシート出力処理
	/**
	 * daylyReportRetailの処理
	 *
	 * @param businessTasks
	 * @return
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	private List<ReportBusinessDto> daylyReportDetail(List<BusinessTask> businessTasks, WorkDay reportDay)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		List<ReportBusinessDto> reportBusinessDtoList = new ArrayList<>();
		for (BusinessTask b : businessTasks) {
			ReportBusinessDto reportBusinessDto = new ReportBusinessDto();
			// マークと業務名の処理
			String mark = b.getMark();

			// 業務名を取得する
			String workName = b.getBusinessName();
			String workNameCn = b.getBusinessNameCn();

			// モードを取得
			String chargeType = b.getChargeType();

			reportBusinessDto.setMark(mark);
			reportBusinessDto.setWorkName(workName);
			reportBusinessDto.setWorkNameCn(workNameCn);
			reportBusinessDto.setChargeType(chargeType);
			reportBusinessDtoList.add(reportBusinessDto);

			reportBusinessDto.setAreaName(b.getArea());
			reportBusinessDto.setProcess(b.getProcess());
			// LFバイト
			ReportWorkHourDto lfWorkHourDto = new ReportWorkHourDto();
			lfWorkHourDto.setDayTotal(0);
			lfWorkHourDto.setNightTotal(0);
			reportBusinessDto.setParttimeHourDto(lfWorkHourDto);
			// 派遣
			ReportWorkHourDto dispatchWorkHourDto = new ReportWorkHourDto();
			dispatchWorkHourDto.setDayTotal(0);
			dispatchWorkHourDto.setNightTotal(0);
			reportBusinessDto.setTemporaryHourDto(dispatchWorkHourDto);

			if (CollectionUtils.isNotEmpty(b.getWorkTask())) {
				for (WorkTask w : b.getWorkTask()) {

					if (w.getWorkStartTime() == null || w.getWorkEndTime() == null) {
						logger.error("workId : " + w.getWorkId() + " staffId : " + w.getWorkStaffId() + " business : "
								+ w.getWorkBusinessId() + " の開始時間または終了時間は設定されていない。");
						continue;
					}

					// 作業時間の割当
					if (Objects.equals(w.getStaff().getStaffWorkType(), "parttime")) {
						// LFバイト 時間を計算します
						setWorkMinuteByHour(w.getWorkStartTime(), w.getWorkEndTime(), lfWorkHourDto, reportDay);

					} else {
						// 派遣時間を計算します
						setWorkMinuteByHour(w.getWorkStartTime(), w.getWorkEndTime(), dispatchWorkHourDto, reportDay);
					}
				}
			}

		}

		for (ReportBusinessDto rDto : reportBusinessDtoList) {
			convertToHour(rDto.getParttimeHourDto());
			convertToHour(rDto.getTemporaryHourDto());
		}

		return reportBusinessDtoList;
	}

	// ここからdaylyReportDetailシート出力処理
	/**
	 * daylyReportの処理
	 *
	 * @param businesses
	 * @return
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	private List<ReportBusinessDto> daylyReport(List<ReportBusinessDto> businesses)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		List<ReportBusinessDto> hasWorksBusinesses = new ArrayList<ReportBusinessDto>();
		for (ReportBusinessDto b : businesses) {
			ReportBusinessDto rb = null;
			// トイレ時間をDに
			boolean isZMark = false;
			if (b.getMark().equals("Z")) {
				b.setMark("D");
				isZMark = true;
			}
			rb = hasWorksBusinesses.stream().filter(p -> b.getMark().equals(p.getMark())).findFirst().orElse(rb);
			if (rb == null) {
				rb = new ReportBusinessDto();
				rb.setMark(b.getMark());
				rb.setWorkName(b.getWorkName());

				rb.setAreaName(b.getAreaName());
				rb.setProcess(b.getProcess());

				hasWorksBusinesses.add(rb);
				// LFバイト
				ReportWorkHourDto d1 = new ReportWorkHourDto();
				d1.setDayTotal(0);
				d1.setNightTotal(0);
				rb.setParttimeHourDto(d1);
				// 派遣
				ReportWorkHourDto d2 = new ReportWorkHourDto();
				d2.setDayTotal(0);
				d2.setNightTotal(0);
				rb.setTemporaryHourDto(d2);
			} else {
				if (!b.getMark().equals("Z")) {
					if (!rb.getWorkName().contains(b.getWorkName())) {
						rb.setWorkName(rb.getWorkName() + "、" + b.getWorkName());
					}
				}

			}

			totalWorkHour(rb.getParttimeHourDto(), b.getParttimeHourDto());
			totalWorkHour(rb.getTemporaryHourDto(), b.getTemporaryHourDto());

			if (isZMark) {
				b.setMark("Z");
			}
		}

		return hasWorksBusinesses;
	}

	/**
	 * 分から時間へ変換する。
	 *
	 * @param dto
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	private void convertToHour(ReportWorkHourDto dto)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		for (int index = 0; index < 24; index++) {
			double val = (double) PropertyUtils.getProperty(dto, "workTime" + index);
			BeanUtils.setProperty(dto, "workTime" + index, JxlsUtils.divide(val));
		}

		dto.setDayTotal(JxlsUtils.divide(dto.getDayTotal()));
		dto.setNightTotal(JxlsUtils.divide(dto.getNightTotal()));
		dto.setTotal(JxlsUtils.divide(dto.getTotal()));
	}

	/**
	 * 値を設定する。
	 *
	 * @param startTime
	 * @param endTime
	 * @param rWorkHourDto
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	private void setWorkMinuteByHour(DateTime startTime, DateTime endTime, ReportWorkHourDto rWorkHourDto, WorkDay reportDay)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		// 時間を計算する
		double[] hours = calculate(startTime, endTime, reportDay);

		for (int index = 0; index < 24; index++) {
			double val = (double) PropertyUtils.getProperty(rWorkHourDto, "workTime" + index);
			BeanUtils.setProperty(rWorkHourDto, "workTime" + index, val + hours[index]);
		}

		rWorkHourDto.setDayTotal(rWorkHourDto.getDayTotal() + hours[24]);
		rWorkHourDto.setNightTotal(rWorkHourDto.getNightTotal() + hours[25]);
		rWorkHourDto.setTotal(rWorkHourDto.getTotal() + hours[24] + hours[25]);
	}

	/**
	 * 工数の合計
	 *
	 * @param totalParttimeHour
	 * @param reportWorkHourDto
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	private void totalWorkHour(ReportWorkHourDto totalParttimeHour, ReportWorkHourDto reportWorkHourDto)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		for (int index = 0; index < 24; index++) {
			double val1 = (double) PropertyUtils.getProperty(totalParttimeHour, "workTime" + index);
			double val2 = (double) PropertyUtils.getProperty(reportWorkHourDto, "workTime" + index);
			BeanUtils.setProperty(totalParttimeHour, "workTime" + index, val1 + val2);
		}

		totalParttimeHour.setDayTotal(totalParttimeHour.getDayTotal() + reportWorkHourDto.getDayTotal());
		totalParttimeHour.setNightTotal(totalParttimeHour.getNightTotal() + reportWorkHourDto.getNightTotal());

	}

	/**
	 * データを取得する
	 *
	 * @param reportBusinessDtoList
	 * @return
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	private List<ReportWorkHourDto> getTotalData(List<ReportBusinessDto> reportBusinessDtoList)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		List<ReportWorkHourDto> list = new ArrayList<ReportWorkHourDto>();

		// LF
		ReportWorkHourDto parttimeHour = new ReportWorkHourDto();

		// 派遣
		ReportWorkHourDto temporaryHour = new ReportWorkHourDto();

		// 合計
		ReportWorkHourDto totalHour = new ReportWorkHourDto();

		for (ReportBusinessDto dto : reportBusinessDtoList) {

			totalWorkHour(parttimeHour, dto.getParttimeHourDto());
			totalWorkHour(temporaryHour, dto.getTemporaryHourDto());
		}

		// 合計
		totalWorkHour(totalHour, parttimeHour);
		totalWorkHour(totalHour, temporaryHour);

		list.add(parttimeHour);
		list.add(temporaryHour);
		list.add(totalHour);

		return list;

	}

	/**
	 * businessを準備する
	 *
	 * @param business
	 * @param workStartTime
	 * @param workEndTime
	 * @update business(不要データがないのになる)
	 */
	private void processBusinessData(List<BusinessTask> business, DateTime workStartTime, DateTime workEndTime) {

		//    	dealWithNoOneAtWorkSituation(business);
		processOvernightShiftEndedToday(business, workStartTime);
		processOvernightShiftStartedToday(business, workEndTime);

	}

	//	/**
	//	 * その日誰も仕事をしていない状況に対処する
	//	 * @param business
	//	 */
	//	private void dealWithNoOneAtWorkSituation(List<BusinessTask> business) {
	//		business.stream().forEach(x->
	//				{
	//					x.getWorkTask().removeIf(y->y.getWorkStaffId() == null);
	//				}
	//		);
	//	}

	/**
	 * シフトが昨日始まり、今日終わる状況に対処する
	 * @param business
	 * @param workEndTime
	 */
	private void processOvernightShiftStartedToday(List<BusinessTask> business, DateTime workEndTime) {
		business.stream().filter(a -> CollectionUtils.isNotEmpty(a.getWorkTask())).forEach(b -> {
			b.getWorkTask().stream().filter(t -> t.getWorkStartTime() != null && t.getWorkEndTime().isAfter(workEndTime)).forEach(t -> {
				t.setWorkEndTime(workEndTime);
			});
		});
	}

	/**
	 * シフトが今日始まり、明日終わる状況に対処する
	 * @param business
	 * @param workStartTime
	 */
	private void processOvernightShiftEndedToday(List<BusinessTask> business, DateTime workStartTime) {
		business.stream().filter(a -> CollectionUtils.isNotEmpty(a.getWorkTask())).forEach(b -> {
			b.getWorkTask().stream().filter(t -> t.getWorkStartTime() != null && t.getWorkStartTime().isBefore(workStartTime)).forEach(t -> {
				t.setWorkStartTime(workStartTime);
			});
		});
	}
	//	/**
	//	 * 時間を変更する
	//	 *
	//	 * @param date
	//	 * @param daysToAdd
	//	 * @return dateManipulator = $date + $daysToAdd
	//	 */
	//	private static DateTime dateManipulator(DateTime date, int daysToAdd){
	//		Calendar cal = Calendar.getInstance();
	//		cal.setTime(date.toDate());
	//		cal.add(Calendar.DATE, daysToAdd);
	//		return new DateTime(cal.getTime());
	//	}

	/**
	 * 時間帯を計算する
	 *
	 * @param start
	 * @param end
	 * @return
	 */
	private static double[] calculate(DateTime start, DateTime end, WorkDay reportDay) {

		double[] worktime = new double[26];
		Arrays.fill(worktime, 0);
		//WorkDay wd = ServiceUtil.getWorkDayForReportService(start);

		// 7時から
		DateTime fromTime = reportDay.getWorkStartTime();

		long fromLimit = start.toDate().getTime() - fromTime.toDate().getTime();
		long toLimit = end.toDate().getTime() - fromTime.toDate().getTime();

		long milliSecondPerHour = 60 * 60 * 1000;

		long startCount = fromLimit / milliSecondPerHour;
		long startRemnant = fromLimit % milliSecondPerHour;

		long toCount = toLimit / milliSecondPerHour;
		long toRemnant = toLimit % milliSecondPerHour;
		if (toCount > 24) {
			toCount = 24;
			toRemnant = 0;
		}

		for (long index = startCount; index <= toCount; index++) {

			long deductionFrom = 0;
			if (index == startCount) {
				deductionFrom = startRemnant / (60 * 1000);
			}
			long really = 60;
			if (index == toCount) {
				really = toRemnant / (60 * 1000);
			}
			// 7時から
			worktime[(int) index] = really - deductionFrom;

		}

		// 日勤（7:00～21:59）
		for (int i = 0; i < 15; i++) {
			worktime[24] = worktime[24] + worktime[i];
		}
		// 夜勤（22:00～06:59）
		for (int i = 15; i < 24; i++) {
			worktime[25] = worktime[25] + worktime[i];
		}
		return worktime;
	}
}
