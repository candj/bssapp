package com.lf.bigdata.service;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.candj.webpower.web.core.exception.UncheckedNotFoundException;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import com.lf.bigdata.dao.mapper.BusinessAttendanceDao;
import com.lf.bigdata.dto.BusinessAttendanceDto;
import com.lf.bigdata.mapper.model.BusinessAttendance;

/**
 * 勤怠業務情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class BusinessAttendanceService {
	@Autowired
	BusinessAttendanceDao businessAttendanceDao;

	@Autowired
	Mapper mapper;

	// 出勤
	public static final String starting = "01";
	// 作業
	public static final String task_work = "02";
	// 退勤
	public static final String leaving = "03";
	// 休憩45分
	public static final String rest_45_min = "04";
	// 休憩15分
	public static final String rest_15_min = "05";
	// トイレ休憩
	public static final String toilet = "06";

	//勤務業務区分
	public static final List<Object> attendance = Arrays.asList(starting, leaving);

	//作業業務区分
	public static final List<Object> task = Arrays.asList(task_work, rest_45_min, rest_15_min, toilet);

	/**
	 * プライマリーキーで勤怠業務を検索する。
	 * @param businessId ID
	 * @return 結果
	 */
	public BusinessAttendanceDto selectByPrimaryKey(Integer businessId) {
		//プライマリーキーで勤怠業務を検索する。
		BusinessAttendance ret = businessAttendanceDao.selectByPrimaryKey(businessId);
		return DozerHelper.map(mapper, ret, BusinessAttendanceDto.class);
	}

	/**
	 * プライマリーキーで勤怠業務を検索する。（連携情報含む）
	 * @param businessId ID
	 * @return 結果
	 */
	public BusinessAttendanceDto selectAllByPrimaryKey(Integer businessId) {
		//プライマリーキーで勤怠業務検索する。（連携情報含む）
		BusinessAttendance ret = businessAttendanceDao.selectAllByPrimaryKey(businessId).stream().findFirst()
				.orElseThrow(UncheckedNotFoundException::new);
		return DozerHelper.map(mapper, ret, BusinessAttendanceDto.class);
	}

	/**
	 * 条件で勤怠業務を検索する。（連携情報含む）
	 * @param dto 検索条件
	 * @return 結果
	 */
	public List<BusinessAttendanceDto> selectByExample(BusinessAttendanceDto dto) {
		WhereCondition whereCondition = new WhereCondition();
		if (dto != null) {
			whereCondition.createCriteria()
					.andEqualTo("business_attendance.business_id", dto.getBusinessId())
					.andEqualTo("business_attendance.business_name", dto.getBusinessName())
					.andEqualTo("business_attendance.business_name_cn", dto.getBusinessNameCn())
					.andEqualTo("business_attendance.business_code", dto.getBusinessCode())
					.andEqualTo("business_attendance.business_encrypt_code", dto.getBusinessEncryptCode())
					.andEqualTo("business_attendance.business_start_time", dto.getBusinessStartTime())
					.andEqualTo("business_attendance.limit_end_time", dto.getLimitEndTime())
					.andEqualTo("business_attendance.limit_end_time_day", dto.getLimitEndTimeDay())
					.andEqualTo("business_attendance.business_type_id", dto.getBusinessTypeId())
					.andEqualTo("business_attendance.create_user", dto.getCreateUser())
					.andEqualTo("business_attendance.update_user", dto.getUpdateUser())
					.andEqualTo("business_attendance.create_time", dto.getCreateTime())
					.andEqualTo("business_attendance.update_time", dto.getUpdateTime());
		}
		//条件で勤怠業務を検索する。（連携情報含む）)
		List<BusinessAttendance> ret = businessAttendanceDao.selectByExample(whereCondition);
		return DozerHelper.mapList(mapper, ret, BusinessAttendanceDto.class);
	}

	/**
	 * 条件で勤怠業務を検索する。（連携情報含む）
	 * @param dto 検索条件
	 * @return 結果
	 */
	public List<BusinessAttendanceDto> selectAllByExample(BusinessAttendanceDto dto) {
		WhereCondition whereCondition = new WhereCondition();
		if (dto != null) {
			whereCondition.createCriteria()
					.andEqualTo("business_attendance.business_id", dto.getBusinessId())
					.andEqualTo("business_attendance.business_name", dto.getBusinessName())
					.andEqualTo("business_attendance.business_name_cn", dto.getBusinessNameCn())
					.andEqualTo("business_attendance.business_code", dto.getBusinessCode())
					.andEqualTo("business_attendance.business_encrypt_code", dto.getBusinessEncryptCode())
					.andEqualTo("business_attendance.business_start_time", dto.getBusinessStartTime())
					.andEqualTo("business_attendance.limit_end_time", dto.getLimitEndTime())
					.andEqualTo("business_attendance.limit_end_time_day", dto.getLimitEndTimeDay())
					.andEqualTo("business_attendance.business_type_id", dto.getBusinessTypeId())
					.andEqualTo("business_attendance.create_user", dto.getCreateUser())
					.andEqualTo("business_attendance.update_user", dto.getUpdateUser())
					.andEqualTo("business_attendance.create_time", dto.getCreateTime())
					.andEqualTo("business_attendance.update_time", dto.getUpdateTime());
		}
		//条件で勤怠業務を検索する。（連携情報含む）)
		List<BusinessAttendance> ret = businessAttendanceDao.selectAllByExample(whereCondition);
		return DozerHelper.mapList(mapper, ret, BusinessAttendanceDto.class);
	}

	/**
	 * 条件で勤怠業務を検索する。（連携情報含む）
	 * @param dto 検索条件
	 * @return 結果
	 */
	public List<BusinessAttendanceDto> selectLeavingBusiness() {
		//条件で勤怠業務を検索する。（連携情報含む）)
		List<BusinessAttendance> ret = businessAttendanceDao.selectByTypeCode(leaving);
		return com.candj.webpower.web.core.util.DozerHelper.mapList(mapper, ret, BusinessAttendanceDto.class);
	}

	/**
	 * 勤怠業務を新規追加する。
	 * @param dto 勤怠業務
	 * @return 結果
	 */
	public int insertSelective(BusinessAttendanceDto dto) {

		WhereCondition whereCondition = new WhereCondition();
		whereCondition.createCriteria()
				.andEqualTo("business_attendance.business_encrypt_code", dto.getBusinessEncryptCode());
		List<BusinessAttendance> list = businessAttendanceDao.selectAllByExample(whereCondition);
		if (CollectionUtils.isNotEmpty(list)) {
			throw new UncheckedLogicException("errors.error.data_already_exist", "バーコード");
		}

		BusinessAttendance businessAttendance = mapper.map(dto, BusinessAttendance.class);
		//勤怠業務を新規追加する。
		int ret = businessAttendanceDao.insertSelective(businessAttendance);
		return ret;
	}

	/**
	 * 勤怠業務を新規追加する。
	 * @param dto 勤怠業務
	 * @return 結果
	 */
	public int insert(BusinessAttendanceDto dto) {
		BusinessAttendance businessAttendance = mapper.map(dto, BusinessAttendance.class);
		//勤怠業務を新規追加する。
		int ret = businessAttendanceDao.insert(businessAttendance);
		return ret;
	}

	/**
	 * プライマリーキーで勤怠業務を更新する。
	 * @param dto 勤怠業務
	 * @return 結果
	 */
	public int updateByPrimaryKey(BusinessAttendanceDto dto) {
		BusinessAttendance businessAttendance = mapper.map(dto, BusinessAttendance.class);
		//プライマリーキーで勤怠業務を更新する。
		int ret = businessAttendanceDao.updateByPrimaryKey(businessAttendance);
		return ret;
	}

	/**
	 * プライマリーキーで勤怠業務を更新する。
	 * @param dto 勤怠業務
	 * @return 結果
	 */
	public int updateByPrimaryKeySelective(BusinessAttendanceDto dto) {

		WhereCondition whereCondition = new WhereCondition();
		whereCondition.createCriteria()
				.andEqualTo("business_attendance.business_encrypt_code", dto.getBusinessEncryptCode());
		List<BusinessAttendance> list = businessAttendanceDao.selectAllByExample(whereCondition);
		list.stream().filter(w -> !w.getBusinessId().equals(dto.getBusinessId())).findFirst().ifPresent(b -> {
			throw new UncheckedLogicException("errors.error.data_already_exist", "バーコード");

		});
		BusinessAttendance businessAttendance = DozerHelper.map(mapper, dto, BusinessAttendance.class);
		//プライマリーキーで勤怠業務を更新する。
		int ret = businessAttendanceDao.updateByPrimaryKeySelective(businessAttendance);
		return ret;
	}

	/**
	 * 勤怠業務を削除する。
	 * @param businessId ID
	 * @return 結果
	 */
	public int deleteByPrimaryKey(Integer businessId) {
		//勤怠業務を削除する。
		int ret = businessAttendanceDao.deleteByPrimaryKey(businessId);
		return ret;
	}

	public boolean businessIsExists(BusinessAttendanceDto businessAttendanceDto) {

		WhereCondition whereCondition = new WhereCondition();
		WhereCondition.Criteria criteria = whereCondition.createCriteria();

		if (!StringUtils.isEmpty(businessAttendanceDto.getBusinessCode())) {
			criteria.andEqualTo("business_code", businessAttendanceDto.getBusinessCode());
		}

		if (!StringUtils.isEmpty(businessAttendanceDto.getBusinessEncryptCode())) {
			criteria.andEqualTo("business_encrypt_code", businessAttendanceDto.getBusinessEncryptCode());
		}

		if (businessAttendanceDto.getBusinessId() != null && businessAttendanceDto.getBusinessId() > 0) {
			criteria.andNotEqualTo("business_id", businessAttendanceDto.getBusinessId());
		}

		int count = businessAttendanceDao.selectCountByExample(whereCondition);
		return count > 0;
	}

}