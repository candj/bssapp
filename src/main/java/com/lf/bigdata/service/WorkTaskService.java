package com.lf.bigdata.service;

import java.text.ParseException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.candj.webpower.web.core.exception.UncheckedNotFoundException;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.model.WhereCondition.Criteria;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import com.lf.bigdata.dao.mapper.WorkTaskDao;
import com.lf.bigdata.dto.WorkTaskDto;
import com.lf.bigdata.mapper.model.WorkTask;

/**
 * スタッフ作業情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class WorkTaskService {
    @Autowired
    WorkTaskDao workTaskDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーでスタッフ作業を検索する。
     * @param workId ID
     * @return 結果
     */
    public WorkTaskDto selectByPrimaryKey(Integer workId) {
        //プライマリーキーでスタッフ作業を検索する。
        WorkTask ret = workTaskDao.selectByPrimaryKey(workId);
        return DozerHelper.map(mapper, ret, WorkTaskDto.class);
    }

    /**
     * プライマリーキーでスタッフ作業を検索する。（連携情報含む）
     * @param workId ID
     * @return 結果
     */
    public WorkTaskDto selectAllByPrimaryKey(Integer workId) {
        //プライマリーキーでスタッフ作業検索する。（連携情報含む）
        WorkTask ret = workTaskDao.selectAllByPrimaryKey(workId).stream().findFirst().orElseThrow(UncheckedNotFoundException::new);
        return DozerHelper.map(mapper, ret, WorkTaskDto.class);
    }

    /**
     * 条件でスタッフ作業を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<WorkTaskDto> selectByExample(WorkTaskDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("work_task.work_id", dto.getWorkId())
            .andEqualTo("work_task.work_staff_id", dto.getWorkStaffId())
            .andEqualTo("work_task.work_business_id", dto.getWorkBusinessId())
            .andEqualTo("work_task.work_start_time", dto.getWorkStartTime())
            .andEqualTo("work_task.work_end_time", dto.getWorkEndTime())
            .andEqualTo("work_task.create_user", dto.getCreateUser())
            .andEqualTo("work_task.update_user", dto.getUpdateUser())
            .andEqualTo("work_task.create_time", dto.getCreateTime())
            .andEqualTo("work_task.update_time", dto.getUpdateTime());
        }
        //条件でスタッフ作業を検索する。（連携情報含む）)
        List<WorkTask> ret = workTaskDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, WorkTaskDto.class);
    }

    /**
     * 条件でスタッフ作業を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<WorkTaskDto> selectAllByExample(WorkTaskDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("work_task.work_id", dto.getWorkId())
            .andEqualTo("work_task.work_staff_id", dto.getWorkStaffId())
            .andEqualTo("work_task.work_business_id", dto.getWorkBusinessId())
            .andEqualTo("work_task.work_start_time", dto.getWorkStartTime())
            .andEqualTo("work_task.work_end_time", dto.getWorkEndTime())
            .andEqualTo("work_task.create_user", dto.getCreateUser())
            .andEqualTo("work_task.update_user", dto.getUpdateUser())
            .andEqualTo("work_task.create_time", dto.getCreateTime())
            .andEqualTo("work_task.update_time", dto.getUpdateTime());
        }
        //条件でスタッフ作業を検索する。（連携情報含む）)
        List<WorkTask> ret = workTaskDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, WorkTaskDto.class);
    }

    /**
     * スタッフ作業を新規追加する。
     * @param dto スタッフ作業
     * @return 結果
     */
    public int insertSelective(WorkTaskDto dto) {
        WorkTask workTask  =  mapper.map(dto, WorkTask.class);
        //スタッフ作業を新規追加する。
        int ret = workTaskDao.insertSelective(workTask);
        return ret;
    }

    /**
     * スタッフ作業を新規追加する。
     * @param dto スタッフ作業
     * @return 結果
     */
    public int insert(WorkTaskDto dto) {
        WorkTask workTask  =  mapper.map(dto, WorkTask.class);
        //スタッフ作業を新規追加する。
        int ret = workTaskDao.insert(workTask);
        return ret;
    }

    /**
     * プライマリーキーでスタッフ作業を更新する。
     * @param dto スタッフ作業
     * @return 結果
     */
    public int updateByPrimaryKey(WorkTaskDto dto) {
        WorkTask workTask  =  mapper.map(dto, WorkTask.class);
        //プライマリーキーでスタッフ作業を更新する。
        int ret = workTaskDao.updateByPrimaryKey(workTask);
        return ret;
    }

    /**
     * プライマリーキーでスタッフ作業を更新する。
     * @param dto スタッフ作業
     * @return 結果
     */
    public int updateByPrimaryKeySelective(WorkTaskDto dto) {
        WorkTask workTask  =  mapper.map(dto, WorkTask.class);
        //プライマリーキーでスタッフ作業を更新する。
        int ret = workTaskDao.updateByPrimaryKeySelective(workTask);
        return ret;
    }

    /**
     * スタッフ作業を削除する。
     * @param workId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer workId) {
        //スタッフ作業を削除する。
        int ret = workTaskDao.deleteByPrimaryKey(workId);
        return ret;
    }

    /**
     * 条件でスタッフ働きデータを検索する。（連携情報含む） 一日の範囲： 00:00から翌日00:00
     *
     * @param workTaskDto
     *            検索条件
     * @return 結果
     * @throws ParseException
     */
    public List<WorkTaskDto> selectOneDayWorkByExample(String staffCode, String staffName, DateTime workStartTime)
            throws ParseException {

        List<WorkTask> ret = null;
        WhereCondition whereCondition = new WhereCondition();

//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date from = df.parse(date + " 00:00:00");
//        Calendar c = Calendar.getInstance();
//        c.setTime(from);
//        c.add(Calendar.DATE, 1);
//        Date to = c.getTime();

        Criteria criteria = whereCondition.createCriteria();
        criteria.andGreaterThanOrEqualTo("work_task.work_start_time", workStartTime);
        criteria.andLessThan("work_task.work_start_time", workStartTime.withFieldAdded(DurationFieldType.days(), 1));

//        Criteria orCriteria = whereCondition.or();
//        orCriteria.andGreaterThanOrEqualTo("work_task.work_end_time", from);
//        orCriteria.andLessThan("work_task.work_end_time", to);
        if (StringUtils.isNotEmpty(staffCode)) {
            criteria.andLike("staff.staff_code", staffCode);
//            orCriteria.andEqualTo("staff.staff_code", staffCode);
        }
        if (StringUtils.isNotEmpty(staffName)) {
            criteria.andLike("staff.staff_Name", staffName);
//            orCriteria.andLike("staff.staff_Name", staffName);
        }

        whereCondition.setOrderByClause("staff.staff_id ASC,work_task.work_start_time ASC");

        ret = workTaskDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, WorkTaskDto.class);
    }

}