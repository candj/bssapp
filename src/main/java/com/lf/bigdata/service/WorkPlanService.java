package com.lf.bigdata.service;

import com.candj.webpower.web.core.exception.BusinessLogicException;
import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import com.lf.bigdata.dao.mapper.WorkPlanDao;
import com.lf.bigdata.dto.StaffDto;
import com.lf.bigdata.dto.WorkPlanDto;
import com.lf.bigdata.mapper.model.WorkPlan;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opencsv.CSVReader;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 作業予定情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class WorkPlanService {
    @Autowired
    WorkPlanDao workPlanDao;

    @Autowired
    WorkPlanService workPlanService;

    @Autowired
    StaffService staffService;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで作業予定を検索する。
     * @param workPlanId 作業予定ID
     * @return 結果
     */
    public WorkPlanDto selectByPrimaryKey(Integer workPlanId) {
        //プライマリーキーで作業予定を検索する。
        WorkPlan ret = workPlanDao.selectByPrimaryKey(workPlanId);
        return DozerHelper.map(mapper, ret, WorkPlanDto.class);
    }

    /**
     * プライマリーキーで作業予定を検索する。（連携情報含む）
     * @param workPlanId 作業予定ID
     * @return 結果
     */
    public List<WorkPlanDto> selectAllByPrimaryKey(Integer workPlanId) {
        //プライマリーキーで作業予定検索する。（連携情報含む）
        List<WorkPlan> ret = workPlanDao.selectAllByPrimaryKey(workPlanId);
        return DozerHelper.mapList(mapper, ret, WorkPlanDto.class);
    }

    /**
     * 条件で作業予定を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<WorkPlanDto> selectByExample(WorkPlanDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("work_plan.work_plan_id", dto.getWorkPlanId())
            .andEqualTo("work_plan.work_plan_company_id", dto.getWorkPlanCompanyId())
            .andEqualTo("work_plan.work_plan_staff_id", dto.getWorkPlanStaffId())
            .andEqualTo("work_plan.work_plan_date", dto.getWorkPlanDate())
            .andEqualTo("work_plan.work_plan_number", dto.getWorkPlanNumber())
            .andEqualTo("work_plan.work_plan_level", dto.getWorkPlanLevel())
            .andEqualTo("work_plan.work_plan_name", dto.getWorkPlanName())
            .andEqualTo("work_plan.work_plan_name_kana", dto.getWorkPlanNameKana())
            .andEqualTo("work_plan.create_user", dto.getCreateUser())
            .andEqualTo("work_plan.update_user", dto.getUpdateUser())
            .andEqualTo("work_plan.create_time", dto.getCreateTime())
            .andEqualTo("work_plan.update_time", dto.getUpdateTime());
        }
        //条件で作業予定を検索する。（連携情報含む）)
        List<WorkPlan> ret = workPlanDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, WorkPlanDto.class);
    }

    /**
     * 条件で作業予定を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<WorkPlanDto> selectAllByExample(WorkPlanDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("work_plan.work_plan_id", dto.getWorkPlanId())
            .andEqualTo("work_plan.work_plan_company_id", dto.getWorkPlanCompanyId())
            .andEqualTo("work_plan.work_plan_staff_id", dto.getWorkPlanStaffId())
            .andEqualTo("work_plan.work_plan_date", dto.getWorkPlanDate())
            .andLike("work_plan.work_plan_number", dto.getWorkPlanNumber())
            .andEqualTo("work_plan.work_plan_level", dto.getWorkPlanLevel())
            .andLike("work_plan.work_plan_name", dto.getWorkPlanName())
            .andEqualTo("work_plan.work_plan_name_kana", dto.getWorkPlanNameKana())
            .andEqualTo("work_plan.create_user", dto.getCreateUser())
            .andEqualTo("work_plan.update_user", dto.getUpdateUser())
            .andEqualTo("work_plan.create_time", dto.getCreateTime())
            .andEqualTo("work_plan.update_time", dto.getUpdateTime());
        }
        //条件で作業予定を検索する。（連携情報含む）)
        List<WorkPlan> ret = workPlanDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, WorkPlanDto.class);
    }

    /**
     * 作業予定を新規追加する。
     * @param dto 作業予定
     * @return 結果
     */
    public int insertSelective(WorkPlanDto dto) {
        WorkPlan workPlan  =  mapper.map(dto, WorkPlan.class);
        //作業予定を新規追加する。
        int ret = workPlanDao.insertSelective(workPlan);
        return ret;
    }

    /**
     * 作業予定を新規追加する。
     * @param dto 作業予定
     * @return 結果
     */
    public int insert(WorkPlanDto dto) {
        WorkPlan workPlan  =  mapper.map(dto, WorkPlan.class);
        //作業予定を新規追加する。
        int ret = workPlanDao.insert(workPlan);
        return ret;
    }

    /**
     * プライマリーキーで作業予定を更新する。
     * @param dto 作業予定
     * @return 結果
     */
    public int updateByPrimaryKey(WorkPlanDto dto) {
        WorkPlan workPlan  =  mapper.map(dto, WorkPlan.class);
        //プライマリーキーで作業予定を更新する。
        int ret = workPlanDao.updateByPrimaryKey(workPlan);
        return ret;
    }

    /**
     * プライマリーキーで作業予定を更新する。
     * @param dto 作業予定
     * @return 結果
     */
    public int updateByPrimaryKeySelective(WorkPlanDto dto) {
        WorkPlan workPlan  =  mapper.map(dto, WorkPlan.class);
        //プライマリーキーで作業予定を更新する。
        int ret = workPlanDao.updateByPrimaryKeySelective(workPlan);
        return ret;
    }

    /**
     * 作業予定を削除する。
     * @param workPlanId 作業予定ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer workPlanId) {
        //作業予定を削除する。
        int ret = workPlanDao.deleteByPrimaryKey(workPlanId);
        return ret;
    }

    /**
     * エクセルで作業予定を追加する。
     * @param inputStream エクセルのパス
     * @return 結果
     */
    public int getWorkPlan(InputStream inputStream, DateTime date, Integer companyId) throws IOException{

        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

        //excelが正しいかどうっかを確認
        int sheetCount = workbook.getNumberOfSheets();
        if (sheetCount < 1) {
            throw new UncheckedLogicException("errors.error.file_content_not_validate");
        }

        //データーを取得する
        XSSFSheet sheet = workbook.getSheetAt(0);
        int lastRowNum = sheet.getLastRowNum();
        int rowIndex = 0;

        //スタッフデータを取得
        StaffDto dto = new StaffDto();
        dto.setStaffCompanyId(companyId);
        List<StaffDto> staffDtoList =  staffService.selectByExample(dto);
        Map<String, StaffDto> staffs = new HashMap<String, StaffDto>();
        for(StaffDto s : staffDtoList){
            staffs.put(s.getWorkerNumber(), s);
        }
        //作業予定データを取得する
        WorkPlanDto wDto = new WorkPlanDto();
        wDto.setWorkPlanDate(date);
        wDto.setWorkPlanCompanyId(companyId);
        List<WorkPlanDto> workPlanDtoList = workPlanService.selectByExample(wDto);
        Map<String, WorkPlanDto> workPlans = new HashMap<String, WorkPlanDto>();
        for(WorkPlanDto w : workPlanDtoList){
            workPlans.put(w.getWorkPlanNumber(), w);
        }

        for(; rowIndex <= lastRowNum; rowIndex++){
            if(sheet.getRow(rowIndex) == null){
                throw new UncheckedLogicException("errors.error.file_content_not_validate");
            }
            else if(sheet.getRow(rowIndex).getCell(0).toString().equals("行")){
                break;
            }
        }
            if(rowIndex >= lastRowNum){
                throw new UncheckedLogicException("errors.error.file_content_not_validate");
            }
            rowIndex++;
            for (; rowIndex <= lastRowNum; rowIndex++) {
                WorkPlan workPlan = new WorkPlan();

                XSSFRow xRow = sheet.getRow(rowIndex);
                if(xRow.getCell(3).getRawValue() == null){
                    break;
                }
                String workerNumber = xRow.getCell(3).toString();

                if(staffs.containsKey(workerNumber)){
                    StaffDto staffDto = staffs.get(workerNumber);
                    workPlan.setWorkPlanStaffId(staffDto.getStaffId());
                    workPlan.setWorkPlanNumber(workerNumber);
                    workPlan.setWorkPlanName(staffDto.getStaffName());
                }
                else{
                    workPlan.setWorkPlanNumber(xRow.getCell(3).toString());
                    workPlan.setWorkPlanName(xRow.getCell(4).toString());
                }
                workPlan.setWorkPlanCompanyId(companyId);
                workPlan.setWorkPlanDate(date);
                workPlan.setWorkPlanLevel(xRow.getCell(2).toString());
                workPlan.setWorkPlanNameKana(xRow.getCell(5).toString());

                if(workPlans.containsKey(workerNumber)){
                    //作業予定を更新する。
                    workPlan.setWorkPlanId(workPlans.get(workerNumber).getWorkPlanId());
                    workPlanDao.updateByPrimaryKeySelective(workPlan);
                }
                else{
                    //作業予定を新規追加する。
                    workPlanDao.insertSelective(workPlan);
                }
            }


        return 1;
    }
}
