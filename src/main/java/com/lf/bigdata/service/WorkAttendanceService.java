package com.lf.bigdata.service;

import java.text.ParseException;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.candj.webpower.web.core.exception.UncheckedNotFoundException;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import com.lf.bigdata.dao.mapper.WorkAttendanceDao;
import com.lf.bigdata.dto.CostConditionDto;
import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.dto.WorkAttendanceReportDto;
import com.lf.bigdata.dto.WorkCostReportDto;
import com.lf.bigdata.dto.WorkDto;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;
import com.lf.bigdata.mapper.model.WorkAttendance;
import com.lf.bigdata.mapper.model.WorkDay;
import com.lf.bigdata.utils.ServiceUtil;

/**
 * スタッフ出勤情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class WorkAttendanceService {
    @Autowired
    WorkAttendanceDao workAttendanceDao;

    @Autowired
    Mapper mapper;

    BusinessTypeCodeEnum businessTypeCodeEnum;

    /**
     * プライマリーキーでスタッフ出勤を検索する。
     * @param workId ID
     * @return 結果
     */
    public WorkAttendanceDto selectByPrimaryKey(Integer workId) {
        //プライマリーキーでスタッフ出勤を検索する。
        WorkAttendance ret = workAttendanceDao.selectByPrimaryKey(workId);
        return DozerHelper.map(mapper, ret, WorkAttendanceDto.class);
    }

    /**
     * プライマリーキーでスタッフ出勤を検索する。（連携情報含む）
     * @param workId ID
     * @return 結果
     */
    public WorkAttendanceDto selectAllByPrimaryKey(Integer workId) {
        //プライマリーキーでスタッフ出勤検索する。（連携情報含む）
        WorkAttendance ret = workAttendanceDao.selectAllByPrimaryKey(workId).stream().findFirst().orElseThrow(UncheckedNotFoundException::new);
        return DozerHelper.map(mapper, ret, WorkAttendanceDto.class);
    }

    /**
     * 条件でスタッフ出勤を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<WorkAttendanceDto> selectByExample(WorkAttendanceDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("work_attendance.work_id", dto.getWorkId())
            .andEqualTo("work_attendance.work_staff_id", dto.getWorkStaffId())
            .andEqualTo("work_attendance.work_business_id", dto.getWorkBusinessId())
            .andEqualTo("work_attendance.work_start_time", dto.getWorkStartTime())
            .andEqualTo("work_attendance.real_work_start_time", dto.getRealWorkStartTime())
            .andEqualTo("work_attendance.work_end_time", dto.getWorkEndTime())
            .andEqualTo("work_attendance.real_work_end_time", dto.getRealWorkEndTime())
            .andEqualTo("work_attendance.create_user", dto.getCreateUser())
            .andEqualTo("work_attendance.update_user", dto.getUpdateUser())
            .andEqualTo("work_attendance.create_time", dto.getCreateTime())
            .andEqualTo("work_attendance.update_time", dto.getUpdateTime());
        }
        //条件でスタッフ出勤を検索する。（連携情報含む）)
        List<WorkAttendance> ret = workAttendanceDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, WorkAttendanceDto.class);
    }

    /**
     * 条件でスタッフ出勤を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<WorkAttendanceDto> selectByStaffAndTypeID(WorkAttendanceDto dto, String businessTypeCode) {
        WhereCondition whereCondition = new WhereCondition();
        if (dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("work_staff_id", dto.getWorkStaffId())
                    .andEqualTo("business_type_id", businessTypeCode);
        }
        whereCondition.setOrderByClause("work_attendance.work_start_time DESC");
        //条件でスタッフ出勤を検索する。（連携情報含む）)
        List<WorkAttendance> ret = workAttendanceDao.selectAllByStaffAndTypeID(whereCondition);
        return com.candj.webpower.web.core.util.DozerHelper.mapList(mapper, ret, WorkAttendanceDto.class);
    }



    /**
     * 条件でスタッフ出勤を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<WorkAttendanceDto> selectAllByExample(WorkAttendanceDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("work_attendance.work_id", dto.getWorkId())
            .andEqualTo("work_attendance.work_staff_id", dto.getWorkStaffId())
            .andEqualTo("work_attendance.work_business_id", dto.getWorkBusinessId())
            .andEqualTo("work_attendance.work_start_time", dto.getWorkStartTime())
            .andEqualTo("work_attendance.real_work_start_time", dto.getRealWorkStartTime())
            .andEqualTo("work_attendance.work_end_time", dto.getWorkEndTime())
            .andEqualTo("work_attendance.real_work_end_time", dto.getRealWorkEndTime())
            .andEqualTo("work_attendance.create_user", dto.getCreateUser())
            .andEqualTo("work_attendance.update_user", dto.getUpdateUser())
            .andEqualTo("work_attendance.create_time", dto.getCreateTime())
            .andEqualTo("work_attendance.update_time", dto.getUpdateTime());
        }
        //条件でスタッフ出勤を検索する。（連携情報含む）)
        List<WorkAttendance> ret = workAttendanceDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, WorkAttendanceDto.class);
    }

    /**
     * スタッフ出勤を新規追加する。
     * @param dto スタッフ出勤
     * @return 結果
     */
    public int insertSelective(WorkAttendanceDto dto) {
        WorkAttendance workAttendance  =  mapper.map(dto, WorkAttendance.class);
        //スタッフ出勤を新規追加する。
        int ret = workAttendanceDao.insertSelective(workAttendance);
        return ret;
    }

    /**
     * スタッフ出勤を新規追加する。
     * @param dto スタッフ出勤
     * @return 結果
     */
    public int insert(WorkAttendanceDto dto) {
        WorkAttendance workAttendance  =  mapper.map(dto, WorkAttendance.class);
        //スタッフ出勤を新規追加する。
        int ret = workAttendanceDao.insert(workAttendance);
        return ret;
    }

    /**
     * プライマリーキーでスタッフ出勤を更新する。
     * @param dto スタッフ出勤
     * @return 結果
     */
    public int updateByPrimaryKey(WorkAttendanceDto dto) {
        WorkAttendance workAttendance  =  mapper.map(dto, WorkAttendance.class);
        //プライマリーキーでスタッフ出勤を更新する。
        int ret = workAttendanceDao.updateByPrimaryKey(workAttendance);
        return ret;
    }

    /**
     * プライマリーキーでスタッフ出勤を更新する。
     * @param dto スタッフ出勤
     * @return 結果
     */
    public int updateByPrimaryKeySelective(WorkAttendanceDto dto) {
        WorkAttendance workAttendance  =  mapper.map(dto, WorkAttendance.class);
        //プライマリーキーでスタッフ出勤を更新する。
        int ret = workAttendanceDao.updateByPrimaryKeySelective(workAttendance);
        return ret;
    }

    /**
     * スタッフ出勤を削除する。
     * @param workId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer workId) {
        //スタッフ出勤を削除する。
        int ret = workAttendanceDao.deleteByPrimaryKey(workId);
        return ret;
    }

    /**
     * 条件でスタッフ出勤を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<WorkAttendanceDto> selectLastWorkDay(String staffCode, String staffName) {
        WhereCondition whereCondition = new WhereCondition();
        WhereCondition.Criteria c = whereCondition.createCriteria();
        if (StringUtils.isNotEmpty(staffCode)) {

            c.andEqualTo("staff.staff_code", staffCode);
        }
        if (StringUtils.isNotEmpty(staffName)) {

            c.andEqualTo("staff.staff_name", staffName);
        }

        //条件でスタッフ出勤を検索する。（連携情報含む）)
        List<WorkAttendance> ret = workAttendanceDao.selectLastWorkDay(whereCondition);
        return com.candj.webpower.web.core.util.DozerHelper.mapList(mapper, ret, WorkAttendanceDto.class);
    }

    /**
     * 条件でスタッフ出勤を検索する。（連携情報含む）
     * @param staffId 検索条件
     * @return 結果
     */
    public List<WorkAttendanceDto> selectWorks(WorkAttendanceDto dto, List<Object> businessTypeCode)
            throws ParseException {
        WhereCondition whereCondition = new WhereCondition();

        WhereCondition.Criteria c = whereCondition.createCriteria();
        if (dto.getWorkStaffId() != null) {
            c.andEqualTo("work_staff_id", dto.getWorkStaffId());
        }
        if (dto.getStaff() != null) {
            if (StringUtils.isNotEmpty(dto.getStaff().getStaffCode())) {
                c.andLike("staff.staff_code", dto.getStaff().getStaffCode());
            }
            if (StringUtils.isNotEmpty(dto.getStaff().getStaffName())) {
                c.andLike("staff.staff_name", dto.getStaff().getStaffName());
            }
        }

        if (CollectionUtils.isNotEmpty(businessTypeCode)) {
            c.andIn("business_attendance.business_type_id", businessTypeCode);
        }
        if (dto.getWorkStartTime() != null) {
            c.andGreaterThanOrEqualTo("work_start_time", dto.getWorkStartTime());
        }
        if (dto.getWorkEndTime() != null) {
            c.andLessThan("work_start_time", dto.getWorkEndTime());
        }
        //条件でスタッフ出勤を検索する。（連携情報含む）)
        List<WorkAttendance> ret = workAttendanceDao.selectAllByExample(whereCondition);
        return com.candj.webpower.web.core.util.DozerHelper.mapList(mapper, ret, WorkAttendanceDto.class);
    }


    /**
     * 条件でスタッフ出勤を検索する。（連携情報含む）
     * @param staffId 検索条件
     * @return 結果
     */
    public List<WorkDto> selectAllOneDayWorks(WorkAttendanceDto dto, List<Object> businessTypeCode)
            throws ParseException {
//        WhereCondition whereCondition = new WhereCondition();
//
//        WhereCondition.Criteria c = whereCondition.createCriteria();
//        if (dto.getWorkStaffId() != null) {
//            c.andEqualTo("work_staff_id", dto.getWorkStaffId());
//        }
//        if (dto.getStaff() != null) {
//            if (StringUtils.isNotEmpty(dto.getStaff().getStaffCode())) {
//                c.andLike("staff.staff_code", dto.getStaff().getStaffCode());
//            }
//            if (StringUtils.isNotEmpty(dto.getStaff().getStaffName())) {
//                c.andLike("staff.staff_name", dto.getStaff().getStaffName());
//            }
//        }
//
//        if (dto.getWorkStartTime() != null) {
//            c.andGreaterThanOrEqualTo("work_start_time", dto.getWorkStartTime());
//        }
//        if (dto.getWorkEndTime() != null) {
//            c.andLessThan("work_start_time", dto.getWorkEndTime());
//        }
        //条件でスタッフ出勤を検索する。（連携情報含む）)
        return workAttendanceDao.selectAllOneDayWorks(com.candj.webpower.web.core.util.DozerHelper.map(mapper, dto, WorkAttendance.class));
    }

    /**
     * 条件でスタッフ出勤を検索する。（連携情報含む） 一日の範囲： 00:00から翌日00:00
     *
     * @param dto
     *            検索条件
     * @return 結果
     */
    public List<WorkAttendanceReportDto> selectAttendanceByExample(WorkAttendanceDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        WhereCondition.Criteria criteria = null;
        if (dto.getWorkStartTime() != null) {
            WorkDay wd = ServiceUtil.getWorkDay(dto.getWorkStartTime());
            criteria = whereCondition.createCriteria().andGreaterThanOrEqualTo("work_start_time", wd.getWorkStartTime())
            		.andLessThan("work_start_time", wd.getWorkEndTime());
        }
        if (dto.getStaff() != null && StringUtils.isNotEmpty(dto.getStaff().getStaffCode())) {
            criteria.andEqualTo("staff.staff_code", dto.getStaff().getStaffCode());
        }
        if (dto.getStaff() != null && StringUtils.isNotEmpty(dto.getStaff().getStaffName())) {
            criteria.andLike("staff.staff_name", dto.getStaff().getStaffName());
        }

        //criteria.andEqualTo("business_type_id", BusinessTypeCodeEnum.starting);


        whereCondition.setOrderByClause("staff.staff_code ASC,work_attendance.work_start_time ASC");
        List<WorkAttendanceReportDto> list = workAttendanceDao.selectOneDayAttendances(whereCondition);
        list.forEach(w -> {
        	w.setFormattedRestTime(ServiceUtil.formatRestTime(w.getRestTime()));
        });
        return list;
    }

    /**
     * 条件でスタッフ出勤を検索する。（連携情報含む） 一日の範囲： 00:00から翌日00:00
     *
     * @param dto
     *            検索条件
     * @return 結果
     */
    public List<WorkAttendanceReportDto> selectMultiAttendanceByExample(WorkAttendanceDto dto) {
        WhereCondition whereCondition = new WhereCondition();
        WhereCondition.Criteria criteria = null;
        if (dto.getWorkStartTime() != null) {
            WorkDay wd = ServiceUtil.getMultiWorkDay(dto.getWorkStartTime(), dto.getWorkEndTime());
            criteria = whereCondition.createCriteria().andGreaterThanOrEqualTo("work_start_time", wd.getWorkStartTime())
                    .andLessThan("work_start_time", wd.getWorkEndTime());
        }
        if (dto.getStaff() != null && StringUtils.isNotEmpty(dto.getStaff().getStaffCode())) {
            criteria.andEqualTo("staff.staff_code", dto.getStaff().getStaffCode());
        }
        if (dto.getStaff() != null && StringUtils.isNotEmpty(dto.getStaff().getStaffName())) {
            criteria.andLike("staff.staff_name", dto.getStaff().getStaffName());
        }

        //criteria.andEqualTo("business_type_id", BusinessTypeCodeEnum.starting);


        whereCondition.setOrderByClause("staff.staff_code ASC,work_attendance.work_start_time ASC");
        List<WorkAttendanceReportDto> list = workAttendanceDao.selectOneDayAttendances(whereCondition);
        list.forEach(w -> {
            w.setFormattedRestTime(ServiceUtil.formatRestTime(w.getRestTime()));
        });
        return list;
    }

    /**
     * コスト一覧情報取得する。
     *
     * @param date
     *            日付
     * @return 結果
     */
    public List<WorkCostReportDto> selectCost(DateTime fromDate, DateTime toDate, String staffWorkType) {


    	CostConditionDto dto = new CostConditionDto();
    	dto.setWorkStartTime(fromDate);
    	dto.setWorkEndTime(toDate);

    	WhereCondition whereCondition = new WhereCondition();
    	WhereCondition.Criteria criteria = null;
    	criteria = whereCondition.createCriteria().andGreaterThanOrEqualTo("work_attendance.work_start_time", fromDate)
    	        .andLessThan("work_attendance.work_start_time", toDate)
    	        .andEqualTo("staff.staff_work_type", staffWorkType);
    	whereCondition.setOrderByClause("work_start_time, staff_code");

    	List<WorkCostReportDto> list = workAttendanceDao.selectCost(dto, whereCondition);
        list.forEach(w -> {
        	w.setFormattedRestTime(ServiceUtil.formatRestTime(w.getRestTime()));
        	w.setWorkStartTime(ServiceUtil.resetStartEndTime(w.getWorkStartTime()));
        	w.setWorkEndTime(ServiceUtil.resetStartEndTime(w.getWorkEndTime()));
        	if(w.getWorkPlanLevel() != null) {
                if (w.getWorkPlanLevel().equals("リーダー")) {
                    w.setLeaderBonus(w.getStaff().getCompany().getLeaderSalary());
                } else if (w.getWorkPlanLevel().equals("サブリーダー")) {
                    w.setLeaderBonus(w.getStaff().getCompany().getSubLeaderSalary());
                } else if (w.getWorkPlanLevel().equals("トレーナー")) {
                    w.setLeaderBonus(w.getStaff().getCompany().getTrainerSalary());
                } else if (w.getWorkPlanLevel().equals("フォーク")) {
                    w.setLeaderBonus(w.getStaff().getCompany().getForkSalary());
                }
            }
        });
        return list;
    }



//    /**
//     * 条件でスタッフ出勤を検索する。（連携情報含む） 一日の範囲： 00:00から翌日00:00
//     *
//     * @param dto
//     *            検索条件
//     * @return 結果
//     */
//    public List<WorkAttendanceReportDto> selectAttendanceByExample(DateTime fromDate, DateTime toDate) {
//        WhereCondition whereCondition = new WhereCondition();
//        whereCondition.createCriteria().andGreaterThanOrEqualTo("work_start_time", fromDate)
//        		.andLessThan("work_start_time", toDate);
//        whereCondition.setOrderByClause("work_attendance.work_start_time ASC, staff.staff_code ASC");
//        List<WorkAttendanceReportDto> list = workAttendanceDao.selectOneDayAttendances(whereCondition);
//        list.forEach(w -> {
//        	w.setFormattedRestTime(ServiceUtil.formatRestTime(w.getRestTime()));
//        });
//        return list;
//    }
//




}