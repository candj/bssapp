package com.lf.bigdata.service;

import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import com.lf.bigdata.dao.mapper.ProcessDao;
import com.lf.bigdata.dto.ProcessDto;
import com.lf.bigdata.mapper.model.Process;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 作業区分情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class ProcessService {
    @Autowired
    ProcessDao processDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで作業区分を検索する。
     * @param processId ID
     * @return 結果
     */
    public ProcessDto selectByPrimaryKey(Integer processId) {
        //プライマリーキーで作業区分を検索する。
        Process ret = processDao.selectByPrimaryKey(processId);
        return DozerHelper.map(mapper, ret, ProcessDto.class);
    }

    /**
     * プライマリーキーで作業区分を検索する。（連携情報含む）
     * @param processId ID
     * @return 結果
     */
    public List<ProcessDto> selectAllByPrimaryKey(Integer processId) {
        //プライマリーキーで作業区分検索する。（連携情報含む）
        List<Process> ret = processDao.selectAllByPrimaryKey(processId);
        return DozerHelper.mapList(mapper, ret, ProcessDto.class);
    }

    /**
     * 条件で作業区分を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<ProcessDto> selectByExample(ProcessDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("process.process_id", dto.getProcessId())
            .andEqualTo("process.process_code", dto.getProcessCode())
            .andLike("process.process_name", dto.getProcessName())
            //.andEqualTo("process.process_status", dto.getProcessStatus())
            .andEqualTo("process.create_user", dto.getCreateUser())
            .andEqualTo("process.update_user", dto.getUpdateUser())
            .andEqualTo("process.create_time", dto.getCreateTime())
            .andEqualTo("process.update_time", dto.getUpdateTime());
        }
        //条件で作業区分を検索する。（連携情報含む）)
        List<Process> ret = processDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, ProcessDto.class);
    }

    /**
     * 条件で作業区分を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<ProcessDto> selectByStatus(ProcessDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
            whereCondition.createCriteria()
                    .andEqualTo("process.process_status", true);
            whereCondition.setOrderByClause("process.process_name asc");
        //条件で作業区分を検索する。（連携情報含む）)
        List<Process> ret = processDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, ProcessDto.class);
    }

    /**
     * 条件で作業区分を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<ProcessDto> selectAllByExample(ProcessDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("process.process_id", dto.getProcessId())
            .andEqualTo("process.process_code", dto.getProcessCode())
            .andEqualTo("process.process_name", dto.getProcessName())
            .andEqualTo("process.process_status", dto.getProcessStatus())
            .andEqualTo("process.create_user", dto.getCreateUser())
            .andEqualTo("process.update_user", dto.getUpdateUser())
            .andEqualTo("process.create_time", dto.getCreateTime())
            .andEqualTo("process.update_time", dto.getUpdateTime());
        }
        whereCondition.setOrderByClause("process.process_name asc");
        //条件で作業区分を検索する。（連携情報含む）)
        List<Process> ret = processDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, ProcessDto.class);
    }

    /**
     * 作業区分を新規追加する。
     * @param dto 作業区分
     * @return 結果
     */
    public int insertSelective(ProcessDto dto) {
        Process process  =  mapper.map(dto, Process.class);
        //作業区分を新規追加する。
        int ret = processDao.insertSelective(process);
        return ret;
    }

    /**
     * 作業区分を新規追加する。
     * @param dto 作業区分
     * @return 結果
     */
    public int insert(ProcessDto dto) {
        Process process  =  mapper.map(dto, Process.class);
        //作業区分を新規追加する。
        int ret = processDao.insert(process);
        return ret;
    }

    /**
     * プライマリーキーで作業区分を更新する。
     * @param dto 作業区分
     * @return 結果
     */
    public int updateByPrimaryKey(ProcessDto dto) {
        Process process  =  mapper.map(dto, Process.class);
        //プライマリーキーで作業区分を更新する。
        int ret = processDao.updateByPrimaryKey(process);
        return ret;
    }

    /**
     * プライマリーキーで作業区分を更新する。
     * @param dto 作業区分
     * @return 結果
     */
    public int updateByPrimaryKeySelective(ProcessDto dto) {
        Process process  =  mapper.map(dto, Process.class);
        //プライマリーキーで作業区分を更新する。
        int ret = processDao.updateByPrimaryKeySelective(process);
        return ret;
    }

    /**
     * 作業区分を削除する。
     * @param processId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer processId) {
        //作業区分を削除する。
        int ret = processDao.deleteByPrimaryKey(processId);
        return ret;
    }
}
