package com.lf.bigdata.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.model.WhereCondition.Criteria;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import com.lf.bigdata.dao.mapper.StaffDao;
import com.lf.bigdata.dto.StaffDto;
import com.lf.bigdata.mapper.model.Staff;

/**
 * スタッフ情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class StaffService {
    @Autowired
    StaffDao staffDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーでスタッフを検索する。
     * @param staffId ID
     * @return 結果
     */
    public StaffDto selectByPrimaryKey(Integer staffId) {
        //プライマリーキーでスタッフを検索する。
        Staff ret = staffDao.selectByPrimaryKey(staffId);
        return DozerHelper.map(mapper, ret, StaffDto.class);
    }

    /**
     * プライマリーキーでスタッフを検索する。（連携情報含む）
     * @param staffId ID
     * @return 結果
     */
    public List<StaffDto> selectAllByPrimaryKey(Integer staffId) {
        //プライマリーキーでスタッフ検索する。（連携情報含む）
        List<Staff> ret = staffDao.selectAllByPrimaryKey(staffId);
        return DozerHelper.mapList(mapper, ret, StaffDto.class);
    }

    /**
     * 条件でスタッフを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffDto> selectByExample(StaffDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("staff.staff_id", dto.getStaffId())
            .andLike("staff.staff_code", dto.getStaffCode())
            .andEqualTo("staff.staff_encrypt_code", dto.getStaffEncryptCode())
            .andLike("staff.staff_name", dto.getStaffName())
            .andEqualTo("staff.staff_work_type", dto.getStaffWorkType())
            .andEqualTo("staff.staff_company_id", dto.getStaffCompanyId())
            .andEqualTo("staff.worker_number", dto.getWorkerNumber())
            .andEqualTo("staff.bussiness_grade", dto.getBussinessGrade())
            .andEqualTo("staff.remark", dto.getRemark())
            .andEqualTo("staff.black_status", dto.getBlackStatus())
            .andEqualTo("staff.status", dto.getStatus())
            .andEqualTo("staff.create_user", dto.getCreateUser())
            .andEqualTo("staff.update_user", dto.getUpdateUser())
            .andEqualTo("staff.create_time", dto.getCreateTime())
            .andEqualTo("staff.update_time", dto.getUpdateTime());
        }
        //条件でスタッフを検索する。（連携情報含む）)
        List<Staff> ret = staffDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, StaffDto.class);
    }

    /**
     * 条件でスタッフを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffDto> selectAllByExample(StaffDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("staff.staff_id", dto.getStaffId())
            .andLike("staff.staff_code", dto.getStaffCode())
            .andEqualTo("staff.staff_encrypt_code", dto.getStaffEncryptCode())
            .andLike("staff.staff_name", dto.getStaffName())
            .andEqualTo("staff.staff_work_type", dto.getStaffWorkType())
            .andEqualTo("staff.staff_company_id", dto.getStaffCompanyId())
            .andEqualTo("staff.worker_number", dto.getWorkerNumber())
            .andEqualTo("staff.bussiness_grade", dto.getBussinessGrade())
            .andEqualTo("staff.remark", dto.getRemark())
            .andEqualTo("staff.black_status", dto.getBlackStatus())
            .andEqualTo("staff.status", dto.getStatus())
            .andEqualTo("staff.create_user", dto.getCreateUser())
            .andEqualTo("staff.update_user", dto.getUpdateUser())
            .andEqualTo("staff.create_time", dto.getCreateTime())
            .andEqualTo("staff.update_time", dto.getUpdateTime());
        }
        //条件でスタッフを検索する。（連携情報含む）)
        List<Staff> ret = staffDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, StaffDto.class);
    }

    /**
     * スタッフを新規追加する。
     * @param dto スタッフ
     * @return 結果
     */
    public int insertSelective(StaffDto dto) {
        Staff staff  =  mapper.map(dto, Staff.class);
        //スタッフを新規追加する。
        int ret = staffDao.insertSelective(staff);
        return ret;
    }

    /**
     * スタッフを新規追加する。
     * @param dto スタッフ
     * @return 結果
     */
    public int insert(StaffDto dto) {
        Staff staff  =  mapper.map(dto, Staff.class);
        //スタッフを新規追加する。
        int ret = staffDao.insert(staff);
        return ret;
    }

    /**
     * プライマリーキーでスタッフを更新する。
     * @param dto スタッフ
     * @return 結果
     */
    public int updateByPrimaryKey(StaffDto dto) {
        Staff staff  =  mapper.map(dto, Staff.class);
        //プライマリーキーでスタッフを更新する。
        int ret = staffDao.updateByPrimaryKey(staff);
        return ret;
    }

    /**
     * プライマリーキーでスタッフを更新する。
     * @param dto スタッフ
     * @return 結果
     */
    public int updateByPrimaryKeySelective(StaffDto dto) {
        Staff staff  =  mapper.map(dto, Staff.class);
        //プライマリーキーでスタッフを更新する。
        int ret = staffDao.updateByPrimaryKeySelective(staff);
        return ret;
    }

    /**
     * スタッフを削除する。
     * @param staffId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer staffId) {
        //スタッフを削除する。
        int ret = staffDao.deleteByPrimaryKey(staffId);
        return ret;
    }

    public boolean staffIsExists(StaffDto staffDto) {


        WhereCondition whereCondition= new WhereCondition() ;
        Criteria criteria =  whereCondition.createCriteria();

        if(!StringUtils.isEmpty(staffDto.getStaffCode())){
            criteria.andEqualTo("staff_code", staffDto.getStaffCode());
        }

        if(!StringUtils.isEmpty(staffDto.getStaffEncryptCode())){
            criteria.andEqualTo("staff_encrypt_code", staffDto.getStaffEncryptCode());
        }

        if(staffDto.getStaffId()!=null && staffDto.getStaffId() > 0) {
            criteria.andNotEqualTo("staff_id", staffDto.getStaffId());
        }

        int count = staffDao.selectCountByExample(whereCondition);
        return count > 0;
    }

}