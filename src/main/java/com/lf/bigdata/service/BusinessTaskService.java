package com.lf.bigdata.service;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import com.lf.bigdata.dao.mapper.BusinessTaskDao;
import com.lf.bigdata.dto.BusinessTaskDto;
import com.lf.bigdata.mapper.model.BusinessTask;

/**
 * 作業業務情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class BusinessTaskService {
    @Autowired
    BusinessTaskDao businessTaskDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで作業業務を検索する。
     * @param businessId ID
     * @return 結果
     */
    public BusinessTaskDto selectByPrimaryKey(Integer businessId) {
        //プライマリーキーで作業業務を検索する。
        BusinessTask ret = businessTaskDao.selectByPrimaryKey(businessId);
        return DozerHelper.map(mapper, ret, BusinessTaskDto.class);
    }

    /**
     * プライマリーキーで作業業務を検索する。（連携情報含む）
     * @param businessId ID
     * @return 結果
     */
    public List<BusinessTaskDto> selectAllByPrimaryKey(Integer businessId) {
        //プライマリーキーで作業業務検索する。（連携情報含む）
        List<BusinessTask> ret = businessTaskDao.selectAllByPrimaryKey(businessId);
        return DozerHelper.mapList(mapper, ret, BusinessTaskDto.class);
    }

    /**
     * 条件で作業業務を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<BusinessTaskDto> selectByExample(BusinessTaskDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("business_task.business_id", dto.getBusinessId())
            .andEqualTo("business_task.business_name", dto.getBusinessName())
            .andEqualTo("business_task.business_name_cn", dto.getBusinessNameCn())
            .andEqualTo("business_task.business_code", dto.getBusinessCode())
            .andEqualTo("business_task.business_encrypt_code", dto.getBusinessEncryptCode())
            .andEqualTo("business_task.area", dto.getArea())
            .andEqualTo("business_task.process", dto.getProcess())
            .andEqualTo("business_task.mark", dto.getMark())
            .andEqualTo("business_task.charge_type", dto.getChargeType())
            .andEqualTo("business_task.business_type_id", dto.getBusinessTypeId())
            .andEqualTo("business_task.create_user", dto.getCreateUser())
            .andEqualTo("business_task.update_user", dto.getUpdateUser())
            .andEqualTo("business_task.create_time", dto.getCreateTime())
            .andEqualTo("business_task.update_time", dto.getUpdateTime());
        }
        //条件で作業業務を検索する。（連携情報含む）)
        List<BusinessTask> ret = businessTaskDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, BusinessTaskDto.class);
    }

    /**
     * 条件で作業業務を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<BusinessTaskDto> selectAllByExample(BusinessTaskDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("business_task.business_id", dto.getBusinessId())
            .andEqualTo("business_task.business_name", dto.getBusinessName())
            .andEqualTo("business_task.business_name_cn", dto.getBusinessNameCn())
            .andEqualTo("business_task.business_code", dto.getBusinessCode())
            .andEqualTo("business_task.business_encrypt_code", dto.getBusinessEncryptCode())
            .andEqualTo("business_task.area", dto.getArea())
            .andEqualTo("business_task.process", dto.getProcess())
            .andEqualTo("business_task.mark", dto.getMark())
            .andEqualTo("business_task.charge_type", dto.getChargeType())
            .andEqualTo("business_task.business_type_id", dto.getBusinessTypeId())
            .andEqualTo("business_task.create_user", dto.getCreateUser())
            .andEqualTo("business_task.update_user", dto.getUpdateUser())
            .andEqualTo("business_task.create_time", dto.getCreateTime())
            .andEqualTo("business_task.update_time", dto.getUpdateTime());
        }
        //条件で作業業務を検索する。（連携情報含む）)
        List<BusinessTask> ret = businessTaskDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, BusinessTaskDto.class);
    }

    /**
     * 作業業務を新規追加する。
     * @param dto 作業業務
     * @return 結果
     */
    public int insertSelective(BusinessTaskDto dto) {

        WhereCondition whereCondition= new WhereCondition() ;
        whereCondition.createCriteria()
        .andEqualTo("business_task.business_encrypt_code", dto.getBusinessEncryptCode());
        List<BusinessTask> list = businessTaskDao.selectAllByExample(whereCondition);
        if(CollectionUtils.isNotEmpty(list)) {
        	throw new UncheckedLogicException("errors.error.data_already_exist", "バーコード");
        }


        BusinessTask businessTask  =  mapper.map(dto, BusinessTask.class);
        //作業業務を新規追加する。
        int ret = businessTaskDao.insertSelective(businessTask);
        return ret;
    }

    /**
     * 作業業務を新規追加する。
     * @param dto 作業業務
     * @return 結果
     */
    public int insert(BusinessTaskDto dto) {
        BusinessTask businessTask  =  mapper.map(dto, BusinessTask.class);
        //作業業務を新規追加する。
        int ret = businessTaskDao.insert(businessTask);
        return ret;
    }

    /**
     * プライマリーキーで作業業務を更新する。
     * @param dto 作業業務
     * @return 結果
     */
    public int updateByPrimaryKey(BusinessTaskDto dto) {
        BusinessTask businessTask  =  mapper.map(dto, BusinessTask.class);
        //プライマリーキーで作業業務を更新する。
        int ret = businessTaskDao.updateByPrimaryKey(businessTask);
        return ret;
    }

    /**
     * プライマリーキーで作業業務を更新する。
     * @param dto 作業業務
     * @return 結果
     */
    public int updateByPrimaryKeySelective(BusinessTaskDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        whereCondition.createCriteria()
        .andEqualTo("business_task.business_encrypt_code", dto.getBusinessEncryptCode());
        List<BusinessTask> list = businessTaskDao.selectAllByExample(whereCondition);

		list.stream().filter(w -> !w.getBusinessId().equals(dto.getBusinessId())).findFirst().ifPresent(b -> {
			throw new UncheckedLogicException("errors.error.data_already_exist", "バーコード");

		});

        BusinessTask businessTask  =  mapper.map(dto, BusinessTask.class);
        //プライマリーキーで作業業務を更新する。
        int ret = businessTaskDao.updateByPrimaryKeySelective(businessTask);
        return ret;
    }

    /**
     * 作業業務を削除する。
     * @param businessId ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer businessId) {
        //作業業務を削除する。
        int ret = businessTaskDao.deleteByPrimaryKey(businessId);
        return ret;
    }

    public boolean businessIsExists(BusinessTaskDto businessTaskDto) {

        WhereCondition whereCondition = new WhereCondition();
        WhereCondition.Criteria criteria = whereCondition.createCriteria();

        if (!StringUtils.isEmpty(businessTaskDto.getBusinessCode())) {
            criteria.andEqualTo("business_code", businessTaskDto.getBusinessCode());
        }

        if (!StringUtils.isEmpty(businessTaskDto.getBusinessEncryptCode())) {
            criteria.andEqualTo("business_encrypt_code", businessTaskDto.getBusinessEncryptCode());
        }

        if (businessTaskDto.getBusinessId() != null && businessTaskDto.getBusinessId() > 0) {
            criteria.andNotEqualTo("business_id", businessTaskDto.getBusinessId());
        }

        int count = businessTaskDao.selectCountByExample(whereCondition);
        return count > 0;
    }

}