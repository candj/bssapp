package com.lf.bigdata.webservice.search;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.candj.webpower.web.core.exception.UncheckedNotFoundException;
import com.lf.bigdata.dto.StaffDto;
import com.lf.bigdata.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RequestMapping
@RestController
public class SearchStaffRestController {

    @Autowired
    StaffService staffService;

    @RequestMapping(value = "/workPlan/searchStaffByNumber", method = RequestMethod.POST)
    public StaffDto searchStaffByNumberForInsert(StaffDto dto) throws UncheckedLogicException {
        if(dto.getWorkerNumber() == ""){
            return null;
        }

        Optional<StaffDto> o = staffService.selectByExample(dto).stream().findFirst();

        if(o.isPresent()) {
            return o.get();
        }
        else{
            return null;
        }
    }

    @RequestMapping(value = "/workPlan/edit/searchStaffByNumber", method = RequestMethod.POST)
    public StaffDto searchStaffByNumberForEdit(StaffDto dto) throws UncheckedLogicException {
        if(dto.getWorkerNumber() == ""){
            return null;
        }

        Optional<StaffDto> o = staffService.selectByExample(dto).stream().findFirst();

        if(o.isPresent()) {
            return o.get();
        }
        else{
            return null;
        }
    }

}
