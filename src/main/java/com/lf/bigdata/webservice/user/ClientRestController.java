package com.lf.bigdata.webservice.user;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.candj.webpower.web.core.exception.NotValidException;
import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.candj.webpower.web.core.exception.UncheckedNotFoundException;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import com.lf.bigdata.dto.BusinessAttendanceDto;
import com.lf.bigdata.dto.BusinessTaskDto;
import com.lf.bigdata.dto.NotificationDto;
import com.lf.bigdata.dto.ProcessDto;
import com.lf.bigdata.dto.StaffDto;
import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.dto.WorkTaskDto;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;
import com.lf.bigdata.mapper.model.Notification;
import com.lf.bigdata.service.BusinessAttendanceService;
import com.lf.bigdata.service.BusinessTaskService;
import com.lf.bigdata.service.NotificationService;
import com.lf.bigdata.service.ProcessService;
import com.lf.bigdata.service.StaffService;
import com.lf.bigdata.strategy.WorkStrategyFactory;
import com.lf.bigdata.utils.DateUtils;

/**
 * スタッフ情報検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/clientRest")
@RestController
public class ClientRestController {

	@Autowired
	StaffService staffService;

	@Autowired
	BusinessTaskService businessTaskService;

	@Autowired
	BusinessAttendanceService businessAttendanceService;

	@Autowired
	WorkStrategyFactory factory;

	@Autowired
	ProcessService processService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	Mapper mapper;

	/**
	 * スタッフコードでスタッフレコードの存在をチェックする
	 *
	 * @param staffDto
	 *            スタッフ
	 * @param pageReq
	 *            改ページ情報
	 * @return 結果
	 */
	@RequestMapping(value = "/checkStaffCode", method = RequestMethod.POST)
	public StaffDto checkStaffCodeExists(String staffEncryptCode) throws UncheckedLogicException{

		StaffDto staffDto = new StaffDto();
		staffDto.setStaffEncryptCode(staffEncryptCode);

		Optional<StaffDto> o = staffService.selectByExample(staffDto).stream().findFirst();
		if (o.isPresent()) {
			return o.get();
		} else {
			throw new UncheckedNotFoundException("スタッフID");
		}
	}

	/**
	 * 業務コードの正確性をチェックする。
	 *
	 * @param workStaffId
	 *            スタッフ
	 * @param businessEncryptCode
	 *            改ページ情報
	 * @return 結果
	 * @throws ParseException
	 */
	@RequestMapping(value = "/checkBusinessCode", method = RequestMethod.POST)
	public Object businessCheckCode(String businessEncryptCode, Integer workStaffId) throws Exception{
		// 条件を設置する
		BusinessTaskDto cond1 = new BusinessTaskDto();

		cond1.setBusinessEncryptCode(businessEncryptCode);

		// 業務データを取得する
		//List<BusinessTaskDto> businessTaskDtos = businessTaskService.selectByExample(cond1);

		Optional<BusinessTaskDto> o = businessTaskService.selectByExample(cond1).stream().findFirst();
		if (o.isPresent()) {
			return o.get();
		} else {
			BusinessAttendanceDto cond2 = new BusinessAttendanceDto();
			cond2.setBusinessEncryptCode(businessEncryptCode);
			Optional<BusinessAttendanceDto> o2 = businessAttendanceService.selectByExample(cond2).stream().findFirst();
			if (o2.isPresent()) {
				return o2.get();
			} else {
				throw new UncheckedNotFoundException("打刻内容");
			}
		}
	}

	/**
	 * スタッフの業務履歴の追加する
	 *
	 * @param staffDto
	 *            スタッフ
	 * @param pageReq
	 *            改ページ情報
	 * @return 結果
	 * @throws Exception
	 */
	@RequestMapping(value = "/task/insert", method = RequestMethod.POST)
	public int insertTaskWork(WorkTaskDto workTaskDto) throws Exception {

		BusinessTaskDto bat = businessTaskService.selectByPrimaryKey(workTaskDto.getWorkBusinessId());

		if (bat == null) {
			throw new UncheckedNotFoundException("打刻内容");
		}

		workTaskDto.setBusinessTask(bat);

		StaffDto staff = staffService.selectByPrimaryKey(workTaskDto.getWorkStaffId());
		if (staff == null) {
			throw new UncheckedNotFoundException("スタッフID");
		}
		workTaskDto.setWorkCompanyId(staff.getStaffCompanyId());
		workTaskDto.setStaff(staff);

		workTaskDto.setWorkStartTime(DateUtils.getCurrentDateTime());

		return factory.getWorkStrategy(workTaskDto).insert(workTaskDto);
	}

	/**
	 * スタッフの業務履歴の追加する
	 *
	 * @param staffDto
	 *            スタッフ
	 * @param pageReq
	 *            改ページ情報
	 * @return 結果
	 * @throws Exception
	 */
	@RequestMapping(value = "/attendance/insert", method = RequestMethod.POST)
	public int insertWork(WorkAttendanceDto workAttendanceDto) throws Exception {
		workAttendanceDto.setRealWorkStartTime(DateUtils.getCurrentDateTime());
		workAttendanceDto.setWorkStartTime(workAttendanceDto.getRealWorkStartTime());
		preSaveWork(businessAttendanceService, staffService, workAttendanceDto);

		return factory.getWorkStrategy(workAttendanceDto).insert(workAttendanceDto);
	}

	private void preSaveWork(BusinessAttendanceService businessAttendanceService, StaffService staffService,
			WorkAttendanceDto workAttendanceDto) throws Exception {

		BusinessAttendanceDto bat = businessAttendanceService.selectByPrimaryKey(workAttendanceDto.getWorkBusinessId());

		if (bat == null) {
			throw new UncheckedNotFoundException("打刻内容");
		}

		workAttendanceDto.setBusinessAttendance(bat);

		StaffDto staff = staffService.selectByPrimaryKey(workAttendanceDto.getWorkStaffId());
		if (staff == null) {
			throw new UncheckedNotFoundException("スタッフID");
		}
		workAttendanceDto.setWorkCompanyId(staff.getStaffCompanyId());
		workAttendanceDto.setStaff(staff);
		if (BusinessTypeCodeEnum.starting.contains(bat.getBusinessTypeId())
				|| BusinessTypeCodeEnum.leaving.contains(bat.getBusinessTypeId())) {
			DateUtils.resetWorkingHours(workAttendanceDto);
		} else {
			if (workAttendanceDto.getWorkStartTime() == null) {
				workAttendanceDto.setWorkStartTime(DateUtils.getCurrentDateTime());
			}
			DateTime workEndTime = DateUtils.getRestEndTime(workAttendanceDto.getWorkStartTime(),
					bat.getBusinessTypeId());

			workAttendanceDto.setWorkEndTime(workEndTime);

		}

	}

	/**
	 * 業務区別を取得する
	 *
	 * @param processDto
	 *            業務区別
	 * @return 結果
	 * @throws Exception
	 */
	@RequestMapping(value = "/notification/process", method = RequestMethod.GET)
	public List<ProcessDto> process(ProcessDto processDto) throws Exception {

		processDto.setProcessStatus(true);
		List<ProcessDto> processDtoList = processService.selectByStatus(processDto);

		return processDtoList;
	}

	/**
	 * 業掲示板情報を取得する
	 *
	 * @param notificationDto
	 *            掲示板情報
	 * @return 結果
	 * @throws Exception
	 */
	@RequestMapping(value = "/notification/list/{notificationProcessId}", method = RequestMethod.GET)
	public List<NotificationDto> notificationList(NotificationDto notificationDto) throws Exception {

		List<NotificationDto> notificationDtoList = notificationService.selectByExample(notificationDto);

		return notificationDtoList;
	}

	/**
	 * 業掲示板情報内容を取得する
	 *
	 * @param notificationDto
	 *            掲示板情報
	 * @return 結果
	 * @throws Exception
	 */
	@RequestMapping(value = "/notification/detail/{notificationId}", method = RequestMethod.GET)
	public Notification notificationDetail(NotificationDto notificationDto, String processCode) throws Exception {

		Notification res = notificationService.selectByPrimaryKey(notificationDto.getNotificationId());

		return res;
	}

	/**
	 * 掲示板情報を変更する。
	 * @param notificationDto 掲示板情報
	 * @param bindingResult バンディング結果
	 * @return 結果
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/notification/save", method = RequestMethod.POST)
	public Notification save(NotificationDto notificationDto, Errors rrrors) throws Exception{
		validate(notificationDto);
		//プライマリーキーで掲示板情報を更新する。
		Notification res = new Notification();
		if(notificationDto.getNotificationId() == null){
			notificationDto.setNotificationOpenTime(new DateTime());
			int ret = notificationService.insertSelective(notificationDto);
			/////////////////////////res = notificationService.selectByTime().get(0);
		}
		else {
			Notification lastNo = notificationService.selectByPrimaryKey(notificationDto.getNotificationId());
			if(lastNo.getNotificationStatus() != notificationDto.getNotificationStatus()){
				notificationDto.setNotificationOpenTime(notificationDto.getNotificationStatus()? DateTime.now() : null);
			}
			int ret = notificationService.updateByPrimaryKeySelective(notificationDto);
			res = notificationService.selectByPrimaryKey(notificationDto.getNotificationId());
			ProcessDto processDto = processService.selectByPrimaryKey(res.getNotificationProcessId());
			res.setProcess(DozerHelper.map(mapper, processDto, com.lf.bigdata.mapper.model.Process.class));
			ProcessDto lastProcessDto = processService.selectByPrimaryKey(lastNo.getNotificationProcessId());
			//////////////////////////////////////res.setLastProcessId(lastProcessDto.getProcessId());
		}
		return res;
	}

	private void validate(NotificationDto notificationDto) throws Exception{
		if(notificationDto.getNotificationProcessId() == null || notificationDto.getNotificationProcessId().equals("")){
			throw new NotValidException("errors.error.validator.notEmpty", "業務区別");
		}
		if(notificationDto.getNotificationTitle() == null || notificationDto.getNotificationTitle().equals("")){
			throw new NotValidException("errors.error.validator.notEmpty", "タイトル");
		}
	}
}
