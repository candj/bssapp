package com.lf.bigdata.webservice;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lf.bigdata.dto.BusinessAttendanceDto;
import com.lf.bigdata.dto.BusinessTaskDto;
import com.lf.bigdata.service.BusinessAttendanceService;
import com.lf.bigdata.service.BusinessTaskService;

/**
 * 勤怠業務情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/businessAttendanceRest")
@RestController
public class BusinessAttendanceRestController {
    @Autowired
    BusinessAttendanceService businessAttendanceService;

    @Autowired
    BusinessTaskService businessTaskService;

    /**
     * 勤怠業務を新規追加する。
     * @param businessId ID
     * @return 結果
     */
    @RequestMapping(value = "{businessId}", method = RequestMethod.GET)
    public BusinessAttendanceDto get(@PathVariable Integer businessId) {
        //プライマリーキーで勤怠業務を検索する。
        BusinessAttendanceDto ret = businessAttendanceService.selectByPrimaryKey(businessId);
        return ret;
    }

    /**
     * 勤怠業務を新規追加する。
     * @param businessAttendanceDto 勤怠業務
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody BusinessAttendanceDto businessAttendanceDto, Errors rrrors) {
        //勤怠業務を新規追加する。
        int ret = businessAttendanceService.insertSelective(businessAttendanceDto);
        return ret;
    }

    /**
     * 勤怠業務を変更する。
     * @param businessAttendanceDto 勤怠業務
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody BusinessAttendanceDto businessAttendanceDto, Errors rrrors) {
        //プライマリーキーで勤怠業務を更新する。
        int ret = businessAttendanceService.updateByPrimaryKeySelective(businessAttendanceDto);
        return ret;
    }

    /**
     * 勤怠業務を新規追加する。
     * @param businessId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{businessId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer businessId) {
        //勤怠業務を削除する。
        int ret = businessAttendanceService.deleteByPrimaryKey(businessId);
        return ret;
    }

    /**
     * 勤怠業務一覧画面を表示する。
     * @param businessAttendanceDto 勤怠業務
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<BusinessAttendanceDto> list(BusinessAttendanceDto businessAttendanceDto, JqgridPageReq pageReq) {
    	Page<BusinessAttendanceDto> page = PageUtil.startPage(pageReq);
        //条件で勤怠業務を検索する。（連携情報含む）
        businessAttendanceService.selectByExample(businessAttendanceDto);
        return PageUtil.resp(page);
    }

    /**
     * 勤怠業務一覧画面を表示する。
     * @param businessAttendanceDto 勤怠業務
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<BusinessAttendanceDto> listAll(BusinessAttendanceDto businessAttendanceDto, JqgridPageReq pageReq) {
    	Page<BusinessAttendanceDto> page = PageUtil.startPage(pageReq);
        //条件で勤怠業務を検索する。（連携情報含む）
        businessAttendanceService.selectAllByExample(businessAttendanceDto);
        return PageUtil.resp(page);
    }

    /**
     * 業務コード唯一の検査
     *
     * @param businessCode
     *            スタッフ
     * @param id
     *            改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/businessCodeUnique/{id}", method = RequestMethod.GET)
    public Object businessCodeUnique(@PathVariable Integer id,String businessCode) {

        BusinessTaskDto businessTaskDto = new BusinessTaskDto();
        BusinessAttendanceDto businessAttendanceDto = new BusinessAttendanceDto();
        businessTaskDto.setBusinessId(id);
        businessAttendanceDto.setBusinessId(id);
        businessTaskDto.setBusinessCode(businessCode);
        businessAttendanceDto.setBusinessCode(businessCode);
        if( businessTaskService.businessIsExists(businessTaskDto)) {
            return "businessCodeExists";
        }
        if( businessAttendanceService.businessIsExists(businessAttendanceDto)) {
            return "businessCodeExists";
        }
        return true;
    }

    /**
     * 業務コード唯一の検査
     *
     * @param businessEncryptCode
     *            スタッフ
     * @param id
     *            改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/businessEncryptCodeUnique/{id}", method = RequestMethod.GET)
    public Object staffEncryptCodeUnique(@PathVariable Integer id,String businessEncryptCode) {


        BusinessTaskDto businessTaskDto = new BusinessTaskDto();
        BusinessAttendanceDto businessAttendanceDto = new BusinessAttendanceDto();
        businessTaskDto.setBusinessId(id);
        businessAttendanceDto.setBusinessId(id);
        businessTaskDto.setBusinessEncryptCode(businessEncryptCode);
        businessAttendanceDto.setBusinessEncryptCode(businessEncryptCode);
        if( businessTaskService.businessIsExists(businessTaskDto)) {
            return "businessEncryptCodeExists";
        }
        if( businessAttendanceService.businessIsExists(businessAttendanceDto)) {
            return "businessEncryptCodeExists";
        }
        return true;
    }
}