package com.lf.bigdata.webservice;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lf.bigdata.dto.NotificationDto;
import com.lf.bigdata.mapper.model.Notification;
import com.lf.bigdata.service.NotificationService;

/**
 * 掲示板情報情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/notificationRest")
@RestController
public class NotificationRestController {
    @Autowired
    NotificationService notificationService;

    /**
     * 掲示板情報を新規追加する。
     * @param notificationId ID
     * @return 結果
     */
    @RequestMapping(value = "{notificationId}", method = RequestMethod.GET)
    public Notification get(@PathVariable Integer notificationId) {
        //プライマリーキーで掲示板情報を検索する。
        Notification ret = notificationService.selectByPrimaryKey(notificationId);
        return ret;
    }

    /**
     * 掲示板情報を新規追加する。
     * @param notificationDto 掲示板情報
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody NotificationDto notificationDto, Errors rrrors) {
        //掲示板情報を新規追加する。
        int ret = notificationService.insertSelective(notificationDto);
        return ret;
    }

    /**
     * 掲示板情報を変更する。
     * @param notificationDto 掲示板情報
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody NotificationDto notificationDto, Errors rrrors) {
        //プライマリーキーで掲示板情報を更新する。
        int ret = notificationService.updateByPrimaryKeySelective(notificationDto);
        return ret;
    }

    /**
     * 掲示板情報を新規追加する。
     * @param notificationId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{notificationId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer notificationId) {
        //掲示板情報を削除する。
        int ret = notificationService.deleteByPrimaryKey(notificationId);
        return ret;
    }

    /**
     * 掲示板情報一覧画面を表示する。
     * @param notificationDto 掲示板情報
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<NotificationDto> list(NotificationDto notificationDto, JqgridPageReq pageReq) {
        Page<NotificationDto> page = PageUtil.startPage(pageReq);
        //条件で掲示板情報を検索する。（連携情報含む）
        List<NotificationDto> ret = notificationService.selectByExample(notificationDto);
        return PageUtil.resp(page);
    }

    /**
     * 掲示板情報一覧画面を表示する。
     * @param notificationDto 掲示板情報
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<NotificationDto> listAll(NotificationDto notificationDto, JqgridPageReq pageReq) {
        Page<NotificationDto> page = PageUtil.startPage(pageReq);
        //条件で掲示板情報を検索する。（連携情報含む）
        List<NotificationDto> ret = notificationService.selectAllByExample(notificationDto);
        return PageUtil.resp(page);
    }
}
