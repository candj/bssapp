package com.lf.bigdata.webservice;

import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lf.bigdata.dto.BusinessTaskDto;
import com.lf.bigdata.dto.StaffDto;
import com.lf.bigdata.dto.WorkTaskDto;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;
import com.lf.bigdata.service.BusinessTaskService;
import com.lf.bigdata.service.StaffService;
import com.lf.bigdata.service.WorkTaskService;
import com.lf.bigdata.strategy.WorkStrategyFactory;

/**
 * スタッフ作業情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/workTaskRest")
@RestController
public class WorkTaskRestController {
	@Autowired
	WorkTaskService workTaskService;

	@Autowired
	BusinessTaskService businessTaskService;

	@Autowired
	StaffService staffService;

	@Autowired
	WorkStrategyFactory factory;

	/**
	 * スタッフ作業を新規追加する。
	 * @param workId ID
	 * @return 結果
	 */
	@RequestMapping(value = "{workId}", method = RequestMethod.GET)
	public WorkTaskDto get(@PathVariable Integer workId) {
		//プライマリーキーでスタッフ作業を検索する。
		WorkTaskDto ret = workTaskService.selectAllByPrimaryKey(workId);
		return ret;
	}

	/**
	 * スタッフ作業を新規追加する。
	 * @param workTaskDto スタッフ作業
	 * @param bindingResult バンディング結果
	 * @return 結果
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	public int insert(@Valid @RequestBody WorkTaskDto workTaskDto, Errors rrrors) throws Exception {
		//スタッフ作業を新規追加する。
		preSaveWork(businessTaskService, staffService, workTaskDto);
		return factory.getWorkStrategy(workTaskDto).insert(workTaskDto);
	}

	/**
	 * スタッフ作業を変更する。
	 * @param workTaskDto スタッフ作業
	 * @param bindingResult バンディング結果
	 * @return 結果
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.PUT)
	public int update(@Valid @RequestBody WorkTaskDto workTaskDto, Errors rrrors) throws Exception {
		//プライマリーキーでスタッフ作業を更新する。
		preSaveWork(businessTaskService, staffService, workTaskDto);
		return factory.getWorkStrategy(workTaskDto).update(workTaskDto, workTaskDto.getWorkId());
	}

	/**
	 * スタッフ作業を新規追加する。
	 * @param workId ID
	 * @return 結果
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = "{workId}", method = RequestMethod.DELETE)
	public int delete(@PathVariable Integer workId) throws Exception {
		//スタッフ作業を削除する。
		WorkTaskDto dto = workTaskService.selectAllByPrimaryKey(workId);
		return factory.getWorkStrategy(dto).remove(dto, dto.getWorkId());
	}

	/**
	 * スタッフ作業一覧画面を表示する。
	 * @param workTaskDto スタッフ作業
	 * @param pageReq 改ページ情報
	 * @return 結果
	 */
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public PageInfo<WorkTaskDto> list(@RequestParam(name = "staffCode", required = false) String staffCode,
			@RequestParam(name = "workStartTime", required = false) DateTime workStartTime,
			@RequestParam(name = "staffName", required = false) String staffName, JqgridPageReq pageReq)
			throws ParseException {
		Page<WorkTaskDto> page = PageUtil.startPage(pageReq);
		//条件でスタッフ出勤を検索する。（連携情報含む）
		StaffDto staffDto = new StaffDto();
		staffDto.setStaffCode(staffCode);
		staffDto.setStaffName(staffName);
		workTaskService.selectOneDayWorkByExample(staffCode, staffName, workStartTime);
		return PageUtil.resp(page);
	}

	/**
	 * スタッフ作業一覧画面を表示する。
	 * @param workTaskDto スタッフ作業
	 * @param pageReq 改ページ情報
	 * @return 結果
	 */
	@RequestMapping(value = "/listAll", method = RequestMethod.POST)
	public PageInfo<WorkTaskDto> listAll(WorkTaskDto workTaskDto, JqgridPageReq pageReq) {
		Page<WorkTaskDto> page = PageUtil.startPage(pageReq);
		//条件でスタッフ作業を検索する。（連携情報含む）
		workTaskService.selectAllByExample(workTaskDto);
		return PageUtil.resp(page);
	}

	public static WorkTaskDto preSaveWork(BusinessTaskService businessTaskService, StaffService staffService,
			WorkTaskDto workTaskDto) throws UncheckedLogicException {

		DateTime dt = workTaskDto.getWorkStartTime();
		dt = dt.withField(DateTimeFieldType.year(), workTaskDto.getWorkDate().getYear())
				.withField(DateTimeFieldType.monthOfYear(), workTaskDto.getWorkDate().getMonthOfYear())
				.withField(DateTimeFieldType.dayOfMonth(), workTaskDto.getWorkDate().getDayOfMonth());
		
		workTaskDto.setWorkStartTime(dt);
		
		
		BusinessTaskDto bat = businessTaskService.selectByPrimaryKey(workTaskDto.getWorkBusinessId());
		if (bat == null) {
			throw new UncheckedLogicException("errors.error.business_code_not_exist");
		}
		if (!BusinessTypeCodeEnum.task.contains(bat.getBusinessTypeId())) {
			throw new UncheckedLogicException("errors.error.business_code_not_exist");
		}
		//WorkDto workDto = new WorkDto();
		workTaskDto.setBusinessTask(bat);

		StaffDto staffDto = new StaffDto();
		staffDto.setStaffCode(workTaskDto.getStaffCode());

		List<StaffDto> staffs = staffService.selectByExample(staffDto);
		if (CollectionUtils.isEmpty(staffs)) {
			throw new UncheckedLogicException("errors.error.staff_not_exist");
		}

		workTaskDto.setWorkCompanyId(staffs.get(0).getStaffCompanyId());
		workTaskDto.setStaff(staffs.get(0));
		//BeanUtils.copyProperties(workTaskDto, workDto);
		workTaskDto.setWorkStaffId(staffs.get(0).getStaffId());
		workTaskDto.setWorkBusinessId(workTaskDto.getBusinessTask().getBusinessId());

		return workTaskDto;
	}

}