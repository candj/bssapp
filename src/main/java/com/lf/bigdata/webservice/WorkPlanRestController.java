package com.lf.bigdata.webservice;

import com.candj.webpower.web.core.exception.BusinessLogicException;
import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lf.bigdata.dto.StaffDto;
import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.dto.WorkPlanDto;
import com.lf.bigdata.mapper.model.WorkPlan;
import com.lf.bigdata.service.StaffService;
import com.lf.bigdata.service.WorkPlanService;
import java.util.List;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * 作業予定情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/workPlanRest")
@RestController
public class WorkPlanRestController {
    @Autowired
    WorkPlanService workPlanService;

    @Autowired
    StaffService staffService;

    /**
     * 作業予定を新規追加する。
     * @param workPlanId 作業予定ID
     * @return 結果
     */
    @RequestMapping(value = "{workPlanId}", method = RequestMethod.GET)
    public WorkPlanDto get(@PathVariable Integer workPlanId) {
        //プライマリーキーで作業予定を検索する。
        WorkPlanDto ret = workPlanService.selectByPrimaryKey(workPlanId);
        return ret;
    }

    /**
     * 作業予定を新規追加する。
     * @param workPlanDto 作業予定
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody WorkPlanDto workPlanDto, Errors rrrors) {
        //作業予定を新規追加する。
        preSaveWorkPlan(workPlanDto);
        int ret = workPlanService.insertSelective(workPlanDto);
        return ret;
    }

    /**
     * 作業予定を変更する。
     * @param workPlanDto 作業予定
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody WorkPlanDto workPlanDto, Errors rrrors){
        //プライマリーキーで作業予定を更新する。
        int ret = workPlanService.updateByPrimaryKeySelective(workPlanDto);
        return ret;
    }

    /**
     * 作業予定を新規追加する。
     * @param workPlanId 作業予定ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{workPlanId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer workPlanId) {
        //作業予定を削除する。
        int ret = workPlanService.deleteByPrimaryKey(workPlanId);
        return ret;
    }

    /**
     * 作業予定一覧画面を表示する。
     * @param workPlanDto 作業予定
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<WorkPlanDto> list(WorkPlanDto workPlanDto, JqgridPageReq pageReq) {
        Page<WorkPlanDto> page = PageUtil.startPage(pageReq);
        //条件で作業予定を検索する。（連携情報含む）
        List<WorkPlanDto> ret = workPlanService.selectAllByExample(workPlanDto);
        return PageUtil.resp(page);
    }

    /**
     * 作業予定一覧画面を表示する。
     * @param workPlanDto 作業予定
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<WorkPlanDto> listAll(WorkPlanDto workPlanDto, JqgridPageReq pageReq) {
        Page<WorkPlanDto> page = PageUtil.startPage(pageReq);
        //条件で作業予定を検索する。（連携情報含む）
        List<WorkPlanDto> ret = workPlanService.selectAllByExample(workPlanDto);
        return PageUtil.resp(page);
    }

    private WorkPlanDto preSaveWorkPlan(WorkPlanDto workPlanDto) {
        if(workPlanDto.getWorkPlanStaffId() == null){
            throw new UncheckedLogicException("errors.error.staff_number_not_found_error");
        }
        if(workPlanDto.getWorkPlanCompanyId() == null){
            throw new UncheckedLogicException("errors.error.staff_number_not_found_error");
        }
        WorkPlanDto dto = new WorkPlanDto();
        dto.setWorkPlanStaffId(workPlanDto.getWorkPlanStaffId());
        dto.setWorkPlanDate(workPlanDto.getWorkPlanDate());
        if(!CollectionUtils.isEmpty(workPlanService.selectAllByExample(dto))){
            throw new UncheckedLogicException("errors.error.staff_work_plan_repeat");
        }
        return workPlanDto;
    }
}
