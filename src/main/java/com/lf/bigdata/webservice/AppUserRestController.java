package com.lf.bigdata.webservice;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lf.bigdata.dto.AppUserDto;
import com.lf.bigdata.service.AppUserService;

/**
 * ユーザー情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/appUserRest")
@RestController
public class AppUserRestController {
    @Autowired
    AppUserService appUserService;

    /**
     * ユーザーを新規追加する。
     * @param userId ユーザーID
     * @return 結果
     */
    @RequestMapping(value = "{userId}", method = RequestMethod.GET)
    public AppUserDto get(@PathVariable Integer userId) {
        //プライマリーキーでユーザーを検索する。
        AppUserDto ret = appUserService.selectByPrimaryKey(userId);
        return ret;
    }

    /**
     * ユーザーを新規追加する。
     * @param appUserDto ユーザー
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody AppUserDto appUserDto, Errors rrrors) {
        //ユーザーを新規追加する。
        int ret = appUserService.insertSelective(appUserDto);
        return ret;
    }

    /**
     * ユーザーを変更する。
     * @param appUserDto ユーザー
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody AppUserDto appUserDto, Errors rrrors) {
        //プライマリーキーでユーザーを更新する。
    	if(StringUtils.isEmpty(appUserDto.getPassword())) {
    		appUserDto.setPassword(null);
    	}
        int ret = appUserService.updateByPrimaryKeySelective(appUserDto);
        return ret;
    }

    /**
     * ユーザーを新規追加する。
     * @param userId ユーザーID
     * @return 結果
     */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = "{userId}", method = RequestMethod.DELETE)
	public int delete(@PathVariable Integer userId) throws Exception {
        //ユーザーを削除する。
        int ret = appUserService.deleteByPrimaryKey(userId);
        return ret;
    }

    /**
     * ユーザー一覧画面を表示する。
     * @param appUserDto ユーザー
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<AppUserDto> list(AppUserDto appUserDto, JqgridPageReq pageReq) {
    	Page<AppUserDto> page = PageUtil.startPage(pageReq);
        //条件でユーザーを検索する。（連携情報含む）
        appUserService.selectByExample(appUserDto);
        return PageUtil.resp(page);
    }

    /**
     * ユーザー一覧画面を表示する。
     * @param appUserDto ユーザー
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<AppUserDto> listAll(AppUserDto appUserDto, JqgridPageReq pageReq) {
    	Page<AppUserDto> page = PageUtil.startPage(pageReq);
        //条件でユーザーを検索する。（連携情報含む）
        appUserService.selectAllByExample(appUserDto);
        return PageUtil.resp(page);
    }
}