package com.lf.bigdata.webservice;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lf.bigdata.dto.BusinessTaskDto;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;
import com.lf.bigdata.service.BusinessTaskService;

/**
 * 作業業務情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/businessTaskRest")
@RestController
public class BusinessTaskRestController {
    @Autowired
    BusinessTaskService businessTaskService;

    /**
     * 作業業務を新規追加する。
     * @param businessId ID
     * @return 結果
     */
    @RequestMapping(value = "{businessId}", method = RequestMethod.GET)
    public BusinessTaskDto get(@PathVariable Integer businessId) {
        //プライマリーキーで作業業務を検索する。
        BusinessTaskDto ret = businessTaskService.selectByPrimaryKey(businessId);
        return ret;
    }

    /**
     * 作業業務を新規追加する。
     * @param businessTaskDto 作業業務
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody BusinessTaskDto businessTaskDto, Errors rrrors) {
        //作業業務を新規追加する。
        businessTaskDto.setBusinessTypeId(BusinessTypeCodeEnum.task_work);
        int ret = businessTaskService.insertSelective(businessTaskDto);
        return ret;
    }

    /**
     * 作業業務を変更する。
     * @param businessTaskDto 作業業務
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody BusinessTaskDto businessTaskDto, Errors rrrors) {
        //プライマリーキーで作業業務を更新する。
        int ret = businessTaskService.updateByPrimaryKeySelective(businessTaskDto);
        return ret;
    }

    /**
     * 作業業務を新規追加する。
     * @param businessId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{businessId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer businessId) {
        //作業業務を削除する。
        int ret = businessTaskService.deleteByPrimaryKey(businessId);
        return ret;
    }

    /**
     * 作業業務一覧画面を表示する。
     * @param businessTaskDto 作業業務
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<BusinessTaskDto> list(BusinessTaskDto businessTaskDto, JqgridPageReq pageReq) {
    	Page<BusinessTaskDto> page = PageUtil.startPage(pageReq);
        //条件で作業業務を検索する。（連携情報含む）
        businessTaskService.selectByExample(businessTaskDto);
        return PageUtil.resp(page);
    }

    /**
     * 作業業務一覧画面を表示する。
     * @param businessTaskDto 作業業務
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<BusinessTaskDto> listAll(BusinessTaskDto businessTaskDto, JqgridPageReq pageReq) {
    	Page<BusinessTaskDto> page =  PageUtil.startPage(pageReq);
        //条件で作業業務を検索する。（連携情報含む）
        businessTaskService.selectAllByExample(businessTaskDto);
        return PageUtil.resp(page);
    }
}