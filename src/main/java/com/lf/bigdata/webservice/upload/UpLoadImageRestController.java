package com.lf.bigdata.webservice.upload;


import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RequestMapping
@RestController
public class UpLoadImageRestController {

    /**上传地址*/
    @Value("${file.upload.path}")
    String filePath;

    @RequestMapping(value = "/notification/upLoadImage", method = RequestMethod.POST)
    public JSONObject upLoadImage(@RequestParam("image") MultipartFile image, HttpServletRequest request) {

        JSONObject map = new JSONObject();
        JSONObject data = new JSONObject();
        JSONArray map2 = new JSONArray();

        String separator = System.getProperty("file.separator");
        separator = separator.replaceAll("\\\\","/");
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() +
                        request.getContextPath()+ separator; //获取项目路径+端口号 比如：http://localhost:8080/


        if (image != null) {
            try {
                String fileName = image.getOriginalFilename();
                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                String realPath = filePath + uuid + fileName;
                File dest = new File(realPath);
                //フォルダがいないの場合、新規する
                if (!dest.getParentFile().exists()) {
                    dest.getParentFile().mkdirs();
                }
                image.transferTo(dest);

                map.put("errno",0);//0表示成功，1失败
                data.put("url",basePath + "images/" + uuid + fileName);//图片url
                map2.add(data);
                map.put("data",map2);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }else{
            map.put("code",1);//0表示成功，1失败
            map.put("data",map2);
            data.put("url","");//图片url
            map2.add(data);
        }

        String result = map.toString();
        StringEscapeUtils.unescapeJava(result);
        return map;
    }

}
