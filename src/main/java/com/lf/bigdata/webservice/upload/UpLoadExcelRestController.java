package com.lf.bigdata.webservice.upload;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.lf.bigdata.dto.CompanyDto;
import com.lf.bigdata.dto.WorkPlanDto;
import com.lf.bigdata.enums.CompanyName;
import com.lf.bigdata.mapper.model.WorkPlan;
import com.lf.bigdata.service.BusinessAttendanceService;
import com.lf.bigdata.service.CompanyService;
import com.lf.bigdata.service.WorkPlanService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringEscapeUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

@RequestMapping
@RestController
public class UpLoadExcelRestController {

    @Autowired
    WorkPlanService workPlanService;

    @Autowired
    CompanyService companyService;

    /**上传地址*/
    @Value("${file.upload.path}")
    String filePath;

    @RequestMapping(value = "/workPlan/upLoadExcel", method = RequestMethod.POST)
    public String upLoadExcel(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception{

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        df.setLenient(false);
        if (file != null) {

            String fileName = file.getOriginalFilename();


                String fileNames[] = fileName.split("_");
                DateTime date = null;
                if(fileNames.length != 3){
                    throw new UncheckedLogicException("errors.error.file_name_not_validate");
                }
                try {
                    date = new DateTime(df.parse(fileNames[1]));
                } catch(ParseException e){
                    throw new UncheckedLogicException("errors.error.file_name_not_validate");
                }
                //会社を取得する
                StringTokenizer stringTokenizer = new StringTokenizer(fileNames[2], ".");
                String companyCode = stringTokenizer.nextToken();
                CompanyDto dto = new CompanyDto();
                dto.setCompanyCode(companyCode);
                List<CompanyDto> companyDtoList = companyService.selectByExample(dto);
                if(companyDtoList.isEmpty()) {
                    throw new UncheckedLogicException("errors.error.file_name_not_validate");
                }
                CompanyDto companyDto = companyService.selectByExample(dto).get(0);
                workPlanService.getWorkPlan(file.getInputStream(), date, companyDto.getCompanyId());


        }

        return "success";
    }

}
