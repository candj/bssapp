package com.lf.bigdata.webservice;

import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lf.bigdata.dto.StaffDto;
import com.lf.bigdata.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * ブラックリスト情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/blackRest")
@RestController
public class BlackListRestController {
    @Autowired
    StaffService staffService;

    /**
     * スタッフを新規追加する。
     * @param staffId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{staffId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer staffId) {
        //プライマリーキーでスタッフを更新する。
        StaffDto staffDto = staffService.selectByPrimaryKey(staffId);
        staffDto.setBlackStatus(false);
        int ret = staffService.updateByPrimaryKeySelective(staffDto);
        return ret;
    }

    /**
     * ブラックリスト一覧画面を表示する。
     * @param staffDto スタッフ
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<StaffDto> listAll(StaffDto staffDto, JqgridPageReq pageReq) {
        Page<StaffDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフを検索する。（連携情報含む）
        staffDto.setBlackStatus(true);
        staffService.selectAllByExample(staffDto);
        PageInfo<StaffDto> pageInfo = PageUtil.resp(page);
        return pageInfo;
    }
}
