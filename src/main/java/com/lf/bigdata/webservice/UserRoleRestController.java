package com.lf.bigdata.webservice;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lf.bigdata.dto.UserRoleDto;
import com.lf.bigdata.service.UserRoleService;

/**
 * ユーザールール情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/userRoleRest")
@RestController
public class UserRoleRestController {
    @Autowired
    UserRoleService userRoleService;

    /**
     * ユーザールールを新規追加する。
     * @param id ID
     * @return 結果
     */
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public UserRoleDto get(@PathVariable Integer id) {
        //プライマリーキーでユーザールールを検索する。
        UserRoleDto ret = userRoleService.selectByPrimaryKey(id);
        return ret;
    }

    /**
     * ユーザールールを新規追加する。
     * @param userRoleDto ユーザールール
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody UserRoleDto userRoleDto, Errors rrrors) {
        //ユーザールールを新規追加する。
        int ret = userRoleService.insertSelective(userRoleDto);
        return ret;
    }

    /**
     * ユーザールールを変更する。
     * @param userRoleDto ユーザールール
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody UserRoleDto userRoleDto, Errors rrrors) {
        //プライマリーキーでユーザールールを更新する。
        int ret = userRoleService.updateByPrimaryKeySelective(userRoleDto);
        return ret;
    }

    /**
     * ユーザールールを新規追加する。
     * @param id ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer id) {
        //ユーザールールを削除する。
        int ret = userRoleService.deleteByPrimaryKey(id);
        return ret;
    }

    /**
     * ユーザールール一覧画面を表示する。
     * @param userRoleDto ユーザールール
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<UserRoleDto> list(UserRoleDto userRoleDto, JqgridPageReq pageReq) {
    	Page<UserRoleDto> page = PageUtil.startPage(pageReq);
        //条件でユーザールールを検索する。（連携情報含む）
        userRoleService.selectByExample(userRoleDto);
        return PageUtil.resp(page);
    }

    /**
     * ユーザールール一覧画面を表示する。
     * @param userRoleDto ユーザールール
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<UserRoleDto> listAll(UserRoleDto userRoleDto, JqgridPageReq pageReq) {
    	Page<UserRoleDto> page = PageUtil.startPage(pageReq);
        //条件でユーザールールを検索する。（連携情報含む）
        userRoleService.selectAllByExample(userRoleDto);
        return PageUtil.resp(page);
    }
}