package com.lf.bigdata.webservice;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lf.bigdata.dto.StaffDto;
import com.lf.bigdata.service.StaffService;

/**
 * スタッフ情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/staffRest")
@RestController
public class StaffRestController {
    @Autowired
    StaffService staffService;

    /**
     * スタッフを新規追加する。
     * @param staffId ID
     * @return 結果
     */
    @RequestMapping(value = "{staffId}", method = RequestMethod.GET)
    public StaffDto get(@PathVariable Integer staffId) {
        //プライマリーキーでスタッフを検索する。
        StaffDto ret = staffService.selectByPrimaryKey(staffId);
        return ret;
    }

    /**
     * スタッフを新規追加する。
     * @param staffDto スタッフ
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody StaffDto staffDto, Errors rrrors) {
        //スタッフを新規追加する。
        int ret = staffService.insertSelective(staffDto);
        return ret;
    }

    /**
     * スタッフを変更する。
     * @param staffDto スタッフ
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody StaffDto staffDto, Errors rrrors) {
        //プライマリーキーでスタッフを更新する。
        int ret = staffService.updateByPrimaryKeySelective(staffDto);
        return ret;
    }

    /**
     * スタッフを新規追加する。
     * @param staffId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{staffId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer staffId) {
        //スタッフを削除する。
        int ret = staffService.deleteByPrimaryKey(staffId);
        return ret;
    }

    /**
     * スタッフ一覧画面を表示する。
     * @param staffDto スタッフ
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<StaffDto> list(StaffDto staffDto, JqgridPageReq pageReq) {
    	Page<StaffDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフを検索する。（連携情報含む）
        staffService.selectByExample(staffDto);
        return PageUtil.resp(page);
    }

    /**
     * スタッフ一覧画面を表示する。
     * @param staffDto スタッフ
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<StaffDto> listAll(StaffDto staffDto, JqgridPageReq pageReq) {
    	Page<StaffDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフを検索する。（連携情報含む）
        staffDto.setBlackStatus(null);
        staffService.selectAllByExample(staffDto);
        PageInfo<StaffDto> pageInfo = PageUtil.resp(page);
        return pageInfo;
    }

    /**
     * スタッフコード唯一の検査
     *
     * @param staffDto
     *            スタッフ
     * @param pageReq
     *            改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/staffCodeUnique/{id}", method = RequestMethod.GET)
    public Object staffCodeUnique(@PathVariable Integer id,String staffCode) {

        StaffDto staffDto = new StaffDto();
        staffDto.setStaffId(id);
        staffDto.setStaffCode(staffCode);
        if( staffService.staffIsExists(staffDto)) {
            return "staffCodeExists";
        }
        return true;
    }

    /**
     * スタッフコード唯一の検査
     *
     * @param staffDto
     *            スタッフ
     * @param pageReq
     *            改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/staffEncryptCodeUnique/{id}", method = RequestMethod.GET)
    public Object staffEncryptCodeUnique(@PathVariable Integer id,String staffEncryptCode) {

        StaffDto staffDto = new StaffDto();
        staffDto.setStaffId(id);
        staffDto.setStaffEncryptCode(staffEncryptCode);
        if( staffService.staffIsExists(staffDto)) {
            return "staffEncryptCodeExists";
        }
        return true;
    }

}