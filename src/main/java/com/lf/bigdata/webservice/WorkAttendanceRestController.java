package com.lf.bigdata.webservice;

import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lf.bigdata.dto.BusinessAttendanceDto;
import com.lf.bigdata.dto.StaffDto;
import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.dto.WorkDto;
import com.lf.bigdata.dto.WorkTaskDto;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;
import com.lf.bigdata.service.BusinessAttendanceService;
import com.lf.bigdata.service.StaffService;
import com.lf.bigdata.service.WorkAttendanceService;
import com.lf.bigdata.service.WorkTaskService;
import com.lf.bigdata.strategy.WorkStrategyFactory;
import com.lf.bigdata.utils.DateUtils;

/**
 * スタッフ出勤情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/workAttendanceRest")
@RestController
public class WorkAttendanceRestController {
    @Autowired
    WorkAttendanceService workAttendanceService;

	@Autowired
	WorkTaskService workTaskService;

    @Autowired
    BusinessAttendanceService businessAttendanceService;

    @Autowired
    StaffService staffService;

    @Autowired
    WorkStrategyFactory factory;

    /**
     * スタッフ出勤を新規追加する。
     * @param workId ID
     * @return 結果
     */
    @RequestMapping(value = "{workId}", method = RequestMethod.GET)
    public WorkAttendanceDto get(@PathVariable Integer workId) {
        //プライマリーキーでスタッフ出勤を検索する。
        WorkAttendanceDto ret = workAttendanceService.selectAllByPrimaryKey(workId);
        return ret;
    }

    /**
     * スタッフ出勤を新規追加する。
     * @param workAttendanceDto スタッフ出勤
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody WorkAttendanceDto workAttendanceDto, Errors rrrors) throws Exception {
        //スタッフ出勤を新規追加する。
       // workAttendanceDto.setRealWorkStartTime(new DateTime(Calendar.getInstance().getTime()));
		DateTime dt = workAttendanceDto.getWorkStartTime();
		dt = dt.withField(DateTimeFieldType.year(), workAttendanceDto.getWorkDate().getYear())
				.withField(DateTimeFieldType.monthOfYear(), workAttendanceDto.getWorkDate().getMonthOfYear())
				.withField(DateTimeFieldType.dayOfMonth(), workAttendanceDto.getWorkDate().getDayOfMonth());
		workAttendanceDto.setWorkStartTime(dt);

		if(workAttendanceDto.getRealWorkStartTime() != null) {
			dt = workAttendanceDto.getRealWorkStartTime();
			dt = dt.withField(DateTimeFieldType.year(), workAttendanceDto.getWorkDate().getYear())
			.withField(DateTimeFieldType.monthOfYear(), workAttendanceDto.getWorkDate().getMonthOfYear())
			.withField(DateTimeFieldType.dayOfMonth(), workAttendanceDto.getWorkDate().getDayOfMonth());
			workAttendanceDto.setRealWorkStartTime(dt);
		}

        preSaveWork(workAttendanceDto);
        //workAttendanceDto.setWorkStartTime(workAttendanceDto.getRealWorkStartTime());
        return factory.getWorkStrategy(workAttendanceDto).insert(workAttendanceDto);
    }

    /**
     * スタッフ出勤を変更する。
     * @param workAttendanceDto スタッフ出勤
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody WorkAttendanceDto workAttendanceDto, Errors rrrors) throws Exception {
		DateTime dt = workAttendanceDto.getWorkStartTime();
		dt = dt.withField(DateTimeFieldType.year(), workAttendanceDto.getWorkDate().getYear())
				.withField(DateTimeFieldType.monthOfYear(), workAttendanceDto.getWorkDate().getMonthOfYear())
				.withField(DateTimeFieldType.dayOfMonth(), workAttendanceDto.getWorkDate().getDayOfMonth());

		workAttendanceDto.setWorkStartTime(dt);
		if(workAttendanceDto.getRealWorkStartTime() != null) {
			dt = workAttendanceDto.getRealWorkStartTime();
			dt = dt.withField(DateTimeFieldType.year(), workAttendanceDto.getWorkDate().getYear())
			.withField(DateTimeFieldType.monthOfYear(), workAttendanceDto.getWorkDate().getMonthOfYear())
			.withField(DateTimeFieldType.dayOfMonth(), workAttendanceDto.getWorkDate().getDayOfMonth());
			workAttendanceDto.setRealWorkStartTime(dt);
		}

        //プライマリーキーでスタッフ出勤を更新する。
        preSaveWork(workAttendanceDto);
        //workAttendanceDto.setWorkStartTime(workAttendanceDto.getRealWorkStartTime());
        return factory.getWorkStrategy(workAttendanceDto).update(workAttendanceDto, workAttendanceDto.getWorkId());
    }

    /**
     * スタッフ出勤を新規追加する。
     * @param workId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{workId}/{type}", method = RequestMethod.DELETE)
    public int delete(@PathVariable(name="workId") Integer workId, @PathVariable(name="type") String type) throws Exception {
    	if("1".equals(type)) {
    		//スタッフ出勤を削除する。
            WorkAttendanceDto w = workAttendanceService.selectAllByPrimaryKey(workId);
            return factory.getWorkStrategy(w).remove(w, w.getWorkId());
    	} else {
    		//スタッフ作業を削除する。
    		WorkTaskDto dto = workTaskService.selectAllByPrimaryKey(workId);
    		return factory.getWorkStrategy(dto).remove(dto, dto.getWorkId());
    	}

    }

    /**
     * スタッフ出勤一覧画面を表示する。
     * @param workAttendanceDto スタッフ出勤
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<WorkAttendanceDto> list(WorkAttendanceDto workAttendanceDto, JqgridPageReq pageReq) {
    	Page<WorkAttendanceDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフ出勤を検索する。（連携情報含む）
        workAttendanceService.selectByExample(workAttendanceDto);
        return PageUtil.resp(page);
    }

    /**
     * スタッフ出勤一覧画面を表示する。
     * @param workAttendanceDto スタッフ出勤
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<WorkDto> listAll(String staffCode, String staffName, WorkAttendanceDto workAttendanceDto, JqgridPageReq pageReq) throws ParseException  {
    	if(StringUtils.isNoneEmpty(pageReq.getSidx())) {
    		pageReq.setSidx(pageReq.getSidx() + ",");
    	}
    	pageReq.setSidx(pageReq.getSidx() + "work_staff_id, work_start_time");

    	Page<WorkDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフ出勤を検索する。（連携情報含む）
        StaffDto staffDto = new StaffDto();
        staffDto.setStaffCode(staffCode);
        staffDto.setStaffName(staffName);
        workAttendanceDto.setStaff(staffDto);
        workAttendanceDto.setWorkEndTime(workAttendanceDto.getWorkStartTime().withFieldAdded(DurationFieldType.days(), 1));

        workAttendanceService.selectAllOneDayWorks(workAttendanceDto, null);
        return PageUtil.resp(page);
    }

    /**
     * スタッフ最終出勤日一覧画面を表示する。
     * @param workAttendanceDto スタッフ出勤
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/lastWorkDay", method = RequestMethod.POST)
    public PageInfo<WorkAttendanceDto> selectLastWorkDay(String staffCode, String staffName, JqgridPageReq pageReq) {

    	pageReq.setRows(3);
    	pageReq.setSidx("staff_code, work_start_time");
    	pageReq.setSord("desc");
    	if(StringUtils.isEmpty(staffCode) && StringUtils.isEmpty(staffName)) {
			throw new UncheckedLogicException("javax.validation.constraints.NotNull.message.hasArg", "スタッフID又はスタッフ名前");
    	}


    	Page<WorkAttendanceDto> page =  PageUtil.startPage(pageReq);
        workAttendanceService.selectLastWorkDay(staffCode, staffName);
        page.setTotal(page.getTotal() > 3 ? 3 : page.getTotal());
        return PageUtil.resp(page);

    }


    /**
     * 勤怠確認画面を表示する。
     * @param workAttendanceDto スタッフ出勤
     * @param pageReq 改ページ情報
     * @return 結果
     * @throws ParseException
     */
    @RequestMapping(value = "/confirmAttendance", method = RequestMethod.POST)
    public PageInfo<WorkAttendanceDto> confirmAttendance(String staffCode, String staffName, WorkAttendanceDto workAttendanceDto, JqgridPageReq pageReq) throws ParseException {
        StaffDto staffDto = new StaffDto();
        staffDto.setStaffCode(staffCode);
        staffDto.setStaffName(staffName);
        workAttendanceDto.setStaff(staffDto);
    	Page<WorkAttendanceDto> page =  PageUtil.startPage(pageReq);
       // workAttendanceDto.setWorkEndTime(workAttendanceDto.getWorkStartTime().withFieldAdded(DurationFieldType.days(), 1));
        workAttendanceService.selectAttendanceByExample(workAttendanceDto);
        return PageUtil.resp(page);

    }

    /**
     * 勤怠確認画面を表示する。
     * @param workAttendanceDto スタッフ出勤
     * @param pageReq 改ページ情報
     * @return 結果
     * @throws ParseException
     */
    @RequestMapping(value = "/report/confirmAttendance", method = RequestMethod.POST)
    public PageInfo<WorkAttendanceDto> confirmAttendanceForReport(String staffCode, String staffName, WorkAttendanceDto workAttendanceDto, JqgridPageReq pageReq) throws ParseException {
        StaffDto staffDto = new StaffDto();
        staffDto.setStaffCode(staffCode);
        staffDto.setStaffName(staffName);
        workAttendanceDto.setStaff(staffDto);
        Page<WorkAttendanceDto> page =  PageUtil.startPage(pageReq);
        // workAttendanceDto.setWorkEndTime(workAttendanceDto.getWorkStartTime().withFieldAdded(DurationFieldType.days(), 1));
        workAttendanceService.selectMultiAttendanceByExample(workAttendanceDto);
        return PageUtil.resp(page);

    }


    /**
     * 退勤日データを作成する。
     * @param workAttendanceDto スタッフ出勤
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/insertLeave", method = RequestMethod.POST)
    public int insertLeaveWork(@Valid @RequestBody WorkAttendanceDto workAttendanceDto) throws Exception {
        //プライマリーキーでスタッフ出勤を更新する。

    	WorkAttendanceDto newWorkAttendanceDto = new WorkAttendanceDto();
    	newWorkAttendanceDto.setWorkBusinessId(workAttendanceDto.getWorkBusinessId());
    	newWorkAttendanceDto.setWorkStaffId(workAttendanceDto.getWorkStaffId());
    	newWorkAttendanceDto.setWorkStartTime(workAttendanceDto.getWorkEndTime());
    	newWorkAttendanceDto.setRealWorkStartTime(workAttendanceDto.getRealWorkEndTime());


        preSaveWork(newWorkAttendanceDto);
        //workAttendanceDto.setWorkStartTime(workAttendanceDto.getRealWorkStartTime());
        return factory.getWorkStrategy(newWorkAttendanceDto).insert(newWorkAttendanceDto);
    }


    private WorkAttendanceDto preSaveWork(WorkAttendanceDto workAttendanceDto) throws Exception {

    	StaffDto staffDto = new StaffDto();
    	staffDto.setStaffCode(workAttendanceDto.getStaffCode());
    	staffDto.setStaffId(workAttendanceDto.getWorkStaffId());

        List<StaffDto> staffList = staffService.selectByExample(staffDto);
        if(CollectionUtils.isEmpty(staffList)) {
            throw new UncheckedLogicException("errors.error.staff_not_exist");
        }
        workAttendanceDto.setWorkCompanyId(staffList.get(0).getStaffCompanyId());
        workAttendanceDto.setStaff(staffList.get(0));

        BusinessAttendanceDto bat = businessAttendanceService.selectByPrimaryKey(workAttendanceDto.getWorkBusinessId());
        if(bat == null) {
            throw new UncheckedLogicException("errors.error.business_code_not_exist");
        }
        if(!BusinessTypeCodeEnum.attendance.contains(bat.getBusinessTypeId())) {
            throw new UncheckedLogicException("errors.error.business_code_not_exist");
        }
        workAttendanceDto.setBusinessAttendance(bat);
        workAttendanceDto.setWorkStaffId(workAttendanceDto.getStaff().getStaffId());
        workAttendanceDto.setWorkBusinessId(workAttendanceDto.getBusinessAttendance().getBusinessId());

        if (BusinessTypeCodeEnum.starting.contains(bat.getBusinessTypeId())
                || BusinessTypeCodeEnum.leaving.contains(bat.getBusinessTypeId())) {
            //ServiceUtil.resetWorkingHours(workAttendanceDto, workAttendanceDto.getWorkStartTime());
        } else {
            if (workAttendanceDto.getWorkStartTime() == null) {
                workAttendanceDto.setWorkStartTime(DateUtils.getCurrentDateTime());
            }
            DateTime workEndTime = DateUtils.getRestEndTime(workAttendanceDto.getWorkStartTime(),
                    bat.getBusinessTypeId());

            workAttendanceDto.setWorkEndTime(workEndTime);

        }


        return workAttendanceDto;
    }

}