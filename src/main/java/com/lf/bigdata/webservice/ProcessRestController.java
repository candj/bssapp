package com.lf.bigdata.webservice;

import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.lf.bigdata.dto.ProcessDto;
import com.lf.bigdata.service.ProcessService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * 作業区分情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/processRest")
@RestController
public class ProcessRestController {
    @Autowired
    ProcessService processService;

    /**
     * 作業区分を新規追加する。
     * @param processId ID
     * @return 結果
     */
    @RequestMapping(value = "{processId}", method = RequestMethod.GET)
    public ProcessDto get(@PathVariable Integer processId) {
        //プライマリーキーで作業区分を検索する。
        ProcessDto ret = processService.selectByPrimaryKey(processId);
        return ret;
    }

    /**
     * 作業区分を新規追加する。
     * @param processDto 作業区分
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody ProcessDto processDto, Errors rrrors) {
        //作業区分を新規追加する。
        int ret = processService.insertSelective(processDto);
        return ret;
    }

    /**
     * 作業区分を変更する。
     * @param processDto 作業区分
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody ProcessDto processDto, Errors rrrors) {
        //プライマリーキーで作業区分を更新する。
        int ret = processService.updateByPrimaryKeySelective(processDto);
        return ret;
    }

    /**
     * 作業区分を新規追加する。
     * @param processId ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{processId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer processId) {
        //作業区分を削除する。
        int ret = processService.deleteByPrimaryKey(processId);
        return ret;
    }

    /**
     * 作業区分一覧画面を表示する。
     * @param processDto 作業区分
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<ProcessDto> list(ProcessDto processDto, JqgridPageReq pageReq) {
        Page<ProcessDto> page = PageUtil.startPage(pageReq);
        //条件で作業区分を検索する。（連携情報含む）
        List<ProcessDto> ret = processService.selectByExample(processDto);
        return PageUtil.resp(page);
    }

    /**
     * 作業区分一覧画面を表示する。
     * @param processDto 作業区分
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<ProcessDto> listAll(ProcessDto processDto, JqgridPageReq pageReq) {
        Page<ProcessDto> page = PageUtil.startPage(pageReq);
        //条件で作業区分を検索する。（連携情報含む）
        List<ProcessDto> ret = processService.selectAllByExample(processDto);
        return PageUtil.resp(page);
    }
}
