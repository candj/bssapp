package com.lf.bigdata.validator;

import java.util.List;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.lf.bigdata.dto.Work;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;

public class NoStartedValidator implements IWorkValidator<Work<?>> {

	@Override
	public boolean validate(List<Work<?>> works, Work<?> work) {
		//staffがまだ出勤されていない。
		works.stream().filter(w -> BusinessTypeCodeEnum.starting.equals(w.getBusiness().getBusinessTypeId()))
		.findFirst().<UncheckedLogicException>orElseThrow(() -> {
			throw new UncheckedLogicException("errors.error.business_code_no_starttime");
		});

//		if(startWork.getWorkEndTime() != null && work.getWorkStartTime().isAfter(startWork.getWorkEndTime())) {
//			throw new UncheckedLogicException("errors.error.business_code_no_starttime");
//		}

		return true;
	}

}
