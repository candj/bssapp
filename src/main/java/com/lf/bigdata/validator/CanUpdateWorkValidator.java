package com.lf.bigdata.validator;

import java.util.List;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.lf.bigdata.dto.Work;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;

public class CanUpdateWorkValidator implements IWorkValidator<Work<?>> {

	@Override
	public boolean validate(List<Work<?>> works, Work<?> work) {

		works.stream().filter(w -> BusinessTypeCodeEnum.starting.equals(w.getBusiness().getBusinessTypeId()))
				.findFirst().ifPresent(w -> {

					if (work.getWorkStartTime().isBefore(w.getWorkStartTime())) {
						throw new UncheckedLogicException("errors.error.error_update_proid");
					}

				});
		works.stream().filter(w -> BusinessTypeCodeEnum.leaving.equals(w.getBusiness().getBusinessTypeId()))
				.findFirst().ifPresent(w -> {

					if (work.getWorkStartTime().isAfter(w.getWorkStartTime())) {
						throw new UncheckedLogicException("errors.error.error_update_proid");
					}

				});
		return true;
	}
}
