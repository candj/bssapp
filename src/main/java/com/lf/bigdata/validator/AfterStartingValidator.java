package com.lf.bigdata.validator;


import java.util.List;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.lf.bigdata.dto.Work;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;
import com.lf.bigdata.utils.DateUtils;

public class AfterStartingValidator implements IWorkValidator<Work<?>> {

	@Override
	public boolean validate(List<Work<?>> works, Work<?> saveWork) {
		//出勤した後すぐ、休憩、トイレ作業できない。
		if (works.size() > 1) {
			Work<?> afterWork = works.get(1);

			if (BusinessTypeCodeEnum.task_work.equals(afterWork.getBusiness().getBusinessTypeId())
					&& !DateUtils.isToiletWork(afterWork)) {

			} else {
				throw new UncheckedLogicException("errors.error.business_code_second_business");
			}
		}
		return true;
	}

}
