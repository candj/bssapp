package com.lf.bigdata.validator;

import com.lf.bigdata.dto.Work;

import java.util.List;

public interface IWorkValidator<T> {

	boolean validate(List<Work<?>> works, T saveWork) ;

}
