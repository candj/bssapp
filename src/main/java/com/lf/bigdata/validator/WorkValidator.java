package com.lf.bigdata.validator;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.dto.WorkDto;
import com.lf.bigdata.dto.WorkTaskDto;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;
import com.lf.bigdata.strategy.OneDayWorks;
import com.lf.bigdata.utils.Constants;
import com.lf.bigdata.utils.ServiceUtil;

public class WorkValidator {

	public static boolean canRest(WorkAttendanceDto dto, OneDayWorks works) {

		isRepeatWork(works.getWorks());
		if (works.getLastWork(dto.getWorkStartTime()) == null
				|| ServiceUtil.isLeavingWork(works.getLastWork(dto.getWorkStartTime()).getBusinessTypeId())) {
			throw new UncheckedLogicException("errors.error.business_code_no_starttime");
		}
		if (ServiceUtil.isStartingWork(works.getLastWork(dto.getWorkStartTime()).getBusinessTypeId())) {
			throw new UncheckedLogicException("errors.error.business_code_second_business");
		}
//		if (ServiceUtil.isRestWork(works.getLastWork(dto.getWorkStartTime()).getBusinessTypeId())) {
//			throw new UncheckedLogicException("errors.error.business_code_resttime_conflict");
//		}
		vlidateConfictRestTime(works);
		validateOverLimitEndTime(dto.getWorkStartTime(), works);

		hasBeforeStartingWork(works, works.getStartWork().getWorkStartTime());

		if (works.getLeavingWork() != null) {
			hasAfterLeavingWork(works, works.getLeavingWork().getWorkStartTime());
		}

		return true;
	}

	public static boolean canUpdateStartWork(WorkAttendanceDto dto, OneDayWorks works) {
		isRepeatWork(works.getWorks());

		works.getWorks().stream().filter(w -> ServiceUtil.isTaskWork(w.getBusinessTypeId())).findFirst()
				.ifPresent(w -> {
					w.setWorkStartTime(dto.getWorkStartTime().withFieldAdded(DurationFieldType.seconds(), 1));

				});
		hasBeforeStartingWork(works, dto.getWorkStartTime());

		return true;
	}

	public static boolean canLeavingWork(WorkAttendanceDto dto, OneDayWorks works) {
		isRepeatWork(works.getWorks());
		int size = works.getWorks().stream().filter(w -> ServiceUtil.isLeavingWork(w.getBusinessTypeId()))
				.collect(Collectors.toList()).size();
		if (size > 1) {
			throw new UncheckedLogicException("errors.error.already_leaving");
		}

		if (works.getLastWork(dto.getWorkStartTime()) == null
				|| ServiceUtil.isLeavingWork(works.getLastWork(dto.getWorkStartTime()).getBusinessTypeId())) {
			throw new UncheckedLogicException("errors.error.business_code_no_starttime");
		}
		if (ServiceUtil.isStartingWork(works.getLastWork(dto.getWorkStartTime()).getBusinessTypeId())) {
			throw new UncheckedLogicException("errors.error.business_code_second_business");
		}
		validateOverLimitEndTime(dto.getWorkStartTime(), works);

		hasBeforeStartingWork(works, works.getStartWork().getWorkStartTime());

		hasAfterLeavingWork(works, dto.getWorkStartTime());

		return true;
	}

	public static boolean canTaskWork(WorkTaskDto dto, OneDayWorks works) {
		isRepeatWork(works.getWorks());
		if (works.getLastWork(dto.getWorkStartTime()) == null ||
				ServiceUtil.isLeavingWork(works.getLastWork(dto.getWorkStartTime()).getBusinessTypeId())) {
			throw new UncheckedLogicException("errors.error.business_code_no_starttime");
		}
		validateNextStartWork(works.getWorks());

		validateOverLimitEndTime(dto.getWorkStartTime(), works);

		hasBeforeStartingWork(works, works.getStartWork().getWorkStartTime());

		if (works.getLeavingWork() != null) {
			hasAfterLeavingWork(works, works.getLeavingWork().getWorkStartTime());
		}

		return true;
	}

	public static boolean validateOverLimitEndTime(DateTime workStartTime, OneDayWorks works) {

		WorkDto startWork = works.getStartWork();
		//最大退勤時間が超えているかを確認
		DateTime limitEndTime = startWork.getLimitEndTime();
		if (limitEndTime != null) {
			limitEndTime = limitEndTime.withYear(startWork.getWorkStartTime().getYear())
					.withMonthOfYear(startWork.getWorkStartTime().getMonthOfYear())
					.withDayOfMonth(startWork.getWorkStartTime().getDayOfMonth());

			if (Boolean.TRUE.equals(startWork.getLimitEndTimeDay())) {
				limitEndTime = limitEndTime.withFieldAdded(DurationFieldType.days(), 1);
			}
			if (workStartTime.isAfter(limitEndTime)) {
				//staffがまだ退勤していない。
				throw new UncheckedLogicException("errors.error.over_max_leaving_time");
			}
		}
		return true;
	}

	public static boolean validateNextStartWork(List<WorkDto> works) {
		works.stream().filter(w -> !ServiceUtil.isStartingWork(w.getBusinessTypeId())).findFirst().ifPresent(w -> {
			if (ServiceUtil.isLeavingWork(w.getBusinessTypeId()) || ServiceUtil.isRestWork(w.getBusinessTypeId())
					|| Constants.WORK_CODE_TOILET.equals(w.getBusinessCode())) {
				throw new UncheckedLogicException("errors.error.business_code_second_business");
			}
		});
		return true;
	}

	public static boolean isRepeatWork(List<WorkDto> works) {
		Set<DateTime> set = new HashSet<DateTime>();
		int i = 0;
		works.forEach(w -> {
			if(set.size() == 1){
				set.add(w.getWorkStartTime().withSecondOfMinute(0));
			} else {
				if (set.contains(w.getWorkStartTime().withSecondOfMinute(0))) {
					throw new UncheckedLogicException("errors.error.business_code_same_time");
				} else {
					set.add(w.getWorkStartTime().withSecondOfMinute(0));
				}
			}
		});
		set.clear();
		return true;
	}

	public static boolean hasBeforeStartingWork(OneDayWorks works, DateTime startWorkTime) {
		works.getWorks().stream().filter(w -> w.getWorkStartTime().isBefore(startWorkTime)).findFirst().ifPresent(w -> {
			throw new UncheckedLogicException("errors.error.update_to_before_starting");
		});
		return true;
	}

	public static boolean hasAfterLeavingWork(OneDayWorks works, DateTime startWorkTime) {
		works.getWorks().stream().filter(w -> w.getWorkStartTime().isAfter(startWorkTime)).findFirst().ifPresent(w -> {
			throw new UncheckedLogicException("errors.error.update_to_after_leaving");
		});
		return true;
	}

	public static void vlidateConfictRestTime(OneDayWorks works) {

		works.getWorks().stream().filter(w -> BusinessTypeCodeEnum.rest.contains(w.getBusinessTypeId())).forEach(rw -> {
			works.getWorks().stream().filter(cw -> cw.getWorkStartTime().isAfter(rw.getWorkStartTime())
					&& rw.getWorkEndTime().isAfter(cw.getWorkStartTime())
					&& BusinessTypeCodeEnum.rest.contains(cw.getBusinessTypeId())).findFirst().ifPresent(w -> {
						throw new UncheckedLogicException("errors.error.business_code_resttime_conflict");
					});

		});

	}

}
