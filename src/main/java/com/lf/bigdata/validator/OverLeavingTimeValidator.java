package com.lf.bigdata.validator;

import java.util.List;

import org.joda.time.DateTime;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.lf.bigdata.dto.Work;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;

/**
 * 作業時間は終了時間よりオーバーするチェッククラス。
 * @author h-tsujinoi
 *
 */
public class OverLeavingTimeValidator implements IWorkValidator<Work<?>> {

	@Override
	public boolean validate(List<Work<?>> works, Work<?> work) {
		//staffがまだ出勤されていない。
		 works.stream().filter(w -> BusinessTypeCodeEnum.starting.equals(w.getBusiness().getBusinessTypeId()))
		.findFirst().ifPresent(w -> {
			//当日の出勤データを取得する。
			DateTime workEndTime = w.getWorkEndTime();

			if (workEndTime != null) {
				if (work.getWorkStartTime().isAfter(workEndTime)) {
					//staffがまだ出勤されていない。
					throw new UncheckedLogicException("errors.error.business_code_no_starttime");
				}
			}
		});

		return true;
	}

}
