package com.lf.bigdata.validator;

import java.util.List;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.lf.bigdata.dto.Work;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;

/**
 * 出勤できるかどうかチェックするクラス
 * @author 崔
 *
 */
public class StartableValidator implements IWorkValidator<Work<?>> {

	@Override
	public boolean validate(List<Work<?>> works, Work<?> saveWork) {
		//退勤していないため、出勤できません。
		long count = works.stream()
				.filter(w -> BusinessTypeCodeEnum.starting.contains(w.getBusiness().getBusinessTypeId())).count();
		if (count > 1) {
			//退勤の後出勤するかどうか
			Work<?> leavingWork = works.stream()
					.filter(w -> BusinessTypeCodeEnum.leaving.contains(w.getBusiness().getBusinessTypeId())).findFirst()
					.<UncheckedLogicException>orElseThrow(() -> {
						throw new UncheckedLogicException("errors.error.business_code_not_leaving");
					});

			//退勤の後出勤するかどうか
			if (!saveWork.getWorkStartTime().isAfter(leavingWork.getWorkStartTime())) {
				throw new UncheckedLogicException("errors.error.business_code_starttime_repeat");
			}

		}
		if(works.size() > 1) {
			if(works.get(0).getWorkStartTime().isAfter(works.get(works.size() -1).getWorkStartTime())) {
				throw new UncheckedLogicException("errors.error.error_update_proid");
			}



		}




		return true;
	}
}
