package com.lf.bigdata.validator;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.lf.bigdata.dto.Work;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 休憩作業重複チェッククラス。
 * @author 崔
 *
 */
public class RepeatRestValidator implements IWorkValidator<Work<?>> {

	@Override
	public boolean validate(List<Work<?>> works, Work<?> saveWork) {

		works = works.stream().filter(w -> BusinessTypeCodeEnum.rest.contains(w.getBusiness().getBusinessTypeId()) && w != saveWork)
				.collect(Collectors.toList());

		//お休み中ですので、更にお休みできません。
		works.stream().forEach(w -> {
			if (w.getWorkId() != null) {
				if (!saveWork.getWorkStartTime().isBefore(w.getWorkStartTime())
						&& !saveWork.getWorkStartTime().isAfter(w.getWorkEndTime())) {
					throw new UncheckedLogicException("errors.error.business_code_resttime_conflict");
				}
				if (!saveWork.getWorkEndTime().isBefore(w.getWorkStartTime())
						&& !saveWork.getWorkEndTime().isAfter(w.getWorkEndTime())) {
					throw new UncheckedLogicException("errors.error.business_code_resttime_conflict");
				}
			}
		});

		return true;
	}

}
