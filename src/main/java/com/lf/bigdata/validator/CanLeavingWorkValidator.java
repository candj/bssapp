package com.lf.bigdata.validator;

import java.util.List;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.lf.bigdata.dto.Work;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;

public class CanLeavingWorkValidator implements IWorkValidator<Work<?>> {

	@Override
	public boolean validate(List<Work<?>> works, Work<?> work) {

		works.stream().filter(w -> w.getWorkStartTime().isAfter(work.getWorkStartTime()))
				.findFirst().ifPresent(w -> {
					throw new UncheckedLogicException("errors.error.business_cannot_leaving");
				});

		if(works.stream().filter(w -> BusinessTypeCodeEnum.leaving.equals(w.getBusiness().getBusinessTypeId()))
		.count() > 1) {
			throw new UncheckedLogicException("errors.error.already_leaving");
		}



		return true;
	}
}
