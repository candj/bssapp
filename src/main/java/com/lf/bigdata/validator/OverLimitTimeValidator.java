package com.lf.bigdata.validator;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.lf.bigdata.dto.BusinessDto;
import com.lf.bigdata.dto.Work;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;

public class OverLimitTimeValidator implements IWorkValidator<Work<?>> {

	@Override
	public boolean validate(List<Work<?>> works, Work<?> work) {

		works.stream().filter(w -> BusinessTypeCodeEnum.starting.equals(w.getBusiness().getBusinessTypeId()))
				.findFirst().ifPresent(startWork -> {

					//当日の出勤データを取得する。
					//DateTime workEndTime = startWork.getWorkEndTime();

					//if (workEndTime == null) {
					//最大退勤時間が超えているかを確認
					BusinessDto businessDto = startWork.getBusiness();

					DateTime limitEndTime = businessDto.getLimitEndTime();
					if (limitEndTime != null) {
						limitEndTime = limitEndTime.withYear(startWork.getWorkStartTime().getYear())
								.withMonthOfYear(startWork.getWorkStartTime().getMonthOfYear())
								.withDayOfMonth(startWork.getWorkStartTime().getDayOfMonth());

						if (Boolean.TRUE.equals(businessDto.getLimitEndTimeDay())) {
							limitEndTime = limitEndTime.withFieldAdded(DurationFieldType.days(), 1);
						}
						if (work.getWorkStartTime().isAfter(limitEndTime)) {
							//staffがまだ退勤していない。
							throw new UncheckedLogicException("errors.error.no_business_not_leaving");
						}
					}

					//	}
				});

		return true;
	}

}
