package com.lf.bigdata.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * スタッフ
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Staff {
    /** ID */
    private Integer staffId;

    /** スタッフID */
    private String staffCode;

    /** バーコード */
    private String staffEncryptCode;

    /** スタッフ名前 */
    private String staffName;

    /** 区別 */
    private String staffWorkType;

    /** 会社 */
    private Integer staffCompanyId;

    /** 派遣会社作業員番号 */
    private String workerNumber;

    /** 業務等級 */
    private String bussinessGrade;

    /** 説明 */
    private String remark;

    /** ステータス */
    private String status;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** スタッフ出勤 */
    private java.util.List<WorkAttendance> workAttendance;

    /** スタッフ作業 */
    private java.util.List<WorkTask> workTask;

    /** 掲示板表示状態 */
    private Boolean blackStatus;

    /** 会社 */
    private Company company;
}