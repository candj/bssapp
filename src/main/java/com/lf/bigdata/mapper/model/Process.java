package com.lf.bigdata.mapper.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 作業区分
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Process {
    /** 掲示板情報 */
    private List<Notification> notificationList;

    /** ID */
    private Integer processId;

    /** 作業区分コード */
    private String processCode;

    /** 作業区分名 */
    private String processName;

    /** 掲示板表示状態 */
    private Boolean processStatus;

    /** 掲示板表示優先順位*/
    private Integer notifiLevel;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
