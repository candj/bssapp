package com.lf.bigdata.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 作業予定
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class WorkPlan {
    /** 作業予定ID */
    private Integer workPlanId;

    /** 会社 */
    private Company company;

    /** 会社ID */
    private Integer workPlanCompanyId;

    /** スタッフ */
    private Staff staff;

    /** スタッフID */
    private Integer workPlanStaffId;

    /** 日付 */
    private org.joda.time.DateTime workPlanDate;

    /** 社員番号 */
    private String workPlanNumber;

    /** クラス */
    private String workPlanLevel;

    /** 氏名 */
    private String workPlanName;

    /** フリガナ（全角カナ） */
    private String workPlanNameKana;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
