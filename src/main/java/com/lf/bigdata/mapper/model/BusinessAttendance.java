package com.lf.bigdata.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 勤怠業務
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class BusinessAttendance {
    /** ID */
    private Integer businessId;

    /** 業務名 */
    private String businessName;

    /** 業務名「中国語」 */
    private String businessNameCn;

    /** 業務コード */
    private String businessCode;

    /** 暗証化コード */
    private String businessEncryptCode;

    /** 業務開始時間 */
    private org.joda.time.DateTime businessStartTime;

    /** 最大退勤時間（出勤だけ） */
    private org.joda.time.DateTime limitEndTime;

    /** FALSE:今日　TRUE:明日（出勤だけ） */
    private Boolean limitEndTimeDay;

    /** 業務区分ID */
    private String businessTypeId;

    /** ステータス */
    private String status;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** スタッフ出勤 */
    private java.util.List<WorkAttendance> workAttendance;
}