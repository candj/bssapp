package com.lf.bigdata.mapper.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Digits;

/**
 * 会社
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Company {
    /** ID */
    private Integer companyId;

    /** 会社コード */
    private String companyCode;

    /** 会社名 */
    private String companyName;

    /** 住所 */
    private String companyAdress;

    /** 郵便番号 */
    private String postCode;

    /** 電話番号 */
    private String telephoneNumber;

    /** 単金 */
    private Integer salary;

    /** リーダー単金 */
    private Integer leaderSalary;

    /** サブリーダー単金 */
    private Integer subLeaderSalary;

    /** トレーナー単金 */
    private Integer trainerSalary;

    /** フォーク単金 */
    private Integer forkSalary;

    /** 交通費 */
    private Integer commutingCost;

    /** ステータス */
    private String status;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** スタッフ */
    private java.util.List<Staff> staff;
}