package com.lf.bigdata.mapper.model;

import org.joda.time.DateTime;


public class WorkDay {
    /** 開始時間 */
    private DateTime workStartTime;

    /** 完成時間 */
    private DateTime workEndTime;

	public DateTime getWorkStartTime() {
		return workStartTime;
	}

	public void setWorkStartTime(DateTime workStartTime) {
		this.workStartTime = workStartTime;
	}

	public DateTime getWorkEndTime() {
		return workEndTime;
	}

	public void setWorkEndTime(DateTime workEndTime) {
		this.workEndTime = workEndTime;
	}



}
