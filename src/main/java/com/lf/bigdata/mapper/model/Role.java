package com.lf.bigdata.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * ロール
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Role {
    /** ロールID */
    private Integer roleId;

    /** ロールコード */
    private String roleCode;

    /** ロール名 */
    private String roleName;

    /** ステータス */
    private String status;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** ユーザールール */
    private java.util.List<UserRole> userRole;
}