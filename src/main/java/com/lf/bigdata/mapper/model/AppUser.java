package com.lf.bigdata.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * ユーザー
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class AppUser {
    /** ユーザーID */
    private Integer userId;

    /** ユーザー名 */
    private String userName;

    /** ユーザーコード */
    private String userCode;

    /** パスワード */
    private String password;

    /** ステータス */
    private String status;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** ユーザールール */
    private java.util.List<UserRole> userRole;
}