package com.lf.bigdata.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * スタッフ作業
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class WorkTask {
    /** ID */
    private Integer workId;

    /** スタッフID */
    private Integer workStaffId;

    /** 会社ID */
    private Integer workCompanyId;

    /** 業務ID */
    private Integer workBusinessId;

    /** 業務開始時間 */
    private org.joda.time.DateTime workStartTime;

    /** 業務完成時間 */
    private org.joda.time.DateTime workEndTime;

    /** ステータス */
    private String status;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** 作業業務 */
    private BusinessTask businessTask;

    /** スタッフ */
    private Staff staff;
}