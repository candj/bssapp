package com.lf.bigdata.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * ユーザールール
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class UserRole {
    /** ID */
    private Integer id;

    /** ユーザーID */
    private Integer userId;

    /** ロールID */
    private Integer roleId;

    /** 登録ユーザー */
    private Integer createUser;

    /** ステータス */
    private String status;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** ロール */
    private Role role;

    /** ユーザー */
    private AppUser appUser;
}