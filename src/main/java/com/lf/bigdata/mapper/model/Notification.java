package com.lf.bigdata.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 掲示板情報
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Notification {
    /** ID */
    private Integer notificationId;

    /** タイトル */
    private String notificationTitle;

    /** 作業区分 */
    private Process process;

    /** 作業区分ID */
    private Integer notificationProcessId;

    /** 編集可能状態 */
    private Boolean notificationStatus;

    /** 最新開放時間 */
    private org.joda.time.DateTime notificationOpenTime;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** 内容 */
    private String notificationContent;

    /** opened status */
    private boolean openedRecently;

    /** edited status */
    private boolean editedRecently;

}
