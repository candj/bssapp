package com.lf.bigdata.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 作業業務
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class BusinessTask {
    /** ID */
    private Integer businessId;

    /** 業務名 */
    private String businessName;

    /** 業務名「中国語」 */
    private String businessNameCn;

    /** 業務コード */
    private String businessCode;

    /** 暗証化コード */
    private String businessEncryptCode;

    /** ブランド */
    private String area;

    /** 業務区別 */
    private String process;

    /** マーク */
    private String mark;

    /** 業務モード */
    private String chargeType;

    /** 業務区分ID */
    private String businessTypeId;

    /** ステータス */
    private String status;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** スタッフ作業 */
    private java.util.List<WorkTask> workTask;
}