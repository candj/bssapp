package com.lf.bigdata.mapper.model;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Digits;

/**
 * スタッフ出勤
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class WorkAttendance {
    /** ID */
    private Integer workId;

    /** スタッフID */
    private Integer workStaffId;

    /** 会社ID */
    private Integer workCompanyId;

    /** 業務ID */
    private Integer workBusinessId;

    /** 出勤時間 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private org.joda.time.DateTime workStartTime;

    /** 実際出勤時間 */
    private org.joda.time.DateTime realWorkStartTime;

    /** 退勤時間 */
    private org.joda.time.DateTime workEndTime;

    /** 実際退勤時間 */
    private org.joda.time.DateTime realWorkEndTime;

    /** ステータス */
    private String status;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** 勤怠業務 */
    private BusinessAttendance businessAttendance;

    /** スタッフ */
    private Staff staff;
}