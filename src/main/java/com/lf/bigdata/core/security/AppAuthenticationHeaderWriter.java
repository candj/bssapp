package com.lf.bigdata.core.security;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.header.HeaderWriter;

import lombok.extern.slf4j.Slf4j;

/**
 * AppAuthenticationHeaderWriter.
 */
@Slf4j
public class AppAuthenticationHeaderWriter implements HeaderWriter {

    /** {@inheritDoc} */
    @Override
    public void writeHeaders(HttpServletRequest request, HttpServletResponse response) {
        log.debug(this.getClass().getSimpleName() + ".writeHeaders");
        // レスポンスに共通で出力したいものがあれば記載する.
    }
}
