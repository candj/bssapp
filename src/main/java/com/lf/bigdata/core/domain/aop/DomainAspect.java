package com.lf.bigdata.core.domain.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.candj.webpower.web.core.domain.aop.AbstractAspect;

/**
 * DBAccessAspect.
 */
@Aspect
@Component
public class DomainAspect extends AbstractAspect {

    /**
     * @param point {@link JoinPoint}
     */
    @Before(value =
                "execution(* com.lf.bigdata.dao.mapper..*.*(..)) "
                + "|| execution(* com.lf.bigdata.service..*.*(..))"
                + "|| execution(* com.lf.bigdata.controller..*.*(..))"
    			+ "|| execution(* com.lf.bigdata.webservice..*.*(..))")
    public void domainExecute(JoinPoint point) {
        exec(point);
    }

//    /**
//     * @param point {@link JoinPoint}
//     * @param result {@link Object}
//     */
//    @AfterReturning(value = "execution(* com.candj.webpower.web.consumer.repository..*.*(..))", returning = "result")
//    public void queryExecuteSuccess(JoinPoint point, Object result) {
//        exec(point);
//    }

//    /**
//     * @param point {@link JoinPoint}
//     * @param e {@link QueryTimeoutException}
//     */
//    @AfterThrowing(value = "execution(* com.candj.webpower.web.consumer.domain.repository..*.*(..))", throwing = "e")
//    public void queryTimeout(JoinPoint point, QueryTimeoutException e) {
//        exec(point, e);
//        LogUtil.error("007-00001");
//    }

//    /**
//     * @param point {@link JoinPoint}
//     * @param e {@link PersistenceException}
//     */
//    @AfterThrowing(value = "execution(* com.candj.webpower.web.consumer.domain.repository..*.*(..))", throwing = "e")
//    public void queryTimeoutElse(JoinPoint point, DataAccessException e) {
//        exec(point, e);
//        LogUtil.error("007-00003");
//    }
}
