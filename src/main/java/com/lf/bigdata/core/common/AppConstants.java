package com.lf.bigdata.core.common;

public class AppConstants {

    /** 日付フォーマット. */
    public static final String DATE_TIME_FORMAT_YMDHMS = "yyyyMMddHHmmss";
    /** ウォレットサービスIDの最大桁数. */
    public static final int MAX_LENGTH_SERVICE_ID = 3;
    /** ウォレットサービス鍵の最大桁数. */
    public static final int MAX_LENGTH_SERVICE_SECRET = 128;
    /** SmartCode_ワンタイムコードの最大桁数. */
    public static final int MAX_LENGTH_SMART_CODE_ONE_TIME_CODE = 32;
    /** SmartCode_リクエストIDの最大桁数. */
    public static final int MAX_LENGTH_SMART_CODE_REQUEST_ID = 32;
    /** SmartCode_加盟店IDの最大桁数. */
    public static final int MAX_LENGTH_SMART_CODE_BIZ_CODE = 32;
    /** SmartCode_加盟店店舗コードの最大桁数. */
    public static final int MAX_LENGTH_SMART_CODE_STORE_CODE = 32;
    /** SmartCode_加盟店端末コードの最大桁数. */
    public static final int MAX_LENGTH_SMART_CODE_TERM_CODE = 32;
    /** SmartCode_加盟店レシート番号の最大桁数. */
    public static final int MAX_LENGTH_SMART_CODE_RECEIPT_NO = 32;
    /** SmartCode_signの最大桁数. */
    public static final int MAX_LENGTH_SMART_CODE_SIGN = 500;
    /** SmartCode_加盟店端末送信日時の最大桁数. */
    public static final int MAX_LENGTH_SMART_CODE_REQTIME = 14;
    /** SmartCode_取引金額の最大桁数. */
    public static final int MAX_LENGTH_SMART_CODE_AMOUNT = 8;

    /** IncommAdapt_加盟店要求IDの最大桁数. */
    public static final int MAX_LENGTH_INCOMM_ADAPT_MERCHANT_REQUEST_ID = 256;

    /** IncommAdapt_加盟店IDの最大桁数. */
    public static final int MAX_LENGTH_INCOMM_ADAPT_BIZ_CODE = 100;

    /** IncommAdapt_加盟店店舗コードの最大桁数. */
    public static final int MAX_LENGTH_INCOMM_ADAPT_STORE_CODE = 100;

    /** IncommAdapt_加盟店端末コード. */
    public static final int MAX_LENGTH_INCOMM_ADAPT_TERM_CODE = 100;

    /** From 新前給管理システムのAPIの接頭語. */
    public static final String PREFIX_URI_NEW_MAEKYU = "/external/maekyu/";

    /** FromインコムアダプトのAPIの接頭語. */
    public static final String PREFIX_URI_INCOM_ADAPT = "/incom_adapt/";

    /** From SmartCodeのAPIの接頭語. */
    public static final String PREFIX_URI_SMART_CODE = "/smart_code/";

    /** From 新前給管理システムのAPIの接頭語. */
    public static final String PREFIX_URI_TRUSTDOCK = "/external/trustdock";

    /**
     * 決済取引ステータス '"処理待ち" '"waiting" "処理中" "processing" "取引成功" "success" "取引失敗"
     * "failure" "取消済み" "canceled"
     */
    public static final String TRANSACTION_STATUS_WAITING = "処理待ち";
    public static final String TRANSACTION_STATUS_PROCESSING = "処理中";
    public static final String TRANSACTION_STATUS_SUCCESS = "取引成功";
    public static final String TRANSACTION_STATUS_FAILURE = "取引失敗";
    public static final String TRANSACTION_STATUS_CANCELED = "取消済み";

    /** 前給：申込コード */
    public static final int LENGTH_MAEKYU_ORDER_CD = 12;

    /** チャージ区分：前給チャージ. */
    public static final String CHARGE_TYPE_MAEKYU_CHARGE = "1";

    /** チャージ区分：給振チャージ. */
    public static final String CHARGE_TYPE_KYUFURI_CHARGE = "2";

    /** カンマ. */
    public static final String KOMMA = ",";

    /** 利用者IDの桁数. */
    public static final int LENGTH_MEMBER_ID = 10;

    /** 企業グループIDの桁数. */
    public static final int LENGTH_COMP_GROUP_ID = 10;

    /** メールアドレスの最小桁数. */
    public static final int MIN_LENGTH_EMAIL_ADDRESS = 6;

    /** メールアドレスの最大桁数. */
    public static final int MAX_LENGTH_EMAIL_ADDRESS = 100;

    /** Emailの最小桁数. */
    public static final int MIN_LENGTH_EMAIL = 6;

    /** Emailの最大桁数. */
    public static final int MAX_LENGTH_EMAIL = 100;

    /** 確認コードの文字長. */
    public static final int VALIDATE_CODE_LENGTH = 6;

    /** リフレッシュトークンの最大桁数. */
    public static final int MAX_LENGTH_REFRESH_TOKEN = 64;

    /** 取引用ユーザリクエストIDの桁数. */
    public static final int LENGTH_TRANSACTION_REQUEST_ID = 36;

    /** QRコードトークンの最大桁数. */
    public static final int MAX_LENGTH_QR_CODE = 512;

    /** 表示件数の最大値. */
    public static final int MAX_LIMIT = 100;

    /** ページ数の最小値. */
    public static final int MIN_PAGE = 1;

    /** パスワードの最大桁数. */
    public static final int MAX_LENGTH_PASSWORD = 100;

    /** パスワードの最小桁数. */
    public static final int MIN_LENGTH_PASSWORD = 8;

    /** 端末区分. */
    public static final String MAEKYU_TERMINAL_TYPE = "app";

    /** 前給用パスワードの最大桁数. */
    public static final int MAX_LENGTH_PASSWORD_MAEKYU = 20;

    /** 前給用企業グループIDの最大桁数. */
    public static final int MAX_LENGTH_COMPGROUPID_MAEKYU = 10;

    /** 前給用利用者IDの最大桁数. */
    public static final int MAX_LENGTH_MEMBERID_MAEKYU = 10;

    /** 前給用銀行番号の桁数. */
    public static final int LENGTH_BANKNUM_MAEKYU = 4;

    /** 前給用支店番号の桁数. */
    public static final int LENGTH_BRANCHNUM_MAEKYU = 3;

    /** 前給用口座番号の桁数. */
    public static final int LENGTH_ACCOUNTNUM_MAEKYU = 7;

    /** 前給用利用規約IDの桁数. */
    public static final int LENGTH_TOSID_MAEKYU = 10;

    /** 申込日の桁数. */
    public static final int LENGTH_ORDER_DATE = 8;

    /** ユーザ新規登録時画面ID. */
    public static final String SCREEN_ID_CREATE_USER = "MK01-01-04";

    /** メールアドレス変更時画面ID. */
    public static final String SCREEN_ID_EDIT_MAIL_ADDRESS = "MK02-18-03";
}
