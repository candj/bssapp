package com.lf.bigdata.core.common;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.joda.time.DateTime;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import com.candj.webpower.web.core.util.DateUtil;
import com.lf.bigdata.core.security.AppAuthenticationUser;

/**
 * mapperで登録日時と更新日時を自動生成する。.
 *
 */
@Aspect
@Component
public class MapperAspect {

    @Before("execution(* com.lf.bigdata.dao.mapper.*Dao.insert*(..)) || "
            + "execution(* com.lf.bigdata.dao.mapper.*Dao.update*(..))")
    public void setCommonProperty(JoinPoint jp) throws Throwable {

        // Mapperのメソッド名を取得
        MethodSignature signature = (MethodSignature) jp.getSignature();
        Method method = signature.getMethod();
        String methodName = method.getName();

        // 現在日時の取得
        DateTime now = DateUtil.now();

        // Mapperの第一引数（モデルオブジェクト）を取得
        Object[] args = jp.getArgs();
        Object dto = args[0];

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Integer userId = null;
        if(authentication != null && authentication.getPrincipal() instanceof AppAuthenticationUser) {
            AppAuthenticationUser loginUserInfo = AppAuthenticationUser.class.cast(authentication.getPrincipal());
            userId = loginUserInfo.getAppUser().getUserId();
        }
        // create*メソッドは作成者・作成日時・更新者・更新日時をセット
        if (methodName.startsWith("insert")) {
        	setCreateTimeProperty(now, dto);
        	setCreateUserProperty(userId, dto);

        	setUpdateTimeProperty(now, dto);
        	setUpdateUserProperty(userId, dto);


        // update*メソッドは更新者・更新日時をセット
        } else if (methodName.startsWith("update")) {
        	setUpdateTimeProperty(now, dto);
        	setUpdateUserProperty(userId, dto);
        }
    }

    // 作成日時をセット
    private void setCreateTimeProperty(DateTime now, Object dto) throws Throwable {

        // Mapperの引数にsetCreatedAtメソッドがある場合、現在日時をセット
        Method setCreatedAt  = ReflectionUtils.findMethod(dto.getClass(), "setCreateTime", DateTime.class);
        if (setCreatedAt != null) {
            setCreatedAt.invoke(dto, now);
        }

    }

    // 更新日時をセット
    private void setUpdateTimeProperty(DateTime now, Object dto) throws Throwable {

        // Mapperの引数にsetUpdatedAtメソッドがある場合、現在日時をセット
        Method setUpdatedAt  = ReflectionUtils.findMethod(dto.getClass(), "setUpdateTime", DateTime.class);
        if (setUpdatedAt != null) {
            setUpdatedAt.invoke(dto, now);
        }

    }


    // 作成者をセット
    private void setCreateUserProperty(Integer userId, Object dto) throws Throwable {

    	SecurityContextHolder.getContext().getAuthentication().getCredentials();

        // Mapperの引数にsetCreatedAtメソッドがある場合、現在日時をセット
        Method setCreatedAt  = ReflectionUtils.findMethod(dto.getClass(), "setCreateUser", Integer.class);
        if (setCreatedAt != null) {
            setCreatedAt.invoke(dto, userId);
        }

    }

    // 更新者をセット
    private void setUpdateUserProperty(Integer userId, Object dto) throws Throwable {

        // Mapperの引数にsetUpdatedAtメソッドがある場合、現在日時をセット
        Method setUpdatedAt  = ReflectionUtils.findMethod(dto.getClass(), "setUpdateUser", Integer.class);
        if (setUpdatedAt != null) {
            setUpdatedAt.invoke(dto, userId);
        }

    }

}
