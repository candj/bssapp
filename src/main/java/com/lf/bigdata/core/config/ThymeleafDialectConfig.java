package com.lf.bigdata.core.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;

import nz.net.ultraq.thymeleaf.JodaDialect;

@Component
public class ThymeleafDialectConfig implements BeanFactoryAware, InitializingBean {

	private BeanFactory beanFactory;

	@Override
	public void afterPropertiesSet() throws Exception {
		TemplateEngine templateEngine = beanFactory.getBean(TemplateEngine.class);
		templateEngine.addDialect(new JodaDialect());
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}

}
