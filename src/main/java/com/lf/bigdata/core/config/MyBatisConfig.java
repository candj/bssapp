package com.lf.bigdata.core.config;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.lf.bigdata.dao.mapper")
public class MyBatisConfig {
}
