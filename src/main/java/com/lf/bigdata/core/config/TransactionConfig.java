package com.lf.bigdata.core.config;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.AnnotationTransactionAttributeSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.interceptor.BeanFactoryTransactionAttributeSourceAdvisor;
import org.springframework.transaction.interceptor.RollbackRuleAttribute;
import org.springframework.transaction.interceptor.RuleBasedTransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAttribute;
import org.springframework.transaction.interceptor.TransactionInterceptor;

/**
 * トランザクション設定.
 */
@Configuration
@EnableTransactionManagement
public class TransactionConfig {

    /**
     * @param transactionManager .
     * @return .
     */
    @Bean
    public BeanFactoryTransactionAttributeSourceAdvisor transactionAdvisor(TransactionManager transactionManager) {
        AnnotationTransactionAttributeSource source = new RollbackAnnotationTransactionAttributeSource();

        BeanFactoryTransactionAttributeSourceAdvisor advisor = new BeanFactoryTransactionAttributeSourceAdvisor();
        advisor.setTransactionAttributeSource(source);
        advisor.setAdvice(new TransactionInterceptor(transactionManager, source));
        return advisor;
    }

    /**
     * Transactionアノテーション：Rollbackハンドラ.
     */
    public static class RollbackAnnotationTransactionAttributeSource extends AnnotationTransactionAttributeSource {

        private static final long serialVersionUID = 1L;

        /** ROLLBACK対象例外クラス. */
        private static final Class<?>[] ROLLBACK_FOR = {Exception.class};

        /** {@inheritDoc} */
        @Override
        public TransactionAttribute computeTransactionAttribute(Method method, Class<?> targetClass) {
            TransactionAttribute txAttr = super.computeTransactionAttribute(method, targetClass);
            if (txAttr instanceof RuleBasedTransactionAttribute) {
                RuleBasedTransactionAttribute rbta = (RuleBasedTransactionAttribute) txAttr;
                List<RollbackRuleAttribute> rules = rbta.getRollbackRules();
                Arrays.stream(ROLLBACK_FOR).map(RollbackRuleAttribute::new).forEach(rules::add);
            }
            return txAttr;
        }
    }

    @Bean
    public DataSourceTransactionManager transactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
