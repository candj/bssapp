package com.lf.bigdata.core.validation;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 値範囲のバリデーション.
 */
public class ValueInValidator implements ConstraintValidator<ValueIn, String> {

    private String[] values;

    @Override
    public void initialize(ValueIn valueIn) {
        values = valueIn.values();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        for (String str : values) {
            if (str.equals(value)) {
                return true;
            }
        }
        return false;
    }
}
