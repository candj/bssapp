package com.lf.bigdata.core.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = { LinkValueDisplayValidator.class })
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER,TYPE})
@Retention(RUNTIME)
@ReportAsSingleViolation
public @interface LinkValueDisplay {
    String message() default "不正な項目、値が含まれています";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String linkTypeProperty();

    String displayProperty();

    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER,TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        LinkValueDisplay[] value();
    }
}
