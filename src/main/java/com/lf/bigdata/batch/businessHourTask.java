package com.lf.bigdata.batch;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.candj.webpower.web.core.util.DateUtil;
import com.lf.bigdata.dto.WorkTaskDto;
import com.lf.bigdata.service.BusinessTaskService;
import com.lf.bigdata.service.ReportService;

@Component
public class businessHourTask {
	private static final Logger logger = LoggerFactory.getLogger(businessHourTask.class);

	@Autowired
	BusinessTaskService businessTaskService;

	@Autowired
	ReportService reportService;

	// エクセルexportのパースを取得する
	@Value("${spring.businessHour.batchPath}")
	String filePath;

	private File outputFolder = null;

	@PostConstruct
	public void init() throws IOException {

		outputFolder = new File(filePath);
		if (!outputFolder.exists()) {
			outputFolder.mkdirs();
		}
	}

	@Scheduled(cron = "0 30 12 ? * * ")
	public void taskCycle() throws IOException {

		// 毎日12時30分にE001の機能で前日分の統計データを自動生成
		DateTime yesterday = DateTime.parse(DateUtil.now().toString(DateUtil.BM_DATE_FORMAT))
				.withFieldAdded(DurationFieldType.days(), -1);

		WorkTaskDto dto = new WorkTaskDto();
		dto.setWorkStartTime(yesterday);
		String filename = String.format("OverAllMgr_Ver0.3_%s.xlsx", yesterday.toString(DateUtil.BM_DATE_FORMAT));

		File file = new File(outputFolder, filename);

		OutputStream os = new FileOutputStream(file);

		try {
			reportService.outputBusinessDayHourFile(dto, os);
		} catch (Exception ex) {
			logger.error("例外をキャッチしました。", ex);
		} finally {
			if (os != null) {
				os.close();
			}

		}
	}
}
