package com.lf.bigdata.dto;

import java.util.List;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;

/**
 * 作業区分
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class ProcessDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer processId;

    /** 掲示板情報 */
    private List<NotificationDto> notificationList;

    /** 作業区分コード */
    @Length(max=128)
    private String processCode;

    /** 作業区分名 */
    @Length(max=128)
    private String processName;

    /** 掲示板表示状態 */
    private Boolean processStatus;

    /** 掲示板表示優先順位*/
    private Integer notifiLevel;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
