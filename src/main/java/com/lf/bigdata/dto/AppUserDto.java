package com.lf.bigdata.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * ユーザー
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class AppUserDto {
    /** ユーザーID */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** ユーザー名 */
    @Length(max=128)
    private String userName;

    /** ユーザーコード */
    @Length(max=64)
    private String userCode;

    /** パスワード */
    @Length(max=256)
    private String password;

    /** ステータス */
    @Length(max=2)
    private String status;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** ユーザールール */
    private java.util.List<UserRoleDto> userRole;
}