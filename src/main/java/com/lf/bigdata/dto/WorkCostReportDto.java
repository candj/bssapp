package com.lf.bigdata.dto;

import com.lf.bigdata.mapper.model.WorkAttendance;

import lombok.Getter;
import lombok.Setter;

/**
 * コスト
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class WorkCostReportDto extends WorkAttendance {

    /** 休憩時間 */
    private Long restTime;

    /** 休憩時間 */
    private String formattedRestTime;
    /**会社名*/
    private String companyName;

    /**夜間休憩時間*/
    private Long nightRestTime;

    /** 休憩時間 */
    private String formattedNightRestTime;

    /** リーダークラス */
    private String workPlanLevel;

    /** リーダー手当(時給) */
    private Integer leaderBonus;
}