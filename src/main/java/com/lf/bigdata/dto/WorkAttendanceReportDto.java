package com.lf.bigdata.dto;

import com.lf.bigdata.mapper.model.WorkAttendance;

import lombok.Getter;
import lombok.Setter;

/**
 * スタッフ出勤
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class WorkAttendanceReportDto extends WorkAttendance {

    /** 休憩時間 */
    private Long restTime;

    /** 休憩時間 */
    private String formattedRestTime;

    private String companyName;

}