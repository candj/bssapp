package com.lf.bigdata.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 作業予定
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class WorkPlanDto {
    /** 作業予定ID */
    @Digits(integer=10, fraction=0)
    private Integer workPlanId;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer workPlanCompanyId;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer workPlanCompany;

    /** 作業予定 */
    private com.lf.bigdata.mapper.model.Company company;

    /** スタッフID */
    @Digits(integer=10, fraction=0)
    private Integer workPlanStaffId;

    /** 作業予定 */
    private com.lf.bigdata.mapper.model.Staff staff;

    /** 日付 */
    private org.joda.time.DateTime workPlanDate;

    /** 社員番号 */
    @Length(max=16)
    private String workPlanNumber;

    /** クラス */
    @Length(max=16)
    private String workPlanLevel;

    /** 氏名 */
    @Length(max=64)
    private String workPlanName;

    /** フリガナ（全角カナ） */
    @Length(max=64)
    private String workPlanNameKana;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
