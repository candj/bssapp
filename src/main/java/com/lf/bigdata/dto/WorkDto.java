package com.lf.bigdata.dto;

import javax.validation.constraints.Digits;

import org.joda.time.DateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkDto {

    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer workId;

    /** スタッフID */
    @Digits(integer=10, fraction=0)
    private Integer workStaffId;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer workCompanyId;

    /** 業務ID */
    @Digits(integer=10, fraction=0)
    private Integer workBusinessId;

    /** 業務日付 */
    private org.joda.time.DateTime workDate;

    /** 出勤時間 */
    private org.joda.time.DateTime workStartTime;

    /** 退勤時間 */
    private org.joda.time.DateTime workEndTime;

    /** 実際出勤時間 */
    private org.joda.time.DateTime realWorkStartTime;

    /** 実際退勤時間 */
    private org.joda.time.DateTime realWorkEndTime;

    /**会社名*/
    String companyName;

    String businessName;

    String staffCode;

    String staffName;

    String type;

    /** 最大退勤時間（出勤だけ） */
    private DateTime limitEndTime;

    /** FALSE:今日　TRUE:明日（出勤だけ） */
    private Boolean limitEndTimeDay;

    /** 業務区分ID */
    private String businessTypeId;
    
    private String businessCode;

    WorkDto oriWorkDto;
}
