package com.lf.bigdata.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 会社
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class CompanyDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** 会社コード */
    @Length(max=64)
    private String companyCode;

    /** 会社名 */
    @Length(max=64)
    private String companyName;

    /** 住所 */
    @Length(max=128)
    private String companyAdress;

    /** 郵便番号 */
    @Length(max=64)
    private String postCode;

    /** 電話番号 */
    @Length(max=64)
    private String telephoneNumber;

    /** 単金 */
    @Digits(integer=10, fraction=0)
    private Integer salary;

    /** リーダー単金 */
    @Digits(integer=10, fraction=0)
    private Integer leaderSalary;

    /** サブリーダー単金 */
    @Digits(integer=10, fraction=0)
    private Integer subLeaderSalary;

    /** トレーナー単金 */
    @Digits(integer=10, fraction=0)
    private Integer trainerSalary;

    /** フォーク単金 */
    @Digits(integer=10, fraction=0)
    private Integer forkSalary;

    /** 交通費 */
    @Digits(integer=10, fraction=0)
    private Integer commutingCost;

    /** ステータス */
    @Length(max=2)
    private String status;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** スタッフ */
    private java.util.List<StaffDto> staff;
}