package com.lf.bigdata.dto;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.Length;
import org.joda.time.DateTime;

import lombok.Getter;
import lombok.Setter;

/**
 * 勤怠業務
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class BusinessAttendanceDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer businessId;

    /** 業務名 */
    @Length(max=64)
    private String businessName;

    /** 業務名「中国語」 */
    @Length(max=64)
    private String businessNameCn;

    /** 業務コード */
    @Length(max=64)
    private String businessCode;

    /** 暗証化コード */
    @Length(max=64)
    private String businessEncryptCode;

    /** 業務開始時間 */
    private DateTime businessStartTime;

    /** 最大退勤時間（出勤だけ） */
    private DateTime limitEndTime;

    /** FALSE:今日　TRUE:明日（出勤だけ） */
    private Boolean limitEndTimeDay;

    /** 業務区分ID */
    @Length(max=2)
    private String businessTypeId;

    /** ステータス */
    @Length(max=2)
    private String status;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private DateTime createTime;

    /** 更新日付 */
    private DateTime updateTime;

    /** スタッフ出勤 */
    private java.util.List<WorkAttendanceDto> workAttendance;
}