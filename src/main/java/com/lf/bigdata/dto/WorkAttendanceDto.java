package com.lf.bigdata.dto;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * スタッフ出勤
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class WorkAttendanceDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer workId;

    /** スタッフID */
    @Digits(integer=10, fraction=0)
    private Integer workStaffId;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer workCompanyId;

    /** 業務ID */
    @Digits(integer=10, fraction=0)
    private Integer workBusinessId;
    
    /** 業務日付 */
    private org.joda.time.DateTime workDate;

    /** 出勤時間 */
    private org.joda.time.DateTime workStartTime;

    /** 実際出勤時間 */
    private org.joda.time.DateTime realWorkStartTime;

    /** 退勤時間 */
    private org.joda.time.DateTime workEndTime;

    /** 実際退勤時間 */
    private org.joda.time.DateTime realWorkEndTime;

    /** ステータス */
    @Length(max=2)
    private String status;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** 勤怠業務 */
    @JsonProperty("businessAttendance")
    private BusinessAttendanceDto businessAttendance;

    /** スタッフID */
    @Length(max=128)
    private String staffCode;

    /** スタッフ */
    @JsonProperty("staff")
    private StaffDto staff;
}