package com.lf.bigdata.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * ロール
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class RoleDto {
    /** ロールID */
    @Digits(integer=10, fraction=0)
    private Integer roleId;

    /** ロールコード */
    @Length(max=16)
    private String roleCode;

    /** ロール名 */
    @Length(max=255)
    private String roleName;

    /** ステータス */
    @Length(max=2)
    private String status;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** ユーザールール */
    private java.util.List<UserRoleDto> userRole;
}