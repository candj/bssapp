package com.lf.bigdata.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CostConditionDto {

    /** 業務開始時間 */
    private org.joda.time.DateTime workStartTime;

    /** 業務完成時間 */
    private org.joda.time.DateTime workEndTime;

    /** 夜勤開始時間 */
    private org.joda.time.DateTime nightStartTime;

    /** 夜勤終了時間 */
    private org.joda.time.DateTime nightEndTime;

}
