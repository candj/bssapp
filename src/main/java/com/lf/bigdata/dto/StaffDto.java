package com.lf.bigdata.dto;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;

/**
 * スタッフ
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class StaffDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer staffId;

    /** スタッフID */
    @Length(max=128)
    private String staffCode;

    /** バーコード */
    @Length(max=128)
    private String staffEncryptCode;

    /** スタッフ名前 */
    @Length(max=64)
    private String staffName;

    /** 区別 */
    @Length(max=64)
    private String staffWorkType;

    /** 会社 */
    @Digits(integer=10, fraction=0)
    private Integer staffCompanyId;

    /** 派遣会社作業員番号 */
    @Length(max=128)
    private String workerNumber;

    /** 業務等級 */
    @Length(max=64)
    private String bussinessGrade;

    /** 説明 */
    @Length(max=512)
    private String remark;

    /** ステータス */
    @Length(max=2)
    private String status;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** スタッフ出勤 */
    private java.util.List<WorkAttendanceDto> workAttendance;

    /** スタッフ作業 */
    private java.util.List<WorkTaskDto> workTask;

    /** 掲示板表示状態 */
    private Boolean blackStatus;

    /** 会社 */
    private CompanyDto company;
}