package com.lf.bigdata.dto;

import org.joda.time.DateTime;

import com.lf.bigdata.enums.Operating;
import com.lf.bigdata.mapper.model.Staff;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Work<T> {
	public static final int WORK_FLAG_TASK = 1;
	public static final int WORK_FLAG_ATTENDANCE = 2;

	T oriObj;


	public Work() {
	}

    /** ID */
    private Integer workId;

    /** スタッフID */
    private Integer workStaffId;

    /** 会社ID */
    private Integer workCompanyId;

    /** 業務ID */
    private Integer workBusinessId;

    /** 出勤時間 */
    private DateTime workStartTime;

    /** 実際出勤時間 */
    private DateTime realWorkStartTime;

    /** 退勤時間 */
    private DateTime workEndTime;

    /** 実際退勤時間 */
    private DateTime realWorkEndTime;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private DateTime createTime;

    /** 更新日付 */
    private DateTime updateTime;

    /** 勤怠業務 */
    private BusinessDto business;

    /** スタッフ */
    private Staff staff;

    private int workFlag;

    //操作
    private Operating operation;

}