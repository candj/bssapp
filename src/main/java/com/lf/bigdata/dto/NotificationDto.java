package com.lf.bigdata.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 掲示板情報
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class NotificationDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer notificationId;

    /** タイトル */
    @Length(max=256)
    private String notificationTitle;

    /** 作業区分ID */
    @Digits(integer=10, fraction=0)
    private Integer notificationProcessId;

    /** 掲示板情報 */
    private com.lf.bigdata.mapper.model.Process process;

    /** 掲示板情報 */
    private ProcessDto processDto;

    /** 編集可能状態 */
    private Boolean notificationStatus;

    /** 最新開放時間 */
    private org.joda.time.DateTime notificationOpenTime;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** 内容 */
    @Length(max=2147483647)
    private String notificationContent;

    /** opened status */
    private boolean openedRecently;

    /** edited status */
    private boolean editedRecently;

    private Integer lastProcessId;
}
