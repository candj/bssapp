package com.lf.bigdata.dto;

import java.util.List;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.Length;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessDto {

    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer businessId;

    /** 業務名 */
    @Length(max=64)
    private String businessName;

    /** 業務名「中国語」 */
    @Length(max=64)
    private String businessNameCn;

    /** 業務コード */
    @Length(max=64)
    private String businessCode;

    /** 暗証化コード */
    @Length(max=64)
    private String businessEncryptCode;

    /** ブランド */
    @Length(max=64)
    private String area;

    /** 業務区別 */
    @Length(max=64)
    private String process;

    /** マーク */
    @Length(max=5)
    private String mark;

    /** 業務モード */
    @Length(max=64)
    private String chargeType;

    /** 業務開始時間 */
    @DateTimeFormat(pattern = "HH:mm")
    private DateTime businessStartTime;

    /** 最大退勤時間（出勤だけ） */
    @DateTimeFormat(pattern = "HH:mm")
    private DateTime limitEndTime;

    /** FALSE:今日　TRUE:明日（出勤だけ） */
    private Boolean limitEndTimeDay;

    /** 業務区分ID */
    private String businessTypeId;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private DateTime createTime;

    /** 更新日付 */
    private DateTime updateTime;

    /** スタッフ出勤 */
    private List<WorkTaskDto> work;


}
