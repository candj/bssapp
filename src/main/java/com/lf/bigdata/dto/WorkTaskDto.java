package com.lf.bigdata.dto;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;

/**
 * スタッフ作業
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class WorkTaskDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer workId;

    /** スタッフID */
    @Digits(integer=10, fraction=0)
    private Integer workStaffId;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer workCompanyId;

    /** 業務ID */
    @Digits(integer=10, fraction=0)
    private Integer workBusinessId;
    
    /** 業務日付 */
    private org.joda.time.DateTime workDate;
    

    /** 業務開始時間 */
    private org.joda.time.DateTime workStartTime;

    /** 業務完成時間 */
    private org.joda.time.DateTime workEndTime;

    /** ステータス */
    @Length(max=2)
    private String status;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** 作業業務 */
    private BusinessTaskDto businessTask;

    /** スタッフID */
    @Length(max=128)
    private String staffCode;

    /** スタッフ */
    private StaffDto staff;
}