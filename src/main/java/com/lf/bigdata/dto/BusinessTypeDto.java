package com.lf.bigdata.dto;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.Length;
import org.joda.time.DateTime;

import lombok.Getter;
import lombok.Setter;

/**
 * null
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class BusinessTypeDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer id;

    /** 業務区分名 */
    @Length(max=64)
    private String businessTypeName;

    /** 01：出勤　02：作業　03：退勤　04：休憩45分　05：休憩15分　06：トイレ */
    @Length(max=64)
    private String businessTypeCode;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private DateTime createTime;

    /** 更新日付 */
    private DateTime updateTime;


}