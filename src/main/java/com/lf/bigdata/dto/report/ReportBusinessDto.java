package com.lf.bigdata.dto.report;

public class ReportBusinessDto {
	
    private String mark;
    private String workName;
    private String workNameCn;

    private String areaName;
    private String process;
    private String chargeType;


    private ReportWorkHourDto parttimeHourDto;
    private ReportWorkHourDto temporaryHourDto;


	public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }
    
	public String getWorkNameCn() {
		return workNameCn;
	}

	public void setWorkNameCn(String workNameCn) {
		this.workNameCn = workNameCn;
	}

	public ReportWorkHourDto getParttimeHourDto() {
		return parttimeHourDto;
	}

	public void setParttimeHourDto(ReportWorkHourDto parttimeHourDto) {
		this.parttimeHourDto = parttimeHourDto;
	}

	public ReportWorkHourDto getTemporaryHourDto() {
		return temporaryHourDto;
	}

	public void setTemporaryHourDto(ReportWorkHourDto temporaryHourDto) {
		this.temporaryHourDto = temporaryHourDto;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

}
