package com.lf.bigdata.dto.report;


public class ReportWorkHourDto {

    private double workTime7;
    private double workTime8;
    private double workTime9;
    private double workTime10;
    private double workTime11;
    private double workTime12;
    private double workTime13;
    private double workTime14;
    private double workTime15;
    private double workTime16;
    private double workTime17;
    private double workTime18;
    private double workTime19;
    private double workTime20;
    private double workTime21;
    private double workTime22;
    private double workTime23;
    private double workTime0;
    private double workTime1;
    private double workTime2;
    private double workTime3;
    private double workTime4;
    private double workTime5;
    private double workTime6;
    private double dayTotal;
    private double nightTotal;
    private double total;

    public double getWorkTime7() {
        return workTime7;
    }

    public void setWorkTime7(double workTime7) {
        this.workTime7 = workTime7;
    }

    public double getWorkTime8() {
        return workTime8;
    }

    public void setWorkTime8(double workTime8) {
        this.workTime8 = workTime8;
    }

    public double getWorkTime9() {
        return workTime9;
    }

    public void setWorkTime9(double workTime9) {
        this.workTime9 = workTime9;
    }

    public double getWorkTime10() {
        return workTime10;
    }

    public void setWorkTime10(double workTime10) {
        this.workTime10 = workTime10;
    }

    public double getWorkTime11() {
        return workTime11;
    }

    public void setWorkTime11(double workTime11) {
        this.workTime11 = workTime11;
    }

    public double getWorkTime12() {
        return workTime12;
    }

    public void setWorkTime12(double workTime12) {
        this.workTime12 = workTime12;
    }

    public double getWorkTime13() {
        return workTime13;
    }

    public void setWorkTime13(double workTime13) {
        this.workTime13 = workTime13;
    }

    public double getWorkTime14() {
        return workTime14;
    }

    public void setWorkTime14(double workTime14) {
        this.workTime14 = workTime14;
    }

    public double getWorkTime15() {
        return workTime15;
    }

    public void setWorkTime15(double workTime15) {
        this.workTime15 = workTime15;
    }

    public double getWorkTime16() {
        return workTime16;
    }

    public void setWorkTime16(double workTime16) {
        this.workTime16 = workTime16;
    }

    public double getWorkTime17() {
        return workTime17;
    }

    public void setWorkTime17(double workTime17) {
        this.workTime17 = workTime17;
    }

    public double getWorkTime18() {
        return workTime18;
    }

    public void setWorkTime18(double workTime18) {
        this.workTime18 = workTime18;
    }

    public double getWorkTime19() {
        return workTime19;
    }

    public void setWorkTime19(double workTime19) {
        this.workTime19 = workTime19;
    }

    public double getWorkTime20() {
        return workTime20;
    }

    public void setWorkTime20(double workTime20) {
        this.workTime20 = workTime20;
    }

    public double getWorkTime21() {
        return workTime21;
    }

    public void setWorkTime21(double workTime21) {
        this.workTime21 = workTime21;
    }

    public double getWorkTime22() {
        return workTime22;
    }

    public void setWorkTime22(double workTime22) {
        this.workTime22 = workTime22;
    }

    public double getWorkTime23() {
        return workTime23;
    }

    public void setWorkTime23(double workTime23) {
        this.workTime23 = workTime23;
    }

    public double getWorkTime0() {
        return workTime0;
    }

    public void setWorkTime0(double workTime0) {
        this.workTime0 = workTime0;
    }

    public double getWorkTime1() {
        return workTime1;
    }

    public void setWorkTime1(double workTime1) {
        this.workTime1 = workTime1;
    }

    public double getWorkTime2() {
        return workTime2;
    }

    public void setWorkTime2(double workTime2) {
        this.workTime2 = workTime2;
    }

    public double getWorkTime3() {
        return workTime3;
    }

    public void setWorkTime3(double workTime3) {
        this.workTime3 = workTime3;
    }

    public double getWorkTime4() {
        return workTime4;
    }

    public void setWorkTime4(double workTime4) {
        this.workTime4 = workTime4;
    }

    public double getWorkTime5() {
        return workTime5;
    }

    public void setWorkTime5(double workTime5) {
        this.workTime5 = workTime5;
    }

    public double getWorkTime6() {
        return workTime6;
    }

    public void setWorkTime6(double workTime6) {
        this.workTime6 = workTime6;
    }

	public double getDayTotal() {
		return dayTotal;
	}

	public void setDayTotal(double dayTotal) {
		this.dayTotal = dayTotal;
	}

	public double getNightTotal() {
		return nightTotal;
	}

	public void setNightTotal(double nightTotal) {
		this.nightTotal = nightTotal;
	}

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
    
}