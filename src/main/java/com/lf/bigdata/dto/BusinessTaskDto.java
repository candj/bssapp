package com.lf.bigdata.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 作業業務
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class BusinessTaskDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer businessId;

    /** 業務名 */
    @Length(max=64)
    private String businessName;

    /** 業務名「中国語」 */
    @Length(max=64)
    private String businessNameCn;

    /** 業務コード */
    @Length(max=64)
    private String businessCode;

    /** 暗証化コード */
    @Length(max=64)
    private String businessEncryptCode;

    /** ブランド */
    @Length(max=64)
    private String area;

    /** 業務区別 */
    @Length(max=64)
    private String process;

    /** マーク */
    @Length(max=5)
    private String mark;

    /** 業務モード */
    @Length(max=64)
    private String chargeType;

    /** 業務区分ID */
    @Length(max=2)
    private String businessTypeId;

    /** ステータス */
    @Length(max=2)
    private String status;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    /** スタッフ作業 */
    private java.util.List<WorkTaskDto> workTask;
}