package com.lf.bigdata.dao.mapper;

import com.candj.webpower.web.core.model.WhereCondition;
import com.lf.bigdata.mapper.model.Notification;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 掲示板情報情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface NotificationDao {
    /**
     * プライマリーキーで掲示板情報を検索する。
     * @param notificationId ID
     * @return 結果
     */
    Notification selectByPrimaryKey(Integer notificationId);

    /**
     * プライマリーキーで掲示板情報検索する。（連携情報含む）
     * @param notificationId ID
     * @return 結果
     */
    List<Notification> selectAllByPrimaryKey(Integer notificationId);

    /**
     * 条件で掲示板情報を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Notification> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で掲示板情報を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Notification> selectByExample(@Param("example") WhereCondition example);

    /**
     * 掲示板情報を新規追加する。
     * @param record 掲示板情報
     * @return 結果
     */
    int insertSelective(Notification record);

    /**
     * 掲示板情報を新規追加する。
     * @param record 掲示板情報
     * @return 結果
     */
    int insert(Notification record);

    /**
     * プライマリーキーで掲示板情報を更新する。
     * @param record 掲示板情報
     * @return 結果
     */
    int updateByPrimaryKey(Notification record);

    /**
     * プライマリーキーで掲示板情報を更新する。
     * @param record 掲示板情報
     * @return 結果
     */
    int updateByPrimaryKeySelective(Notification record);

    /**
     * 掲示板情報を更新する。
     * @param record 掲示板情報
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Notification record, @Param("example") WhereCondition example);

    /**
     * 掲示板情報を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Notification example);

    /**
     * 掲示板情報を削除する。
     * @param notificationId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer notificationId);
}
