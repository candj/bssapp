package com.lf.bigdata.dao.mapper;

import com.candj.webpower.web.core.model.WhereCondition;
import com.lf.bigdata.mapper.model.Company;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 会社情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface CompanyDao {
    /**
     * プライマリーキーで会社を検索する。
     * @param companyId ID
     * @return 結果
     */
    Company selectByPrimaryKey(Integer companyId);

    /**
     * プライマリーキーで会社検索する。（連携情報含む）
     * @param companyId ID
     * @return 結果
     */
    List<Company> selectAllByPrimaryKey(Integer companyId);

    /**
     * 条件で会社を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Company> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で会社を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Company> selectByExample(@Param("example") WhereCondition example);

    /**
     * 会社を新規追加する。
     * @param record 会社
     * @return 結果
     */
    int insertSelective(Company record);

    /**
     * 会社を新規追加する。
     * @param record 会社
     * @return 結果
     */
    int insert(Company record);

    /**
     * プライマリーキーで会社を更新する。
     * @param record 会社
     * @return 結果
     */
    int updateByPrimaryKey(Company record);

    /**
     * プライマリーキーで会社を更新する。
     * @param record 会社
     * @return 結果
     */
    int updateByPrimaryKeySelective(Company record);

    /**
     * 会社を更新する。
     * @param record 会社
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Company record, @Param("example") WhereCondition example);

    /**
     * 会社を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Company example);

    /**
     * 会社を削除する。
     * @param companyId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer companyId);
}