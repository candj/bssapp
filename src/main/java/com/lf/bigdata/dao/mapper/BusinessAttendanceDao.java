package com.lf.bigdata.dao.mapper;

import com.candj.webpower.web.core.model.WhereCondition;
import com.lf.bigdata.mapper.model.BusinessAttendance;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 勤怠業務情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface BusinessAttendanceDao {
    /**
     * プライマリーキーで勤怠業務を検索する。
     * @param businessId ID
     * @return 結果
     */
    BusinessAttendance selectByPrimaryKey(Integer businessId);

    /**
     * プライマリーキーで勤怠業務検索する。（連携情報含む）
     * @param businessId ID
     * @return 結果
     */
    List<BusinessAttendance> selectAllByPrimaryKey(Integer businessId);

    /**
     * 条件で勤怠業務を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<BusinessAttendance> selectByTypeCode(String businessTypeCode);

    /**
     * 勤怠業務を新規追加する。
     * @param record 勤怠業務
     * @return 結果
     */
    int insertSelective(BusinessAttendance record);

    /**
     * 勤怠業務を新規追加する。
     * @param record 勤怠業務
     * @return 結果
     */
    int insert(BusinessAttendance record);

    /**
     * プライマリーキーで勤怠業務を更新する。
     * @param record 勤怠業務
     * @return 結果
     */
    int updateByPrimaryKey(BusinessAttendance record);

    /**
     * プライマリーキーで勤怠業務を更新する。
     * @param record 勤怠業務
     * @return 結果
     */
    int updateByPrimaryKeySelective(BusinessAttendance record);

    /**
     * 勤怠業務を更新する。
     * @param record 勤怠業務
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(BusinessAttendance record, @Param("example") WhereCondition example);

    /**
     * 勤怠業務を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") BusinessAttendance example);

    /**
     * 勤怠業務を削除する。
     * @param businessId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer businessId);

    /**
     * 条件で勤怠業務を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<BusinessAttendance> selectByExample(@Param("example") WhereCondition example);

    /**
     * 条件で勤怠業務を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<BusinessAttendance> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 勤怠業務データ件数を取得する
     * @param example 検索条件
     * @return 結果
     */
    int selectCountByExample(@Param("example") WhereCondition example);
}