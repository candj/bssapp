package com.lf.bigdata.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.joda.time.DateTime;

import com.candj.webpower.web.core.model.WhereCondition;
import com.lf.bigdata.mapper.model.BusinessTask;

/**
 * 作業業務情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface BusinessTaskDao {
    /**
     * プライマリーキーで作業業務を検索する。
     * @param businessId ID
     * @return 結果
     */
    BusinessTask selectByPrimaryKey(Integer businessId);

    /**
     * プライマリーキーで作業業務検索する。（連携情報含む）
     * @param businessId ID
     * @return 結果
     */
    List<BusinessTask> selectAllByPrimaryKey(Integer businessId);

    /**
     * 条件で業務を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<BusinessTask> selectAllByTime(@Param("workStartTime") DateTime workStartTime, @Param("workEndTime")DateTime workEndTime/**, @Param("example") WhereCondition example*/);


    /**
     * 作業業務を新規追加する。
     * @param record 作業業務
     * @return 結果
     */
    int insertSelective(BusinessTask record);

    /**
     * 作業業務を新規追加する。
     * @param record 作業業務
     * @return 結果
     */
    int insert(BusinessTask record);

    /**
     * プライマリーキーで作業業務を更新する。
     * @param record 作業業務
     * @return 結果
     */
    int updateByPrimaryKey(BusinessTask record);

    /**
     * プライマリーキーで作業業務を更新する。
     * @param record 作業業務
     * @return 結果
     */
    int updateByPrimaryKeySelective(BusinessTask record);

    /**
     * 作業業務を更新する。
     * @param record 作業業務
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(BusinessTask record, @Param("example") WhereCondition example);

    /**
     * 作業業務を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") BusinessTask example);

    /**
     * 作業業務を削除する。
     * @param businessId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer businessId);

    /**
     * 条件で作業業務を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<BusinessTask> selectByExample(@Param("example") WhereCondition example);

    /**
     * 条件で作業業務を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<BusinessTask> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 業務データ件数を取得する
     * @param example 検索条件
     * @return 結果
     */
    int selectCountByExample(@Param("example") WhereCondition example);
}