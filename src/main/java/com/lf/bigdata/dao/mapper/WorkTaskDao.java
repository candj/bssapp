package com.lf.bigdata.dao.mapper;

import com.candj.webpower.web.core.model.WhereCondition;
import com.lf.bigdata.mapper.model.WorkTask;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * スタッフ作業情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface WorkTaskDao {
    /**
     * プライマリーキーでスタッフ作業を検索する。
     * @param workId ID
     * @return 結果
     */
    WorkTask selectByPrimaryKey(Integer workId);

    /**
     * プライマリーキーでスタッフ作業検索する。（連携情報含む）
     * @param workId ID
     * @return 結果
     */
    List<WorkTask> selectAllByPrimaryKey(Integer workId);

    /**
     * スタッフ作業を新規追加する。
     * @param record スタッフ作業
     * @return 結果
     */
    int insertSelective(WorkTask record);

    /**
     * スタッフ作業を新規追加する。
     * @param record スタッフ作業
     * @return 結果
     */
    int insert(WorkTask record);

    /**
     * プライマリーキーでスタッフ作業を更新する。
     * @param record スタッフ作業
     * @return 結果
     */
    int updateByPrimaryKey(WorkTask record);

    /**
     * プライマリーキーでスタッフ作業を更新する。
     * @param record スタッフ作業
     * @return 結果
     */
    int updateByPrimaryKeySelective(WorkTask record);

    /**
     * スタッフ作業を更新する。
     * @param record スタッフ作業
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(WorkTask record, @Param("example") WhereCondition example);

    /**
     * スタッフ作業を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") WorkTask example);

    /**
     * スタッフ作業を削除する。
     * @param workId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer workId);

    /**
     * 条件でスタッフ作業を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<WorkTask> selectByExample(@Param("example") WhereCondition example);

    /**
     * 条件でスタッフ作業を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<WorkTask> selectAllByExample(@Param("example") WhereCondition example);
}