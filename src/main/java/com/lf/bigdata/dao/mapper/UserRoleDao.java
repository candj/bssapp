package com.lf.bigdata.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.candj.webpower.web.core.model.WhereCondition;
import com.lf.bigdata.mapper.model.UserRole;

/**
 * ユーザールール情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface UserRoleDao {
    /**
     * プライマリーキーでユーザールールを検索する。
     * @param id ID
     * @return 結果
     */
    UserRole selectByPrimaryKey(Integer id);

    /**
     * プライマリーキーでユーザールール検索する。（連携情報含む）
     * @param id ID
     * @return 結果
     */
    List<UserRole> selectAllByPrimaryKey(Integer id);

    /**
     * 条件でユーザールールを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<UserRole> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でユーザールールを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<UserRole> selectByExample(@Param("example") WhereCondition example);

    /**
     * ユーザールールを新規追加する。
     * @param record ユーザールール
     * @return 結果
     */
    int insertSelective(UserRole record);

    /**
     * ユーザールールを新規追加する。
     * @param record ユーザールール
     * @return 結果
     */
    int insert(UserRole record);

    /**
     * プライマリーキーでユーザールールを更新する。
     * @param record ユーザールール
     * @return 結果
     */
    int updateByPrimaryKey(UserRole record);

    /**
     * プライマリーキーでユーザールールを更新する。
     * @param record ユーザールール
     * @return 結果
     */
    int updateByPrimaryKeySelective(UserRole record);

    /**
     * ユーザールールを更新する。
     * @param record ユーザールール
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(UserRole record, @Param("example") WhereCondition example);

    /**
     * ユーザールールを削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") WhereCondition example);

    /**
     * ユーザールールを削除する。
     * @param id ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer id);
}