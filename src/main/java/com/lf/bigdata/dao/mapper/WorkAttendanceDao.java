package com.lf.bigdata.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.candj.webpower.web.core.model.WhereCondition;
import com.lf.bigdata.dto.CostConditionDto;
import com.lf.bigdata.dto.WorkAttendanceReportDto;
import com.lf.bigdata.dto.WorkCostReportDto;
import com.lf.bigdata.dto.WorkDto;
import com.lf.bigdata.mapper.model.WorkAttendance;

/**
 * スタッフ出勤情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface WorkAttendanceDao {
	/**
	 * プライマリーキーでスタッフ出勤を検索する。
	 * @param workId ID
	 * @return 結果
	 */
	WorkAttendance selectByPrimaryKey(Integer workId);

	/**
	 * プライマリーキーでスタッフ出勤検索する。（連携情報含む）
	 * @param workId ID
	 * @return 結果
	 */
	List<WorkAttendance> selectAllByPrimaryKey(Integer workId);

	/**
	 * スタッフ出勤を新規追加する。
	 * @param record スタッフ出勤
	 * @return 結果
	 */
	int insertSelective(WorkAttendance record);

	/**
	 * スタッフ出勤を新規追加する。
	 * @param record スタッフ出勤
	 * @return 結果
	 */
	int insert(WorkAttendance record);

	/**
	 * プライマリーキーでスタッフ出勤を更新する。
	 * @param record スタッフ出勤
	 * @return 結果
	 */
	int updateByPrimaryKey(WorkAttendance record);

	/**
	 * プライマリーキーでスタッフ出勤を更新する。
	 * @param record スタッフ出勤
	 * @return 結果
	 */
	int updateByPrimaryKeySelective(WorkAttendance record);

	/**
	 * スタッフ出勤を更新する。
	 * @param record スタッフ出勤
	 * @param example 更新条件
	 * @return 結果
	 */
	int updateByExampleSelective(WorkAttendance record, @Param("example") WhereCondition example);

	/**
	 * スタッフ出勤を削除する。
	 * @param example 削除条件
	 * @return 結果
	 */
	int deleteByExample(@Param("example") WorkAttendance example);

	/**
	 * スタッフ出勤を削除する。
	 * @param workId ID
	 * @return 結果
	 */
	int deleteByPrimaryKey(Integer workId);

	/**
	 * 条件でスタッフ出勤を検索する。（連携情報含む）)
	 * @param example 検索条件
	 * @return 結果
	 */
	List<WorkAttendance> selectByExample(@Param("example") WhereCondition example);

	/**
	 * 条件でスタッフ出勤を検索する。（連携情報含む）)
	 * @param example 検索条件
	 * @return 結果
	 */
	List<WorkAttendance> selectAllByExample(@Param("example") WhereCondition example);

	List<WorkDto> selectAllOneDayWorks(WorkAttendance record);



	List<WorkAttendanceReportDto> selectOneDayAttendances(@Param("example") WhereCondition example);

	List<WorkCostReportDto> selectCost(CostConditionDto record, @Param("example") WhereCondition example);


	List<WorkDto> selectNearWorks(WorkAttendance record);

	List<WorkDto> selectStartLeaveWorks(WorkAttendance record);

	/**
	 * 条件でスタッフ出勤を検索する。（連携情報含む）)
	 * @param example 検索条件
	 * @return 結果
	 */
	List<WorkAttendance> selectAllByStaffAndTypeID(@Param("example") WhereCondition example);

	/**
	 * 条件でスタッフ出勤を検索する。（連携情報含む）)
	 * @param example 検索条件
	 * @return 結果
	 */
	List<WorkAttendance> selectLastWorkDay(@Param("example") WhereCondition example);

	/**
	 * 条件でスタッフ出勤を検索する。（連携情報含む）)
	 * @param example 検索条件
	 * @return 結果
	 */
	List<WorkDto> selectOneDayWorks(@Param("example") WhereCondition example);

}