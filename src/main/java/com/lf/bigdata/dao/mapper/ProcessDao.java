package com.lf.bigdata.dao.mapper;

import com.candj.webpower.web.core.model.WhereCondition;
import com.lf.bigdata.mapper.model.Process;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 作業区分情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface ProcessDao {
    /**
     * プライマリーキーで作業区分を検索する。
     * @param processId ID
     * @return 結果
     */
    Process selectByPrimaryKey(Integer processId);

    /**
     * プライマリーキーで作業区分検索する。（連携情報含む）
     * @param processId ID
     * @return 結果
     */
    List<Process> selectAllByPrimaryKey(Integer processId);

    /**
     * 条件で作業区分を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Process> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で作業区分を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Process> selectByExample(@Param("example") WhereCondition example);

    /**
     * 作業区分を新規追加する。
     * @param record 作業区分
     * @return 結果
     */
    int insertSelective(Process record);

    /**
     * 作業区分を新規追加する。
     * @param record 作業区分
     * @return 結果
     */
    int insert(Process record);

    /**
     * プライマリーキーで作業区分を更新する。
     * @param record 作業区分
     * @return 結果
     */
    int updateByPrimaryKey(Process record);

    /**
     * プライマリーキーで作業区分を更新する。
     * @param record 作業区分
     * @return 結果
     */
    int updateByPrimaryKeySelective(Process record);

    /**
     * 作業区分を更新する。
     * @param record 作業区分
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Process record, @Param("example") WhereCondition example);

    /**
     * 作業区分を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Process example);

    /**
     * 作業区分を削除する。
     * @param processId ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer processId);
}
