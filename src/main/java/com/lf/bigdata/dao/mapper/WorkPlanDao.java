package com.lf.bigdata.dao.mapper;

import com.candj.webpower.web.core.model.WhereCondition;
import com.lf.bigdata.mapper.model.WorkPlan;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 作業予定情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface WorkPlanDao {
    /**
     * プライマリーキーで作業予定を検索する。
     * @param workPlanId 作業予定ID
     * @return 結果
     */
    WorkPlan selectByPrimaryKey(Integer workPlanId);

    /**
     * プライマリーキーで作業予定検索する。（連携情報含む）
     * @param workPlanId 作業予定ID
     * @return 結果
     */
    List<WorkPlan> selectAllByPrimaryKey(Integer workPlanId);

    /**
     * 条件で作業予定を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<WorkPlan> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で作業予定を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<WorkPlan> selectByExample(@Param("example") WhereCondition example);

    /**
     * 作業予定を新規追加する。
     * @param record 作業予定
     * @return 結果
     */
    int insertSelective(WorkPlan record);

    /**
     * 作業予定を新規追加する。
     * @param record 作業予定
     * @return 結果
     */
    int insert(WorkPlan record);

    /**
     * プライマリーキーで作業予定を更新する。
     * @param record 作業予定
     * @return 結果
     */
    int updateByPrimaryKey(WorkPlan record);

    /**
     * プライマリーキーで作業予定を更新する。
     * @param record 作業予定
     * @return 結果
     */
    int updateByPrimaryKeySelective(WorkPlan record);

    /**
     * 作業予定を更新する。
     * @param record 作業予定
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(WorkPlan record, @Param("example") WhereCondition example);

    /**
     * 作業予定を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") WorkPlan example);

    /**
     * 作業予定を削除する。
     * @param workPlanId 作業予定ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer workPlanId);
}
