package com.lf.bigdata.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.dto.WorkTaskDto;
import com.lf.bigdata.service.BusinessAttendanceService;
import com.lf.bigdata.service.WorkAttendanceService;
import com.lf.bigdata.service.WorkTaskService;

/**
 * スタッフ出勤情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/workAttendance")
@Controller
public class WorkAttendanceController {
    @Autowired
    WorkAttendanceService workAttendanceService;

    @Autowired
    BusinessAttendanceService businessAttendanceService;
    @Autowired
    WorkTaskService workTaskService;
    /**
     * スタッフ出勤を新規追加する。
     * @param workAttendanceDto スタッフ出勤
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(WorkAttendanceDto workAttendanceDto, ModelMap model) {
        return new ModelAndView("workAttendance/insert", model);
    }

    /**
     * スタッフ出勤を新規追加(確認)する。
     * @param workAttendanceDto スタッフ出勤
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid WorkAttendanceDto workAttendanceDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("workAttendance/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("workAttendance/insertConfirm", model);
    }

    /**
     * スタッフ出勤を新規追加(終了)する。
     * @param workAttendanceDto スタッフ出勤
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid WorkAttendanceDto workAttendanceDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("workAttendance/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //スタッフ出勤を新規追加する。
        int ret = workAttendanceService.insertSelective(workAttendanceDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("workAttendance/insertFinish", model);
    }

    /**
     * スタッフ出勤を編集する。
     * @param workId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{workId}/{type}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable(name="workId") Integer workId, @PathVariable(name="type") String type, ModelMap model) {
        if("1".equals(type)) {
        	//プライマリーキーでスタッフ出勤を検索する。
            WorkAttendanceDto workAttendanceDto = workAttendanceService.selectAllByPrimaryKey(workId);

            workAttendanceDto.setStaffCode(workAttendanceDto.getStaff().getStaffCode());

            model.addAttribute("workAttendanceDto", workAttendanceDto);
            return new ModelAndView("workAttendance/edit", model);
        } else {
            //プライマリーキーでスタッフ作業を検索する。
            WorkTaskDto workTaskDto = workTaskService.selectAllByPrimaryKey(workId);

            workTaskDto.setStaffCode(workTaskDto.getStaff().getStaffCode());

            model.addAttribute("workTaskDto", workTaskDto);
            return new ModelAndView("workTask/edit", model);
        }


    }

    /**
     * スタッフ出勤を編集(確認)する。
     * @param workAttendanceDto スタッフ出勤
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid WorkAttendanceDto workAttendanceDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("workAttendance/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("workAttendance/editConfirm", model);
    }

    /**
     * スタッフ出勤を編集(終了)する。
     * @param workAttendanceDto スタッフ出勤
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid WorkAttendanceDto workAttendanceDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("workAttendance/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでスタッフ出勤を更新する。
        int ret = workAttendanceService.updateByPrimaryKeySelective(workAttendanceDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("workAttendance/editFinish", model);
    }

    /**
     * スタッフ出勤一覧画面を表示する。
     * @param workAttendanceDto スタッフ出勤
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("workAttendanceDto") WorkAttendanceDto workAttendanceDto, ModelMap model) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = df.format(new Date());
        model.addAttribute("workStartTime", dateStr);
        return new ModelAndView("workAttendance/list", model);
    }

    /**
     * スタッフ出勤一覧画面を表示する。
     *
     * @param workAttendanceDto
     *            スタッフ出勤
     * @param model
     *            モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/leaveWorkList", method = RequestMethod.GET)
    public ModelAndView confirmList(@ModelAttribute("workAttendanceDto") WorkAttendanceDto workAttendanceDto, ModelMap model) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = df.format(new Date());
        model.addAttribute("workStartTime", dateStr);
        return new ModelAndView("workAttendance/leaveWorkList", model);
    }

    /**
     * スタッフ出勤を編集する。
     *
     * @param workId
     *            ID
     * @param model
     *            モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/leaveWork/{workId}", method = RequestMethod.GET)
    public ModelAndView confirmEdit(@PathVariable Integer workId, ModelMap model) {
        // プライマリーキーでスタッフ出勤を検索する。
        WorkAttendanceDto workAttendanceDto = workAttendanceService.selectAllByPrimaryKey(workId);

        workAttendanceDto.setStaffCode(workAttendanceDto.getStaff().getStaffCode());

        model.addAttribute("workAttendanceDto", workAttendanceDto);
        return new ModelAndView("workAttendance/leaveWork", model);
    }

    @RequestMapping(value = "/lastday", method = RequestMethod.GET)
    public ModelAndView latestWorkRecord(@ModelAttribute("workTaskDto") WorkTaskDto workTaskDto, ModelMap model){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = df.format(new Date());
        model.addAttribute("workStartTime", dateStr);
        return new ModelAndView("workAttendance/lastday", model);
    }


}