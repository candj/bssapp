package com.lf.bigdata.controller;

import com.lf.bigdata.dto.AppUserDto;
import com.lf.bigdata.dto.NotificationDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@RequestMapping
@Controller
public class NotificationController {

    @RequestMapping(value = "/notification", method = RequestMethod.GET)
    public ModelAndView insert(NotificationDto notificationDto, ModelMap model) {
        return new ModelAndView("notificationList", model);
    }

}
