package com.lf.bigdata.controller;

import com.lf.bigdata.dto.ProcessDto;
import com.lf.bigdata.service.ProcessService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 作業区分情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/process")
@Controller
public class ProcessController {
    @Autowired
    ProcessService processService;

    /**
     * 作業区分を新規追加する。
     * @param processDto 作業区分
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(ProcessDto processDto, ModelMap model) {
        return new ModelAndView("process/insert", model);
    }

    /**
     * 作業区分を新規追加(確認)する。
     * @param processDto 作業区分
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid ProcessDto processDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/process/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("process/insertConfirm", model);
    }

    /**
     * 作業区分を新規追加(終了)する。
     * @param processDto 作業区分
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid ProcessDto processDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/process/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //作業区分を新規追加する。
        int ret = processService.insertSelective(processDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/process/insertFinish", model);
    }

    /**
     * 作業区分を編集する。
     * @param processId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{processId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer processId, ModelMap model) {
        //プライマリーキーで作業区分を検索する。
        ProcessDto processDto = processService.selectByPrimaryKey(processId);
        model.addAttribute("processDto", processDto);
        return new ModelAndView("process/edit", model);
    }

    /**
     * 作業区分を編集(確認)する。
     * @param processDto 作業区分
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid ProcessDto processDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/process/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("process/editConfirm", model);
    }

    /**
     * 作業区分を編集(終了)する。
     * @param processDto 作業区分
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid ProcessDto processDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/process/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで作業区分を更新する。
        int ret = processService.updateByPrimaryKeySelective(processDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("process/editFinish", model);
    }

    /**
     * 作業区分一覧画面を表示する。
     * @param processDto 作業区分
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("processDto") ProcessDto processDto, ModelMap model) {
        return new ModelAndView("process/list", model);
    }
}
