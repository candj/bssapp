package com.lf.bigdata.controller;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.lf.bigdata.dto.StaffDto;
import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.dto.WorkAttendanceReportDto;
import com.lf.bigdata.dto.WorkCostReportDto;
import com.lf.bigdata.dto.WorkTaskDto;
import com.lf.bigdata.service.ReportService;
import com.lf.bigdata.service.StaffService;
import com.lf.bigdata.service.WorkAttendanceService;
import com.lf.bigdata.utils.ReportUtil;

/**
 * レポート出力を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */

@RequestMapping(value = "/report")
@Controller
public class ReportController {
	/**スタッフテンプレート*/
	private static final String STAFF_TEMPLATE = "template/staffListTemplate.xlsx";
	/**勤務テンプレート*/
	private static final String ATTENDANCE_TEMPLATE = "template/attendanceTemplate.xlsx";
	private static final String COST_TEMPLATE = "template/costTemplate.xlsx";

	@Value("${tempfolder}")
	private String tempFolder;

	@Autowired
	ReportService reportService;

	@Autowired
	WorkAttendanceService workAttendanceService;

	@Autowired
	StaffService staffService;

	@Autowired
	ApplicationContext ac;

	/**
	 * スタッフ出勤一覧画面を表示する。
	 *
	 * @param workAttendanceDto
	 *            スタッフ出勤
	 * @param model
	 *            モデルマップ
	 * @return 結果
	 */
	@RequestMapping(value = "/attendance", method = RequestMethod.GET)
	public ModelAndView attendanceList(@ModelAttribute("workAttendanceDto") WorkAttendanceDto workAttendanceDto,
			ModelMap model) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = df.format(new Date());
		model.addAttribute("workStartTime", dateStr);
		model.addAttribute("workEndTime", dateStr);
		return new ModelAndView("report/attendance", model);
	}

	/**
	 * スタッフ出勤一覧画面を表示する。
	 *
	 * @param model
	 *            モデルマップ
	 * @return 結果
	 */
	@RequestMapping(value = "/cost", method = RequestMethod.GET)
	public ModelAndView attendanceList(ModelMap model) {
		return new ModelAndView("report/cost", model);
	}

	/**
	 * 業務時間統計出力画面を表示する。
	 *
	 * @param workTaskDto
	 * @param model
	 *            モデルマップ
	 * @return 結果
	 */
	@RequestMapping(value = "/business/list", method = RequestMethod.GET)
	public ModelAndView list(@ModelAttribute("workTaskDto") WorkTaskDto workTaskDto, ModelMap model) {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		model.addAttribute("workStartTimeStr", df.format(new Date()));
		model.addAttribute("workEndTimeStr", df.format(new Date()));

		return new ModelAndView("report/businessHour");
	}

	/**
	 * 従業時間統計出力画面を表示する。
	 *
	 * @param workTaskDto
	 *            モデルマップ
	 * @return 結果
	 */
	@RequestMapping(value = "/work/list", method = RequestMethod.GET)
	public ModelAndView workList(@ModelAttribute("workTaskDto") WorkTaskDto workTaskDto) {
		return new ModelAndView("admin/report/work/list");
	}

	@RequestMapping(value = "/attendance/export", method = RequestMethod.GET)
	@ResponseBody
	public void exportAttendance(HttpServletRequest request, HttpServletResponse response, WorkAttendanceDto dto)
			throws Exception {

		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat dfForSheet = new SimpleDateFormat("MMdd");
		Map<String, Object> model = new HashMap<String, Object>();
		List<Map<String, Object>> days = new ArrayList<Map<String, Object>>();
		List<String> sheetNames = new ArrayList<String>();


		DateTime workEndDate = dto.getWorkEndTime();
		for(; !dto.getWorkStartTime().isAfter(workEndDate); dto.setWorkStartTime(dto.getWorkStartTime().withFieldAdded(DurationFieldType.days(), 1))) {
			Map<String, Object> perDay = new HashMap<String, Object>();
			String workDate = df.format(dto.getWorkStartTime().toDate());
			String workDateForSheet = dfForSheet.format(dto.getWorkStartTime().toDate());
			List<WorkAttendanceReportDto> list = workAttendanceService.selectAttendanceByExample(dto);
			perDay.put("attendanceList", list);
			perDay.put("workDate", workDate);
			days.add(perDay);
			sheetNames.add(new String(workDateForSheet + " 勤怠表"));
		}
		model.put("days", days);
		model.put("sheetNames", sheetNames);

		ReportUtil.outputExcel(ac, ATTENDANCE_TEMPLATE, response, "出勤一覧.xlsx", model);
	}

	@RequestMapping(value = "/cost/export", method = RequestMethod.GET)
	@ResponseBody
	public void exportCost(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("fromDate") DateTime fromDate, @ModelAttribute("toDate") DateTime toDate,
			@ModelAttribute("staffWorkType") String staffWorkType)
			throws Exception {

		if(fromDate == null) {
			throw new UncheckedLogicException("javax.validation.constraints.NotNull.message.hasArg", "開始日");

		}
		if(toDate == null) {
			throw new UncheckedLogicException("javax.validation.constraints.NotNull.message.hasArg", "終了日");
		}

		DateTime dt = fromDate.withFieldAdded(DurationFieldType.days(), 31);
		if(dt.isBefore(toDate)) {
			throw new UncheckedLogicException("System.error.work_date_from_to_over_limit");
		}

		List<WorkCostReportDto> list = workAttendanceService.selectCost(fromDate, toDate.withFieldAdded(DurationFieldType.days(), 1), staffWorkType);

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("attendanceList", list);
		model.put("year", fromDate.get(DateTimeFieldType.year()));
		ReportUtil.outputExcel(ac, COST_TEMPLATE, response, "コスト一覧.xlsx", model);
	}

	@RequestMapping(value = "/business/export", method = RequestMethod.GET)
	@ResponseBody
	public void exportBusinessHour(HttpServletRequest request, HttpServletResponse response, WorkTaskDto dto)
			throws Exception {
		SimpleDateFormat dfForMonthDay = new SimpleDateFormat("MMdd");
		SimpleDateFormat dfForYear = new SimpleDateFormat("yyyy");
		String fileName;
		if(dto.getWorkStartTime().isEqual(dto.getWorkEndTime())){
			fileName = new String(dfForMonthDay.format(dto.getWorkStartTime().toDate())) + "_WorkHour_" +
					new String(dfForYear.format(dto.getWorkStartTime().toDate())) + ".xlsx";
		}
		else{
			fileName = new String(dfForMonthDay.format(dto.getWorkStartTime().toDate())) + "-" +
					new String(dfForMonthDay.format(dto.getWorkEndTime().toDate())) + "_WorkHour_" +
					new String(dfForYear.format(dto.getWorkStartTime().toDate())) + ".xlsx";
		}
		reportService.outputBusinessDayHourFile(dto, response, fileName);
	}

	@RequestMapping(value = "/staff/export", method = RequestMethod.GET)
	@ResponseBody
	public void exportBusinessHour(HttpServletRequest request, HttpServletResponse response, StaffDto staffDto)
			throws Exception {
		List<StaffDto> dataList = staffService.selectAllByExample(staffDto);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("staffList", dataList);
		ReportUtil.outputExcel(ac, STAFF_TEMPLATE, response, "スタッフ一覧.xlsx", values);

	}
}
