package com.lf.bigdata.controller;

import com.lf.bigdata.dto.WorkPlanDto;
import com.lf.bigdata.service.WorkPlanService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 作業予定情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/workPlan")
@Controller
public class WorkPlanController {
    @Autowired
    WorkPlanService workPlanService;

    /**
     * 作業予定を新規追加する。
     * @param workPlanDto 作業予定
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(WorkPlanDto workPlanDto, ModelMap model) {
        return new ModelAndView("workPlan/insert", model);
    }

    /**
     * 作業予定を新規追加(確認)する。
     * @param workPlanDto 作業予定
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid WorkPlanDto workPlanDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/workPlan/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("workPlan/insertConfirm", model);
    }

    /**
     * 作業予定を新規追加(終了)する。
     * @param workPlanDto 作業予定
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid WorkPlanDto workPlanDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/workPlan/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //作業予定を新規追加する。
        int ret = workPlanService.insertSelective(workPlanDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/workPlan/insertFinish", model);
    }

    /**
     * 作業予定を編集する。
     * @param workPlanId 作業予定ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{workPlanId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer workPlanId, ModelMap model) {
        //プライマリーキーで作業予定を検索する。
        WorkPlanDto workPlanDto = workPlanService.selectByPrimaryKey(workPlanId);
        workPlanDto.setWorkPlanCompany(workPlanDto.getWorkPlanCompanyId());
        model.addAttribute("workPlanDto", workPlanDto);
        return new ModelAndView("workPlan/edit", model);
    }

    /**
     * 作業予定を編集(確認)する。
     * @param workPlanDto 作業予定
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid WorkPlanDto workPlanDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/workPlan/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("workPlan/editConfirm", model);
    }

    /**
     * 作業予定を編集(終了)する。
     * @param workPlanDto 作業予定
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid WorkPlanDto workPlanDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/workPlan/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで作業予定を更新する。
        int ret = workPlanService.updateByPrimaryKeySelective(workPlanDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("workPlan/editFinish", model);
    }

    /**
     * 作業予定一覧画面を表示する。
     * @param workPlanDto 作業予定
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("workPlanDto") WorkPlanDto workPlanDto, ModelMap model) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = df.format(new Date());
        model.addAttribute("workPlanDate", dateStr);
        return new ModelAndView("workPlan/list", model);
    }
}
