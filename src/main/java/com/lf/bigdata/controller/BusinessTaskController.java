package com.lf.bigdata.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.lf.bigdata.dto.BusinessTaskDto;
import com.lf.bigdata.service.BusinessTaskService;

/**
 * 作業業務情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/businessTask")
@Controller
public class BusinessTaskController {
    @Autowired
    BusinessTaskService businessTaskService;

    /**
     * 作業業務を新規追加する。
     * @param businessTaskDto 作業業務
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(BusinessTaskDto businessTaskDto, ModelMap model)  {
        return new ModelAndView("businessTask/insert", model);
    }

    /**
     * 作業業務を新規追加(確認)する。
     * @param businessTaskDto 作業業務
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid BusinessTaskDto businessTaskDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("businessTask/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("businessTask/insertConfirm", model);
    }

    /**
     * 作業業務を新規追加(終了)する。
     * @param businessTaskDto 作業業務
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid BusinessTaskDto businessTaskDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("businessTask/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //作業業務を新規追加する。
        int ret = businessTaskService.insertSelective(businessTaskDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("businessTask/insertFinish", model);
    }

    /**
     * 作業業務を編集する。
     * @param businessId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{businessId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer businessId, ModelMap model) {
        //プライマリーキーで作業業務を検索する。
        BusinessTaskDto businessTaskDto = businessTaskService.selectByPrimaryKey(businessId);
        model.addAttribute("businessTaskDto", businessTaskDto);
        return new ModelAndView("businessTask/edit", model);
    }

    /**
     * 作業業務を編集(確認)する。
     * @param businessTaskDto 作業業務
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid BusinessTaskDto businessTaskDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("businessTask/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("businessTask/editConfirm", model);
    }

    /**
     * 作業業務を編集(終了)する。
     * @param businessTaskDto 作業業務
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid BusinessTaskDto businessTaskDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("businessTask/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで作業業務を更新する。
        int ret = businessTaskService.updateByPrimaryKeySelective(businessTaskDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("businessTask/editFinish", model);
    }

    /**
     * 作業業務一覧画面を表示する。
     * @param businessTaskDto 作業業務
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("businessTaskDto") BusinessTaskDto businessTaskDto, ModelMap model) {
        return new ModelAndView("businessTask/list", model);
    }
}