package com.lf.bigdata.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.lf.bigdata.dto.WorkTaskDto;
import com.lf.bigdata.service.WorkTaskService;

/**
 * スタッフ作業情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/workTask")
@Controller
public class WorkTaskController {
    @Autowired
    WorkTaskService workTaskService;

    /**
     * スタッフ作業を新規追加する。
     * @param workTaskDto スタッフ作業
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(WorkTaskDto workTaskDto, ModelMap model) {
        return new ModelAndView("workTask/insert", model);
    }

    /**
     * スタッフ作業を新規追加(確認)する。
     * @param workTaskDto スタッフ作業
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid WorkTaskDto workTaskDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("workTask/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("workTask/insertConfirm", model);
    }

    /**
     * スタッフ作業を新規追加(終了)する。
     * @param workTaskDto スタッフ作業
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid WorkTaskDto workTaskDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("workTask/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //スタッフ作業を新規追加する。
        int ret = workTaskService.insertSelective(workTaskDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("workTask/insertFinish", model);
    }

    /**
     * スタッフ作業を編集する。
     * @param workId ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{workId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer workId, ModelMap model) {
        //プライマリーキーでスタッフ作業を検索する。
        WorkTaskDto workTaskDto = workTaskService.selectAllByPrimaryKey(workId);

        workTaskDto.setStaffCode(workTaskDto.getStaff().getStaffCode());

        model.addAttribute("workTaskDto", workTaskDto);
        return new ModelAndView("workTask/edit", model);
    }

    /**
     * スタッフ作業を編集(確認)する。
     * @param workTaskDto スタッフ作業
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid WorkTaskDto workTaskDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("workTask/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("workTask/editConfirm", model);
    }

    /**
     * スタッフ作業を編集(終了)する。
     * @param workTaskDto スタッフ作業
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid WorkTaskDto workTaskDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("workTask/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでスタッフ作業を更新する。
        int ret = workTaskService.updateByPrimaryKeySelective(workTaskDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("workTask/editFinish", model);
    }

    /**
     * スタッフ作業一覧画面を表示する。
     * @param workTaskDto スタッフ作業
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("workTaskDto") WorkTaskDto workTaskDto, ModelMap model) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = df.format(new Date());
        model.addAttribute("workStartTime", dateStr);
        return new ModelAndView("workTask/list", model);
    }
}