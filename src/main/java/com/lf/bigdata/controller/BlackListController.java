package com.lf.bigdata.controller;

import com.lf.bigdata.dto.StaffDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * ブラックリスト情報削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/black")
@Controller
public class BlackListController {

    /**
     * スタッフ一覧画面を表示する。
     * @param staffDto スタッフ
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("staffDto") StaffDto staffDto, ModelMap model) {
        return new ModelAndView("black/list", model);
    }
}
