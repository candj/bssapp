package com.lf.bigdata.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * メニュー画面の遷移を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */

@RequestMapping(value = "")
@Controller
public class IndexController {

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView list() {
        return new ModelAndView("signin");
    }

	@RequestMapping(value = "/user", method = RequestMethod.GET)
    public ModelAndView startWork() {
        return new ModelAndView("startWork");
    }
	@RequestMapping(value = "/main", method = RequestMethod.GET)
    public ModelAndView main() {
        return new ModelAndView("main");
    }

}


