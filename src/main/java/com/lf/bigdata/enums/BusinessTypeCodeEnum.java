package com.lf.bigdata.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@Getter
public class BusinessTypeCodeEnum {

    // 業務区分
    // 出勤
    public static final String starting = "01";
    // 作業
    public static final String task_work = "02";
    // 退勤
    public static final String leaving = "03";
    // 休憩45分
    public static final String rest_45_min = "04";
    // 休憩15分
    public static final String rest_15_min = "05";
    // 休憩60分
    public static final String rest_60_min = "06";
    // 勤務業務区分
    public static final List<Object> attendance = Arrays.asList(starting, rest_60_min, rest_45_min, rest_15_min, leaving);

    // 休憩区分
    public static final List<Object> rest = Arrays.asList(rest_60_min, rest_45_min, rest_15_min);

    // 作業業務区分
    public static final List<Object> task = Arrays.asList(task_work);

}
