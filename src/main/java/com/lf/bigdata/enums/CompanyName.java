package com.lf.bigdata.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@AllArgsConstructor
@Getter
@Setter
public class CompanyName {

    private static final String COMPANY_LF = "LF";

    private static final String COMPANY_USN = "USN";

    private static final String COMPANY_TUB = "TUB";

    private static final String COMPANY_FNC = "FNC";

    private static final String COMPANY_BEW = "BEW";

    private static final String COMPANY_ENTRY = "Entry";

    private static final String COMPANY_AW = "AW";

    private static final String COMPANY_TQJ = "TQJ";

    public String getCompanyLf() {
        return COMPANY_LF;
    }

    public String getCompanyUsn() { return COMPANY_USN; }

    public String getCompanyTub() {
        return COMPANY_TUB;
    }

    public String getCompanyFnc() {
        return COMPANY_FNC;
    }

    public String getCompanyBew() {
        return COMPANY_BEW;
    }

    public String getCompanyEntry() { return COMPANY_ENTRY; }

    public String getCompanyAw() {
        return COMPANY_AW;
    }

    public String getCompanyTqj() {
        return COMPANY_TQJ;
    }

}
