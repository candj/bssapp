package com.lf.bigdata.utils;

public class Constants {
	// 出勤[08:00]
	public static final String WORK_CODE_START_8 = "work9001";
	// 出勤[09:00]
	public static final String WORK_CODE_START_9 = "work9002";
	// 出勤[13:00]
	public static final String WORK_CODE_START_13 = "work9003";
	// 出勤[14:00] 出勤[14:00]
	public static final String WORK_CODE_START_14 = "work9004";
	// 出勤[22:00]
	public static final String WORK_CODE_START_22 = "work9005";
	// 出勤[遅刻]
	public static final String WORK_CODE_START_LATER = "work9006";
	// 退勤[17:00]
	public static final String WORK_CODE_EDN_17 = "work9007";
	// 退勤[18:00]
	public static final String WORK_CODE_EDN_18 = "work9008";
	// 退勤[22:00]
	public static final String WORK_CODE_EDN_22 = "work9009";
	// 退勤[06:00]
	public static final String WORK_CODE_EDN_6 = "work9010";
	// 退勤[07:00]
	public static final String WORK_CODE_EDN_7 = "work9011";
	// 退勤[残業]
	public static final String WORK_CODE_EDN_LATER = "work9012";

	public static final String WORK_CODE_EDN_REST_60 = "work9013";
	// 昼休憩[60分]
	public static final String WORK_CODE_EDN_REST_45 = "work9014";
	// 昼休憩[45分]
	public static final String WORK_CODE_EDN_REST15 = "work9015";
	// トイレ休憩
	public static final String WORK_CODE_TOILET = "work9016";
	// 退勤[早退]
	public static final String WORK_CODE_EDN_EARLY = "work9017";
}
