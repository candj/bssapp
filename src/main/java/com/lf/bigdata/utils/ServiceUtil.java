package com.lf.bigdata.utils;

import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;
import org.joda.time.format.DateTimeFormat;

import com.lf.bigdata.enums.BusinessTypeCodeEnum;
import com.lf.bigdata.mapper.model.WorkDay;

public class ServiceUtil {


	private static final String DEFAULT_MINUTE_FORMAT = "H:mm";

	/**
	 * 作業日開始時間と終了時間を取得する。
	 * @param d 作業日
	 * @return 作業日開始時間と終了時間
	 */
	public static WorkDay getWorkDay(DateTime d) {

		//当日00:00～翌日の00:00
		WorkDay wd = new WorkDay();
		wd.setWorkStartTime(d);
		wd.setWorkEndTime(wd.getWorkStartTime().withFieldAdded(DurationFieldType.days(), 1));
		return wd;

	}

	/**
	 * 作業日開始時間と終了時間を取得する。
	 * @param sd 開始日
	 * @param ed 終了日
	 * @return 作業日開始時間と終了時間
	 */
	public static WorkDay getMultiWorkDay(DateTime sd, DateTime ed) {

		//開始日00:00～終了日の00:00
		WorkDay wd = new WorkDay();
		wd.setWorkStartTime(sd);
		wd.setWorkEndTime(ed.withFieldAdded(DurationFieldType.days(), 1));
		return wd;

	}

	/**
	 * 作業日開始時間と終了時間を取得する。
	 * @param d 作業日
	 * @return 作業日開始時間と終了時間
	 */
	public static WorkDay getWorkDayForReportService(DateTime d) {
		WorkDay wd = new WorkDay();
		//当日7:00～翌日の7:00
		wd.setWorkStartTime(d.withHourOfDay(7));
		wd.setWorkEndTime(wd.getWorkStartTime().withFieldAdded(DurationFieldType.days(), 1));
		return wd;

	}

	/**
	 *
	 * @param restTime 秒
	 * @return
	 */

	public static String formatRestTime(Long restTime) {
		if(restTime == null) {
			return "0:00";
		}
		DateTime dt = DateTime.now().withHourOfDay((int)(restTime / 60)).withMinuteOfHour((int)(restTime % 60));
		return dt.toString(DateTimeFormat.forPattern(DEFAULT_MINUTE_FORMAT));
	}

	public static boolean isStartingWork(String businessTypeId) {
		return BusinessTypeCodeEnum.starting.equals(businessTypeId);
	}

	public static boolean isRestWork(String businessTypeId) {
		return BusinessTypeCodeEnum.rest.contains(businessTypeId);
	}

	public static boolean isLeavingWork(String businessTypeId) {
		return BusinessTypeCodeEnum.leaving.equals(businessTypeId);
	}

	public static boolean isTaskWork(String businessTypeId) {
		return BusinessTypeCodeEnum.task_work.equals(businessTypeId);
	}
	
	
	public static DateTime resetStartEndTime(DateTime startDateTime) {
		//15分単位で切り捨て
		DateTime dt = startDateTime;
		if(startDateTime == null) {
			return null;
		}
		int minutes = dt.getMinuteOfHour();
		if(minutes % 15  != 0) {
			dt = dt.withFieldAdded(DurationFieldType.minutes(), 0 - minutes % 15);
		}
		return dt;
	}
}
