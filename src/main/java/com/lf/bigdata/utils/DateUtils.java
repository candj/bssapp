package com.lf.bigdata.utils;

import java.text.ParseException;

import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.lf.bigdata.dto.BusinessAttendanceDto;
import com.lf.bigdata.dto.Work;
import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;

public class DateUtils {

	public static DateTime getCurrentDateTime() throws ParseException {
//		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateTime date = DateTime.now();
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
		return fmt.parseDateTime(date.toString("yyyy-MM-dd HH:mm"));
	}

	public static DateTime getRestEndTime(DateTime time, String businessTypeId) {

		DateTime dt = new DateTime(time);

		if (BusinessTypeCodeEnum.rest_60_min.equals(businessTypeId)) {
			dt = dt.withFieldAdded(DurationFieldType.minutes(), 60);

		} else if (BusinessTypeCodeEnum.rest_45_min.equals(businessTypeId)) {
			dt = dt.withFieldAdded(DurationFieldType.minutes(), 45);

		} else if (BusinessTypeCodeEnum.rest_15_min.equals(businessTypeId)) {
			dt = dt.withFieldAdded(DurationFieldType.minutes(), 15);

		}
		return dt;
	}

	public static boolean isToiletWork(Work<?> work) {

		return Constants.WORK_CODE_TOILET.equals(work.getBusiness().getBusinessCode());

	}

	public static void resetWorkingHours(WorkAttendanceDto workAttendanceDto) throws ParseException {

		BusinessAttendanceDto businessAttendanceDto = workAttendanceDto.getBusinessAttendance();
		DateTime startTime = null;
		DateTime dt = businessAttendanceDto.getBusinessStartTime();
		if (dt != null) {
			startTime = dt;
			DateTime now = DateTime.now();
			startTime = startTime.withYear(now.getYear()).withMonthOfYear(now.getMonthOfYear())
					.withDayOfMonth(now.getDayOfMonth());
		} else {
			startTime = DateTime.now();
		}

		workAttendanceDto.setWorkStartTime(startTime);
	}

}
