package com.lf.bigdata.utils;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationFieldType;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;

import com.lf.bigdata.dto.WorkCostReportDto;

public class JxlsUtils {

	private static final String SHIFT_TYPE1 = "朝";
	private static final String SHIFT_TYPE2 = "昼";
	private static final String SHIFT_TYPE3 = "夜";
	private static final String BLACK_TRUE = "●";
	private static final String BLACK_FALSE = "";

	private Map<String, Integer> countMap = new HashMap<>();

	public Integer count(String var) {
		if (var == null)
			return null;
		if (countMap.containsKey(var)) {
			Integer t = countMap.get(var);
			t += 1;
			countMap.replace(var, t);
			return t;
		} else {
			countMap.put(var, 1);
		}
		return 1;
	}

	public static Double divide(Double val) {
		if (val == null) {
			return 0.0;
		}
		BigDecimal bg = new BigDecimal(val * 1.0 / 60);
		Double d = bg.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
		return d;
	}

	public Double divide(Double val, int div) {
		if (val == null) {
			return 0.0;
		}
		BigDecimal bg = new BigDecimal(val * 1.0 / div);
		Double d = bg.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
		return d;
	}

	public Date formatDate(DateTime source) {
		if (source == null) {
			return null;
		}
		return source.toLocalDate().toDate();
	}

	public LocalTime formatTime(DateTime source) {
		if (source == null) {
			return null;
		}
		return LocalTime.of(source.toLocalTime().get(DateTimeFieldType.hourOfDay())
				, source.toLocalTime().get(DateTimeFieldType.minuteOfHour()));
	}

	public Date formatDateTime(DateTime source) {
		if (source == null) {
			return null;
		}
		return source.toLocalDateTime().toDate();
	}

	public String formatDate(DateTime source, String format) {
		if (source == null) {
			return null;
		}
		return source.toString(DateTimeFormat.forPattern(format));
	}


	public String formatStartWorkTime(DateTime startWorkTime, String formatStr) {
		if (startWorkTime == null) {
			return "";
		}
		return startWorkTime.toString(DateTimeFormat.forPattern(formatStr));
	}

	public String formatEndWorkTime(DateTime startWorkTime, DateTime endWorkTime, String formatStr) {
		if (startWorkTime == null || endWorkTime == null) {
			return "";
		}
		if (endWorkTime.get(DateTimeFieldType.dayOfMonth()) != startWorkTime.get(DateTimeFieldType.dayOfMonth())) {
			String time = endWorkTime.toString(DateTimeFormat.forPattern(formatStr));
			String[] arr = time.split(":");
			return (Integer.parseInt(arr[0]) + 24) + ":" + arr[1];
		} else {
			return endWorkTime.toString(DateTimeFormat.forPattern(formatStr));
		}
	}

	public String getNightWorkTime(WorkCostReportDto dto) {

		DateTime startTime = dto.getWorkStartTime()
				.withMillisOfSecond(0);

		DateTime restStartTime = startTime.withHourOfDay(22).withMinuteOfHour(0).withSecondOfMinute(0)
				.withMillisOfSecond(0);
		if (startTime.isBefore(restStartTime)) {
			startTime = restStartTime;
		}

		DateTime endTime = dto.getWorkEndTime()
				.withMillisOfSecond(0);
		if(endTime == null) {
			return "";
		}


		if(restStartTime.withFieldAdded(DurationFieldType.hours(), 7).isBefore(endTime)) {
			endTime = endTime.withHourOfDay(5).withMinuteOfHour(0).withSecondOfMinute(0);
		}

		Period period = new Period(startTime, endTime);

//		if(period.getHours() > 6) {
//			period = period.withHours(7).withMinutes(0);
//		}
		if (dto.getNightRestTime() != null) {
			period = period.minusMinutes(dto.getNightRestTime().intValue()).normalizedStandard();
		}
		if(period.getHours() < 0 || period.getMinutes() < 0) {
			return "00:00";
		}
		return String.format("%0" + 2 + "d", period.getHours()) + ":"
				+ String.format("%0" + 2 + "d", period.getMinutes());

	}

	public static void main(String[] args) {
		DateTime start = new DateTime("2013-08-01T22:10:00.999");
		DateTime end = new DateTime("2013-08-02T09:50:00.000");
		// Periodで期間を定義
		JxlsUtils util = new JxlsUtils();
		WorkCostReportDto dto = new WorkCostReportDto();
		dto.setWorkStartTime(start);
		dto.setWorkEndTime(end);


		System.out.println(util.getNightWorkTime(dto));
	}

	public String getShiftType(DateTime startWorkTime) {

		DateTime dt1 = startWorkTime.withHourOfDay(12).withMinuteOfHour(0).withSecondOfMinute(0)
				.withMillisOfSecond(0);
		DateTime dt2 = startWorkTime.withHourOfDay(21).withMinuteOfHour(0).withSecondOfMinute(0)
				.withMillisOfSecond(0);

		if (startWorkTime.isBefore(dt1)) {
			return SHIFT_TYPE1;
		} else if (startWorkTime.isBefore(dt2)) {
			return SHIFT_TYPE2;
		} else {
			return SHIFT_TYPE3;
		}
	}

	public String getBlackStatus(Boolean blackStatus) {

		if (blackStatus.booleanValue() == true) {
			return BLACK_TRUE;
		} else {
			return BLACK_FALSE;
		}
	}

}
