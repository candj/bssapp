package com.lf.bigdata.strategy;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lf.bigdata.dto.WorkDto;
import com.lf.bigdata.dto.WorkTaskDto;
import com.lf.bigdata.validator.WorkValidator;

@Service
@Transactional
public class TaskWorkStrategy extends AbsSaveWorkTaskStrategy {

	@Override
	boolean isSameWork(WorkDto w, Integer workId) {
		return w.getWorkId().equals(workId) && !super.isAttendanceWork(w);
	}


	@Override
	boolean doValidate(WorkTaskDto input, OneDayWorks works) {
		return WorkValidator.canTaskWork(input, works);
	}
}
