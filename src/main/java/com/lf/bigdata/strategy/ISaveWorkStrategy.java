package com.lf.bigdata.strategy;

public interface ISaveWorkStrategy<T> {

	public int insert(T input) throws Exception;

	public int update(T input, Integer workId) throws Exception;

	public int remove(T input, Integer workId) throws Exception;
}
