package com.lf.bigdata.strategy;

import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.dto.WorkDto;
import com.lf.bigdata.mapper.model.WorkAttendance;

public abstract class AbsSaveWorkAttendanceStrategy extends AbsSaveWorkStrategy<WorkAttendanceDto> {

	/**
	 * 一日の作業データを取得する。
	 * @param dto
	 * @return
	 */
	protected OneDayWorks selectOneDayWorks(WorkAttendanceDto dto) {
		//新規の場合
		if(dto.getWorkId() == null) {
			return selectOneDayWorks(dto.getWorkStaffId(), dto.getWorkStartTime());
		}
		WorkAttendance workAttendance = workAttendanceDao.selectByPrimaryKey(dto.getWorkId());
		return selectOneDayWorks(dto.getWorkStaffId(), workAttendance.getWorkStartTime());
	}


	protected WorkDto mapper(WorkAttendanceDto dto) {
		WorkDto newDto = mapper.map(dto, WorkDto.class);
		newDto.setType(ATTENDANCE_FLAG);
		newDto.setBusinessCode(dto.getBusinessAttendance().getBusinessCode());
		newDto.setBusinessTypeId(dto.getBusinessAttendance().getBusinessTypeId());
		return newDto;
	}

	@Override
	protected void applyInputFieldForUpdate(WorkAttendanceDto srcDto, WorkDto targetDto) {
		targetDto.setWorkStartTime(srcDto.getWorkStartTime());
		targetDto.setRealWorkStartTime(srcDto.getRealWorkStartTime());
		targetDto.setWorkEndTime(srcDto.getWorkEndTime());
		targetDto.setWorkBusinessId(srcDto.getWorkBusinessId());
	}

}
