package com.lf.bigdata.strategy;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.PropertyUtils;
import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.model.WhereCondition.Criteria;
import com.github.dozermapper.core.Mapper;
import com.lf.bigdata.dao.mapper.WorkAttendanceDao;
import com.lf.bigdata.dao.mapper.WorkTaskDao;
import com.lf.bigdata.dto.WorkDto;
import com.lf.bigdata.mapper.model.WorkAttendance;
import com.lf.bigdata.mapper.model.WorkTask;
import com.lf.bigdata.service.WorkAttendanceService;
import com.lf.bigdata.utils.ServiceUtil;

@Transactional
public abstract class AbsSaveWorkStrategy<T> implements ISaveWorkStrategy<T> {

	public static final String ATTENDANCE_FLAG = "1";
	public static final String TASK_FLAG = "2";

	@Autowired
	protected WorkAttendanceService workAttendanceService;
	@Autowired
	protected WorkAttendanceDao workAttendanceDao;
	@Autowired
	protected WorkTaskDao workTaskDao;

	@Autowired
	protected Mapper mapper;

	Comparator<WorkDto> ascWorkStartTimeComp = new Comparator<WorkDto>() {
		@Override
		public int compare(WorkDto o1, WorkDto o2) {
			return o1.getWorkStartTime().compareTo(o2.getWorkStartTime());
		}
	};
	Comparator<WorkDto> descWorkStartTimeComp = new Comparator<WorkDto>() {
		@Override
		public int compare(WorkDto o1, WorkDto o2) {
			return o2.getWorkStartTime().compareTo(o1.getWorkStartTime());
		}
	};

	public int insert(T dto) throws Exception {
		OneDayWorks works = selectOneDayWorks(dto);
		WorkDto newDto = mapper(dto);
		works.getWorks().add(newDto);
		//昇順にソートする。
		works.getWorks().sort(ascWorkStartTimeComp);

		doValidate(dto, works);

		update(works);
		return 1;
	}

	@Override
	public int update(T dto, Integer workId) throws Exception {
		OneDayWorks oneDayWorks = selectOneDayWorks(dto);

		//画面入力された内容を反映
		oneDayWorks.getWorks().stream()
				.filter(w -> isSameWork(w, workId)).findFirst()
				.ifPresent(w -> {
					applyInputFieldForUpdate(dto, w);

				});
		//昇順にソートする。
		oneDayWorks.getWorks().sort(ascWorkStartTimeComp);

		oneDayWorks.getWorks().stream().filter(w -> w.getWorkId().equals(workId)).findFirst().ifPresent(w -> {
			if(oneDayWorks.getWorks().indexOf(w) == 1) {
				//最初の作業開始時間を出勤時刻+1秒s
				try {
					PropertyUtils.setProperty(dto, "workStartTime", oneDayWorks.getWorks().get(0).getWorkStartTime().withFieldAdded(DurationFieldType.seconds(), 1));
				} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}

		});
		doValidate(dto, oneDayWorks);
		update(oneDayWorks);
		return 1;
	}

	public int remove(T dto, Integer workId) throws Exception {
		OneDayWorks oneDayWorks = selectOneDayWorks(dto);
		oneDayWorks.getWorks().stream().filter(w -> this.isSameWork(w, workId)).findFirst().ifPresent(w -> {
			oneDayWorks.getWorks().remove(w);
			delete(w);
		});
		doValidate(dto, oneDayWorks);

		update(oneDayWorks);
		return 1;

	}


	/**
	 * 一日の作業データを取得する。
	 * @param dto
	 * @return
	 */
	protected OneDayWorks selectOneDayWorks(Integer staffId, DateTime workStartTime) {
		WorkAttendance record = new WorkAttendance();
		record.setWorkStaffId(staffId);
		record.setWorkStartTime(workStartTime);

		List<WorkDto> startLeaveWorks = workAttendanceDao.selectStartLeaveWorks(record);

		WhereCondition whereCondition = new WhereCondition();
		Criteria criteria = whereCondition.createCriteria()
				.andEqualTo("work_staff_id", staffId);
		if (startLeaveWorks.size() > 0) {
			criteria = criteria.andGreaterThanOrEqualTo("work_start_time", startLeaveWorks.get(0).getWorkStartTime());
			if(startLeaveWorks.get(0).getWorkEndTime() != null) {
				criteria = criteria.andLessThanOrEqualTo("work_start_time", startLeaveWorks.get(0).getWorkEndTime());
			} else {
				if (startLeaveWorks.size() > 1) {
					criteria = criteria.andLessThan("work_start_time", startLeaveWorks.get(1).getWorkStartTime());
				}
			}


		}

		return new OneDayWorks(workAttendanceDao.selectOneDayWorks(whereCondition), mapper);
	}

	protected void update(OneDayWorks oneDayWorks) {
		List<WorkDto> works = oneDayWorks.getWorks();
		works.sort(ascWorkStartTimeComp);

		//休憩作業の時間リセット
		//休憩終了時の作業を取得する。(無ければ、休憩前の作業により新しい作業データを新規追加する。)
		List<WorkDto> restWorks = works.stream()
				.filter(w -> ServiceUtil.isRestWork(w.getBusinessTypeId()))
				.collect(Collectors.toList());

		List<WorkDto> tastWorks = works.stream()
				.filter(w -> !isAttendanceWork(w))
				.collect(Collectors.toList());

		restWorks.forEach(rw -> {
			//休憩終了前作業記録を取得
			List<WorkDto> beforeWorks = tastWorks.stream()
					.filter(w -> w.getWorkStartTime().isBefore(rw.getWorkEndTime()))
					.collect(Collectors.toList());

			//休憩期間内作業記録
			List<WorkDto> restPoriodWorks = tastWorks.stream()
					.filter(w -> w.getWorkStartTime().isBefore(rw.getWorkEndTime())
							&& !w.getWorkStartTime().isBefore(rw.getWorkStartTime()))
					.collect(Collectors.toList());

			//休憩時間終了時間開始作業記録あれば
			WorkDto nextWork = tastWorks.stream()
					.filter(w -> w.getWorkStartTime().equals(rw.getWorkEndTime()))
					.findFirst().orElse(null);
			if (nextWork == null) {
				//新規の作業記録を作成
				if (beforeWorks.size() > 0) {
					WorkDto lastWork = beforeWorks.get(beforeWorks.size() - 1);
					WorkDto newWork = mapper.map(lastWork, WorkDto.class);
					newWork.setWorkStartTime(rw.getWorkEndTime());
					newWork.setWorkId(null);
					newWork.setType(TASK_FLAG);
					newWork.setWorkEndTime(null);
					works.add(newWork);
				}
			} else {

				if (restPoriodWorks.size() > 0) {
					nextWork.setWorkBusinessId(restPoriodWorks.get(restPoriodWorks.size() - 1).getWorkBusinessId());
				}

			}
			if(beforeWorks.size() > 0) {
				WorkDto lastWork = beforeWorks.get(beforeWorks.size() - 1);
				lastWork.setWorkEndTime(rw.getWorkStartTime());
			}


			//休憩中の作業を削除する。
			restPoriodWorks.forEach(w -> {
				delete(w);
				works.remove(w);
			});

		});


		//降順にソートする。
		works.sort(descWorkStartTimeComp);
		WorkDto nextWork = null;
		for(WorkDto work : works) {
			if(nextWork != null) {
				//休憩開始時の作業終了時間がすでに設定されている。
				//if(work.getWorkEndTime() == null) {
					work.setWorkEndTime(nextWork.getWorkStartTime());
				//}
			}
			nextWork = work;
		}
		works.sort(ascWorkStartTimeComp);
		if (works.size() > 1) {
			//最初の作業開始時間を出勤時刻+1秒s
			works.get(1)
					.setWorkStartTime(works.get(0).getWorkStartTime().withFieldAdded(DurationFieldType.seconds(), 1));
		}
		WorkDto leavingWork = oneDayWorks.getLeavingWork();
		if (leavingWork != null) {
			leavingWork.setWorkEndTime(null);
			oneDayWorks.getStartWork().setWorkEndTime(leavingWork.getWorkStartTime());
			oneDayWorks.getStartWork().setRealWorkEndTime(leavingWork.getRealWorkStartTime());

			//終了時間以降の作業データを削除する
			oneDayWorks.getWorks().stream().filter(w -> !w.getWorkStartTime().isBefore(leavingWork.getWorkStartTime()) && (w != leavingWork)).findFirst().ifPresent(w -> {
				works.remove(w);
				delete(w);
			});

		} else {
			if(oneDayWorks.getStartWork() != null) {
				oneDayWorks.getStartWork().setWorkEndTime(null);
				oneDayWorks.getStartWork().setRealWorkEndTime(null);
			}

		}
		works.forEach(w -> {

			if (w.getWorkId() == null) {
				insert(w);
			} else {
				update(w);
			}
		});
	}

	protected void update(WorkDto dto) {
		if (Objects.equals(dto.getOriWorkDto().getWorkStartTime(), dto.getWorkStartTime())
				&& Objects.equals(dto.getOriWorkDto().getWorkEndTime(), dto.getWorkEndTime())
				&& Objects.equals(dto.getOriWorkDto().getRealWorkStartTime(), dto.getRealWorkStartTime())
				&& Objects.equals(dto.getOriWorkDto().getRealWorkEndTime(), dto.getRealWorkEndTime())
				&& Objects.equals(dto.getOriWorkDto().getWorkBusinessId(), dto.getWorkBusinessId())

		) {

		} else {

			if (isAttendanceWork(dto)) {
				workAttendanceDao.updateByPrimaryKeySelective(mapper.map(dto, WorkAttendance.class));

			} else {
				workTaskDao.updateByPrimaryKeySelective(mapper.map(dto, WorkTask.class));
			}
		}

	}

	protected boolean isAttendanceWork(WorkDto dto) {
		return ATTENDANCE_FLAG.equals(dto.getType());
	}


	protected void insert(WorkDto dto) {
		if(isAttendanceWork(dto)) {
			workAttendanceDao.insertSelective(mapper.map(dto, WorkAttendance.class));
		} else {
			workTaskDao.insertSelective(mapper.map(dto, WorkTask.class));
		}

	}
	protected void delete(WorkDto dto) {
		if(isAttendanceWork(dto)) {
			workAttendanceDao.deleteByPrimaryKey(dto.getWorkId());
		} else {
			workTaskDao.deleteByPrimaryKey(dto.getWorkId());
		}
	}

	/**
	 * 一日の作業データを取得する。
	 * @param dto
	 * @return
	 */
	abstract protected OneDayWorks selectOneDayWorks(T dto);


	abstract protected WorkDto mapper(T dto);
	abstract boolean doValidate(T input, OneDayWorks works);
	abstract protected void applyInputFieldForUpdate(T srcDto, WorkDto targetDto);

	abstract boolean isSameWork(WorkDto work, Integer workId);

}
