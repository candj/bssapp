package com.lf.bigdata.strategy;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import com.github.dozermapper.core.Mapper;
import com.lf.bigdata.dto.WorkDto;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;


public class OneDayWorks {

	private WorkDto lastWork;

	private WorkDto nextWork;

	private WorkDto startingWork;
	
	private WorkDto leavingWork;

	private List<WorkDto> works;

	public OneDayWorks(List<WorkDto> works, Mapper mapper) {

		List<WorkDto> newWorks = new ArrayList<WorkDto>();
		works.forEach(w -> {
			boolean isExist = false;
			for(WorkDto d : newWorks) {
				if(d.getWorkId().equals(w.getWorkId()) && d.getType().equals(w.getType())) {
					isExist = true;
				}
			}
			if(!isExist) {
				newWorks.add(w);
			}

		});
		this.works = newWorks;

		this.works.forEach(w -> {
			w.setOriWorkDto(mapper.map(w, WorkDto.class));
		});

	}
	
	public WorkDto getLastWork(DateTime dt) {
		this.works.stream().filter(w -> w.getWorkStartTime().isBefore(dt)).forEach(w -> {
			lastWork = w;
        });
		return lastWork;
	}
	
	public WorkDto getNextWork(DateTime dt) {
		this.works.stream().filter(w -> !w.getWorkStartTime().isBefore(dt)).findFirst().ifPresent(w -> {
        	nextWork = w;
        });
		return nextWork;
	}
	
	public WorkDto getStartWork() {
		this.works.stream().filter(w -> BusinessTypeCodeEnum.starting.equals(w.getBusinessTypeId())).findFirst().ifPresent(w -> {
        	startingWork = w;
        });
		return startingWork;
	}

	public WorkDto getLeavingWork() {
		this.works.stream().filter(w -> BusinessTypeCodeEnum.leaving.equals(w.getBusinessTypeId())).findFirst().ifPresent(w -> {
			leavingWork = w;
        });
		return leavingWork;
	}
	
	
	public List<WorkDto> getWorks() {
		return this.works;
	}

}
