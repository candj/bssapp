package com.lf.bigdata.strategy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.lf.bigdata.dao.mapper.WorkAttendanceDao;
import com.lf.bigdata.dao.mapper.WorkTaskDao;
import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.dto.WorkDto;
import com.lf.bigdata.service.WorkAttendanceService;
import com.lf.bigdata.service.WorkTaskService;
import com.lf.bigdata.utils.ServiceUtil;
import com.lf.bigdata.validator.WorkValidator;

@Service
@Transactional
public class StartingWorkStrategy extends AbsSaveWorkAttendanceStrategy {

	@Autowired
	WorkAttendanceService workAttendanceService;

	@Autowired
	WorkAttendanceDao workAttendanceDao;

	@Autowired
	WorkTaskDao workTaskDao;

	@Autowired
	WorkTaskService workTaskService;

	@Override
	public int insert(WorkAttendanceDto dto) throws Exception {
		dto.setWorkStartTime(ServiceUtil.resetStartEndTime(dto.getWorkStartTime()));
		OneDayWorks works = selectOneDayWorks(dto.getWorkStaffId(), dto.getWorkStartTime());

		WorkValidator.isRepeatWork(works.getWorks());

		works.getWorks().stream().filter(w -> w.getWorkStartTime().equals(dto.getWorkStartTime())).findFirst().ifPresent(w -> {
			throw new UncheckedLogicException("errors.error.business_code_same_time");
		});

		WorkDto lastWork = works.getLastWork(dto.getWorkStartTime());
		if (lastWork != null && !ServiceUtil.isLeavingWork(lastWork.getBusinessTypeId())) {
			throw new UncheckedLogicException("errors.error.business_code_not_leaving");
		}

		//dto.setWorkEndTime(dto.getWorkStartTime().withFieldAdded(DurationFieldType.minutes(), 1));
		return workAttendanceService.insertSelective(dto);
	}

	@Override
	public int update(WorkAttendanceDto dto, Integer workId) throws Exception {
		dto.setWorkStartTime(ServiceUtil.resetStartEndTime(dto.getWorkStartTime()));
		return super.update(dto, workId);
	}

//	@Override
//	public int remove(WorkAttendanceDto input, Integer workId) throws Exception {
//		return workAttendanceService.deleteByPrimaryKey(workId);
//	}

	@Override
	boolean doValidate(WorkAttendanceDto dto, OneDayWorks works) {
		return WorkValidator.canUpdateStartWork(dto, works);
	}

	@Override
	boolean isSameWork(WorkDto w, Integer workId) {
		return w.getWorkId().equals(workId) && ServiceUtil.isStartingWork(w.getBusinessTypeId());
	}


//	private void resetStartTime(WorkAttendanceDto dto) {
//		//15分単位で切り捨て
//		DateTime dt = dto.getWorkStartTime();
//		int minutes = dt.getMinuteOfHour();
//		if(minutes % 15  != 0) {
//			dt = dt.withFieldAdded(DurationFieldType.minutes(), 0 - minutes % 15);
//			dto.setWorkStartTime(dt);
//		}
//	}


}
