package com.lf.bigdata.strategy;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.dto.WorkDto;
import com.lf.bigdata.utils.ServiceUtil;
import com.lf.bigdata.validator.WorkValidator;

@Service
@Transactional
public class LeavingWorkStrategy extends AbsSaveWorkAttendanceStrategy {

//	@Override
//	public int remove(WorkAttendanceDto dto, Integer workId) throws Exception {
//		OneDayWorks oneDayWorks = selectOneDayWorks(dto);
//		WorkAttendance record = workAttendanceDao.selectByPrimaryKey(oneDayWorks.getStartWork().getWorkId());
//		record.setWorkEndTime(null);
//		workAttendanceDao.updateByPrimaryKey(record);
//		return workAttendanceDao.deleteByPrimaryKey(workId);
//	}

	@Override
	public int insert(WorkAttendanceDto dto) throws Exception {
		dto.setWorkStartTime(ServiceUtil.resetStartEndTime(dto.getWorkStartTime()));
		return super.insert(dto);
	}
	@Override
	public int update(WorkAttendanceDto dto, Integer workId) throws Exception {
		dto.setWorkStartTime(ServiceUtil.resetStartEndTime(dto.getWorkStartTime()));
		return super.update(dto, workId);
	}
	@Override
	boolean doValidate(WorkAttendanceDto input, OneDayWorks works) {
		return WorkValidator.canLeavingWork(input, works);
	}

	@Override
	boolean isSameWork(WorkDto w, Integer workId) {
		return w.getWorkId().equals(workId) && ServiceUtil.isLeavingWork(w.getBusinessTypeId());
	}

}
