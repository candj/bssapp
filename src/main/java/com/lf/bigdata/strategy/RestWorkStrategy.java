package com.lf.bigdata.strategy;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.dto.WorkDto;
import com.lf.bigdata.utils.ServiceUtil;
import com.lf.bigdata.validator.WorkValidator;

@Service
@Transactional
public class RestWorkStrategy extends AbsSaveWorkAttendanceStrategy {

//	@Override
//	public int remove(WorkAttendanceDto input, Integer workId) throws Exception {
//		return workAttendanceDao.deleteByPrimaryKey(workId);
//	}

	@Override
	boolean doValidate(WorkAttendanceDto input, OneDayWorks works) {
		return WorkValidator.canRest(input, works);
	}

	@Override
	boolean isSameWork(WorkDto w, Integer workId) {
		return w.getWorkId().equals(workId) && ServiceUtil.isRestWork(w.getBusinessTypeId());
	}

}
