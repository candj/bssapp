package com.lf.bigdata.strategy;

import com.lf.bigdata.dto.WorkDto;
import com.lf.bigdata.dto.WorkTaskDto;
import com.lf.bigdata.mapper.model.WorkTask;

public abstract class AbsSaveWorkTaskStrategy extends AbsSaveWorkStrategy<WorkTaskDto> {

	/**
	 * 一日の作業データを取得する。
	 * @param dto
	 * @return
	 */
	@Override
	protected OneDayWorks selectOneDayWorks(WorkTaskDto dto) {
		//新規の場合
		if(dto.getWorkId() == null) {
			return selectOneDayWorks(dto.getWorkStaffId(), dto.getWorkStartTime());
		}
		WorkTask workTask = workTaskDao.selectByPrimaryKey(dto.getWorkId());
		return selectOneDayWorks(dto.getWorkStaffId(), workTask.getWorkStartTime());
	}

	protected WorkDto mapper(WorkTaskDto dto) {
		WorkDto newDto = mapper.map(dto, WorkDto.class);
		newDto.setType(TASK_FLAG);
		newDto.setBusinessCode(dto.getBusinessTask().getBusinessCode());
		newDto.setBusinessTypeId(dto.getBusinessTask().getBusinessTypeId());
		return newDto;
	}

	@Override
	protected void applyInputFieldForUpdate(WorkTaskDto srcDto, WorkDto targetDto) {
		targetDto.setWorkStartTime(srcDto.getWorkStartTime());
		targetDto.setWorkBusinessId(srcDto.getWorkBusinessId());
	}

}
