package com.lf.bigdata.strategy;

import com.lf.bigdata.dto.WorkAttendanceDto;
import com.lf.bigdata.enums.BusinessTypeCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WorkStrategyFactory {

	@Autowired
	private StartingWorkStrategy startingWorkStrategy;

	@Autowired
	private RestWorkStrategy restWorkStrategy;

	@Autowired
	private TaskWorkStrategy taskWorkStrategy;

	@Autowired
	private LeavingWorkStrategy leavingWorkStrategy;

	@SuppressWarnings("unchecked")
	public <T> ISaveWorkStrategy<T> getWorkStrategy(T input) {
		if(input instanceof WorkAttendanceDto) {
			WorkAttendanceDto dto = WorkAttendanceDto.class.cast(input);
			if(BusinessTypeCodeEnum.starting.contains(dto.getBusinessAttendance().getBusinessTypeId())) {
				return (ISaveWorkStrategy<T>)startingWorkStrategy;
			} else if(BusinessTypeCodeEnum.leaving.contains(dto.getBusinessAttendance().getBusinessTypeId())) {
				return (ISaveWorkStrategy<T>)leavingWorkStrategy;
			} else {
				return (ISaveWorkStrategy<T>)restWorkStrategy;
			}


		} else {
			return (ISaveWorkStrategy<T>)taskWorkStrategy;
		}

	}


}
