/*
 Navicat MySQL Data Transfer

 Source Server         : cfengsx
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : bssapp

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 11/06/2021 10:53:46
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `notification`;
DROP TABLE IF EXISTS `process`;
DROP TABLE IF EXISTS `work_plan`;
DROP TABLE IF EXISTS `work_task`;
DROP TABLE IF EXISTS `work_attendance`;
DROP TABLE IF EXISTS `business_task`;
DROP TABLE IF EXISTS `business_attendance`;
DROP TABLE IF EXISTS `staff`;
DROP TABLE IF EXISTS `company`;
DROP TABLE IF EXISTS `user_role`;
DROP TABLE IF EXISTS `role`;
DROP TABLE IF EXISTS `app_user`;

-- ----------------------------
-- Table structure for app_user
-- ----------------------------

CREATE TABLE `app_user`  (
  `user_id` int NOT NULL AUTO_INCREMENT COMMENT 'ユーザーID',
  `user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ユーザー名',
  `user_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ユーザーコード',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'パスワード',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ステータス',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'ユーザー' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of app_user
-- ----------------------------

INSERT INTO `app_user` VALUES (1, 'admin', 'admin', 'admin', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for role
-- ----------------------------

CREATE TABLE `role`  (
  `role_id` int NOT NULL AUTO_INCREMENT COMMENT 'ロールID',
  `role_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ロールコード',
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ロール名',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ステータス',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'ロール' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role
-- ----------------------------

INSERT INTO `role` VALUES (1, 'admin', 'admin', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for user_role
-- ----------------------------

CREATE TABLE `user_role`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int NOT NULL COMMENT 'ユーザーID',
  `role_id` int NOT NULL COMMENT 'ロールID',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ステータス',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  CONSTRAINT `FK_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'ユーザールール' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_role
-- ----------------------------

INSERT INTO `user_role` VALUES (1, '1', '1', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company`  (
  `company_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `company_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '会社コード',
  `company_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '会社名',
  `company_adress` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '住所',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '郵便番号',
  `telephone_number` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '電話番号',
  `salary` int NOT NULL COMMENT '単金',
  `leader_salary` int NOT NULL COMMENT 'リーダー単金',
  `sub_leader_salary` int NOT NULL COMMENT 'サブリーダー単金',
  `trainer_salary` int NOT NULL COMMENT 'トレーナー単金',
  `fork_salary` int NOT NULL COMMENT 'フォーク単金',
  `commuting_cost` int NOT NULL COMMENT '交通費',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ステータス',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`company_id`) USING BTREE,
  UNIQUE INDEX `company_id`(`company_id`) USING BTREE,
  UNIQUE INDEX `company_code`(`company_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会社' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO `company` VALUES (1, 'LF', 'LF', NULL, NULL, NULL, 1000, 100, 100, 100, 100, 1000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `company` VALUES (2, 'USN', 'USN', NULL, NULL, NULL, 1000, 100, 100, 100, 100, 1000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `company` VALUES (3, 'TUB', 'TUB', NULL, NULL, NULL, 1000, 100, 100, 100, 100, 1000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `company` VALUES (4, 'FNC', 'FNC', NULL, NULL, NULL, 1000, 100, 100, 100, 100, 1000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `company` VALUES (5, 'BEW', 'BEW', NULL, NULL, NULL, 1000, 100, 100, 100, 100, 1000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `company` VALUES (6, 'Entry', 'Entry', NULL, NULL, NULL, 1000, 100, 100, 100, 100, 1000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `company` VALUES (7, 'AW', 'AW', NULL, NULL, NULL, 1000, 100, 100, 100, 100, 1000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `company` VALUES (8, 'TQJ', 'TQJ', NULL, NULL, NULL, 1000, 100, 100, 100, 100, 1000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `company` VALUES (9, 'HX', 'HX', NULL, NULL, NULL, 1000, 100, 100, 100, 100, 1000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `company` VALUES (10, 'KWH', 'KWH', NULL, NULL, NULL, 1000, 100, 100, 100, 100, 1000, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `company` VALUES (11, 'XHZ', 'XHZ', NULL, NULL, NULL, 1000, 100, 100, 100, 100, 1000, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for staff
-- ----------------------------

CREATE TABLE `staff`  (
  `staff_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `staff_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'スタッフID',
  `staff_encrypt_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'バーコード',
  `staff_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'スタッフ名前',
  `staff_work_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '区別',
  `staff_company_id` int NOT NULL COMMENT '会社',
  `worker_number` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '派遣会社作業員番号',
  `bussiness_grade` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務等級',
  `remark` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '説明',
	`black_status` tinyint(1) NOT NULL COMMENT 'ブラックステータス',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ステータス',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`staff_id`) USING BTREE,
  UNIQUE INDEX `staff_id`(`staff_code`) USING BTREE,
  INDEX `FK_COMPANY_ID`(`staff_company_id`) USING BTREE,
  CONSTRAINT `FK_COMPANY_ID` FOREIGN KEY (`staff_company_id`) REFERENCES `company` (`company_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6273 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'スタッフ' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for business_attendance
-- ----------------------------

CREATE TABLE `business_attendance`  (
  `business_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `business_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務名',
  `business_name_cn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務名「中国語」',
  `business_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務コード',
  `business_encrypt_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '暗証化コード',
  `business_start_time` datetime(0) NULL DEFAULT NULL COMMENT '業務開始時間',
  `limit_end_time` datetime(0) NULL DEFAULT NULL COMMENT '最大退勤時間（出勤だけ）',
  `limit_end_time_day` tinyint(1) NULL DEFAULT NULL COMMENT 'FALSE:今日　TRUE:明日（出勤だけ）',
  `business_type_id` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務区分ID',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ステータス',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`business_id`) USING BTREE,
  INDEX `business_id`(`business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 293 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '勤怠業務' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for business_task
-- ----------------------------

CREATE TABLE `business_task`  (
  `business_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `business_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務名',
  `business_name_cn` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務名「中国語」',
  `business_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務コード',
  `business_encrypt_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '暗証化コード',
  `area` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ブランド',
  `process` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務区別',
  `mark` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'マーク',
  `charge_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務モード',
  `business_type_id` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務区分ID',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ステータス',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`business_id`) USING BTREE,
  INDEX `business_id`(`business_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 300 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '作業業務' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for work_attendance
-- ----------------------------

CREATE TABLE `work_attendance`  (
  `work_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `work_staff_id` int NOT NULL COMMENT 'スタッフID',
	`work_company_id` int NULL DEFAULT NULL COMMENT '会社ID',
  `work_business_id` int NOT NULL COMMENT '業務ID',
  `work_start_time` datetime(0) NOT NULL COMMENT '出勤時間',
  `real_work_start_time` datetime(0) NULL DEFAULT NULL COMMENT '実際出勤時間',
  `work_end_time` datetime(0) NULL DEFAULT NULL COMMENT '退勤時間',
  `real_work_end_time` datetime(0) NULL DEFAULT NULL COMMENT '実際退勤時間',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ステータス',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`work_id`) USING BTREE,
  INDEX `staff_id`(`work_staff_id`) USING BTREE,
  INDEX `business_id`(`work_business_id`) USING BTREE,
  INDEX `work_date`(`work_start_time`) USING BTREE,
  CONSTRAINT `work_attendance_ibfk_1` FOREIGN KEY (`work_business_id`) REFERENCES `business_attendance` (`business_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `work_attendance_ibfk_2` FOREIGN KEY (`work_staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 526644 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'スタッフ出勤' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for work_task
-- ----------------------------

CREATE TABLE `work_task`  (
  `work_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `work_staff_id` int NOT NULL COMMENT 'スタッフID',
	`work_company_id` int NULL DEFAULT NULL COMMENT '会社ID',
  `work_business_id` int NOT NULL COMMENT '業務ID',
  `work_start_time` datetime(0) NOT NULL COMMENT '業務開始時間',
  `work_end_time` datetime(0) NULL DEFAULT NULL COMMENT '業務完成時間',
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ステータス',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`work_id`) USING BTREE,
  INDEX `staff_id`(`work_staff_id`) USING BTREE,
  INDEX `business_id`(`work_business_id`) USING BTREE,
  INDEX `work_date`(`work_start_time`) USING BTREE,
  CONSTRAINT `FK_BUSINESS_ID` FOREIGN KEY (`work_business_id`) REFERENCES `business_task` (`business_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_STAFF_ID` FOREIGN KEY (`work_staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 526683 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'スタッフ作業' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for work_plan
-- ----------------------------

CREATE TABLE `work_plan`  (
  `work_plan_id` int NOT NULL AUTO_INCREMENT COMMENT '作業予定ID',
  `work_plan_company_id` int NULL DEFAULT NULL COMMENT '会社ID',
  `work_plan_staff_id` int NOT NULL COMMENT 'スタッフID',
  `work_plan_date` datetime(0) NOT NULL COMMENT '日付',
  `work_plan_number` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '社員番号',
  `work_plan_level` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'クラス',
  `work_plan_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '氏名',
  `work_plan_name_kana` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'フリガナ（全角カナ）',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`work_plan_id`) USING BTREE,
  INDEX `leader_bonus_ibfk_1`(`work_plan_company_id`) USING BTREE,
  INDEX `leader_bonus_ibfk_2`(`work_plan_staff_id`) USING BTREE,
  CONSTRAINT `work_plan_ibfk_1` FOREIGN KEY (`work_plan_company_id`) REFERENCES `company` (`company_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `work_plan_ibfk_2` FOREIGN KEY (`work_plan_staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '作業予定' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for process
-- ----------------------------

CREATE TABLE `process`  (
  `process_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `process_code` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務区別コード',
  `process_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '業務区別名',
  `process_status` tinyint(1) NULL DEFAULT NULL COMMENT '掲示板表示状態',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`process_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '業務区別' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for notification
-- ----------------------------

CREATE TABLE `notification`  (
  `notification_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `notification_title` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'タイトル',
  `notification_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `notification_process_id` int NULL DEFAULT NULL COMMENT '作業区分ID',
  `notification_status` tinyint(1) NOT NULL COMMENT '編集可能状態',
  `notification_open_time` datetime(0) NULL DEFAULT NULL COMMENT '最新開放時間',
  `create_user` int NULL DEFAULT NULL COMMENT '登録ユーザー',
  `update_user` int NULL DEFAULT NULL COMMENT '更新ユーザー',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登録日付',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新日付',
  PRIMARY KEY (`notification_id`) USING BTREE,
  INDEX `notification_ibfk_1`(`notification_process_id`) USING BTREE,
  CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`notification_process_id`) REFERENCES `process` (`process_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 248 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '掲示板情報' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- View structure for start_work
-- ----------------------------
DROP VIEW IF EXISTS `start_work`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `start_work` AS select `work_attendance`.`work_id` AS `work_id`,`work_attendance`.`work_staff_id` AS `work_staff_id`,`work_attendance`.`work_business_id` AS `work_business_id`,`work_attendance`.`work_start_time` AS `work_start_time`,`work_attendance`.`real_work_start_time` AS `real_work_start_time`,`work_attendance`.`work_end_time` AS `work_end_time`,`work_attendance`.`real_work_end_time` AS `real_work_end_time` from (`work_attendance` join `business_attendance` on(((`work_attendance`.`work_business_id` = `business_attendance`.`business_id`) and (`business_attendance`.`business_type_id` = '01'))));

SET FOREIGN_KEY_CHECKS = 1;
