
function initGridWithoutLoadinData(grid_id, grid_colNames, grid_colModel,grid_params, grid_complete) {
	inner_initGrid(false, grid_id, grid_colNames, grid_colModel,grid_params, grid_complete);
}
function initGrid(grid_id, grid_colNames, grid_colModel,grid_params, grid_complete) {
	inner_initGrid(true, grid_id, grid_colNames, grid_colModel,grid_params, grid_complete);
}

function inner_initGrid(init_with_loading, grid_id, grid_colNames, grid_colModel,grid_params, grid_complete) {
  var jGridId = '#' + grid_id
  var jGridPagerId = '#' + grid_id + '_pager'
  var basepath = top.$("meta[name='basepath']").attr('content') || '/'
  var jGrid = $(jGridId)
  var grid_height = $(document.body).height() - 240;




  console.log("grid_height : " + grid_height);
  grid_height = 350;



  eval(`var opts=${jGrid.data('opt')}`)
  if (!opts) {
    opts = { loadUrl: '', addUrl: '', editUrl: '', deleteUrl: '' }
  };

  grid_params = grid_params || {};

  // insert page div
  var url = opts.loadUrl;
  if(url.startsWith("/")) {
	  url = url.substr(1);
  }

  $('<div id="' + grid_id + '_pager"></div>').insertAfter(jGridId);

  var dataType = "local";
  if(init_with_loading) {
	  dataType = "json";
  }


  var jqgrid = jGrid.jqGrid({
    mtype: 'post',
    url: basepath + url,
    datatype: dataType,
    postData:grid_params,
    height: grid_height,
    autowidth: true,
    shrinkToFit: true,
    rowNum: 10,
    rowList: [10, 20, 50, 100],
    colNames: grid_colNames,
    colModel: grid_colModel,
    jsonReader: {
      repeatitems: false,
      root: 'list',
      page: 'pageNum',
      total: 'pages',
      records: 'total',
    },
    pager: jGridPagerId,
    viewrecords: true,
    hidegrid: false,
    gridComplete:grid_complete,

    loadError: function (xhr, status, error) {
     message = xhr.responseJSON.errors[0].message;
     top.swal(message, '', 'error')
    },
  })

  jqgrid.jqGrid(
    'navGrid',
    jGridPagerId,
    {
      edit: false,
      add: false,
      del: false,
      search: false,
      refresh: true,
    },
    {
      ///////////////////////////////////////////height: grid_height,
    	height: 200,
      reloadAfterSubmit: true,
    }
  )

  // テーブルの初期化
  $(jGridId).cjtable({ opts: opts, basepath: basepath })

///////////////////////////  $(".ui-jqgrid-bdiv").height("auto");
///////////////////////////$(".ui-jqgrid-bdiv").css("min-height","300px");
  return jqgrid
}
