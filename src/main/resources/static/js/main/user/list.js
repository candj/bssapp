
var colNames = [ "ユーザーID", "ユーザー名", "ユーザーコード", "パスワード", "ステータス", ];
var colModel = [ {
	name : "userId",
	index : "userId",
	width : 100,
	search : false,
	key : true,
	hidden : true,
	align : "left",
}, {
	name : "userName",
	index : "userName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "account",
	index : "account",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "password",
	index : "password",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "status",
	index : "status",
	width : 100,
	search : false,
	align : "left",
}, ];

$(document).ready(function() {
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});
