
var colNames = [ "ID", "会社名", "クラス", "社員番号", "氏名", "フリガナ（全角カナ）", ];
var colModel = [ {
	name : "companyId",
	index : "companyId",
	width : 100,
	search : false,
	key : true,
	hidden : true,
	align : "left",
}, {
	name : "companyName",
	index : "companyName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "leaderBonusLevel",
	index : "leaderBonusLevel",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "staffName",
	index : "staffName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "staffName",
	index : "staffName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "s",
	index : "s",
	width : 100,
	search : false,
	align : "right",
	formatter : "integer",
	formatoptions : {
		prefix : '$',
		suffix : '円',
		thousandsSeparator : ','
	}
}, ];

$(document).ready(function() {
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});
