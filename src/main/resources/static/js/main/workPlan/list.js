var colNames = [ "ID", "会社名", "スタッフID", "クラス", "社員番号", "氏名", "フリガナ（全角カナ）", ];
			var colModel = [
				{
					name: "workPlanId",
					index: "workPlanId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "company.companyName",
					index: "company.companyName",
					width: 100,
					search : false,
					align: "left",
				},
                {
					name: "workPlanStaffId",
					index: "workPlanStaffId",
					width: 100,
					hidden: true,
					search : false,
					align: "left",
				},
                {
					name: "workPlanLevel",
					index: "workPlanLevel",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "workPlanNumber",
					index: "workPlanNumber",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "workPlanName",
					index: "workPlanName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "workPlanNameKana",
					index: "workPlanNameKana",
					width: 100,
					search : false,
					align: "left",
				},
			];

var postData = {
    workPlanDate : $('#date').val(),
};

var gridComplete = function () {

    var ids = $("#grid").jqGrid("getDataIDs");
    var rowDatas = $("#grid").jqGrid("getRowData");
    for (var ii = 0; ii < rowDatas.length; ii++) {
        var rowData = rowDatas[ii];
        var staffId = rowData.workPlanStaffId;
        if (staffId === "") {
            $("#" + ids[ii] + " td").addClass("warnCell");
        }
    }

}

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData, gridComplete);
});	
	