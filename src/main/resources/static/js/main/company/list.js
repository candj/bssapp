
var colNames = [ "ID", "会社コード", "会社名", "住所", "郵便番号", "電話番号", "単金（/時間）", "リーダー</br>単金（/日）",
		"サブリーダー</br>単金（/日）", "トレーナー</br>単金（/日）", "フォーク</br>単金（/日）","交通費（/日）", ];
var colModel = [ {
	name : "companyId",
	index : "companyId",
	width : 100,
	search : false,
	key : true,
	hidden : true,
	align : "left",
}, {
	name : "companyCode",
	index : "companyCode",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "companyName",
	index : "companyName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "companyAdress",
	index : "companyAdress",
	width : 150,
	search : false,
	align : "left",
}, {
	name : "postCode",
	index : "postCode",
	width : 150,
	search : false,
	align : "left",
}, {
	name : "telephoneNumber",
	index : "telephoneNumber",
	width : 150,
	search : false,
	align : "left",
}, {
  	name : "salary",
  	index : "salary",
  	width : 100,
  	search : false,
  	align : "right",
  	formatter : "integer",
  	formatoptions : {
  		prefix : '$',
  		suffix : '円',
  		thousandsSeparator : ','
  	}
}, {
  	name : "leaderSalary",
  	index : "leaderSalary",
  	width : 100,
  	search : false,
  	align : "right",
  	formatter : "integer",
  	formatoptions : {
  		prefix : '$',
  		suffix : '円',
  		thousandsSeparator : ','
  	}
}, {
    name : "subLeaderSalary",
    index : "subLeaderSalary",
    width : 110,
    search : false,
    align : "right",
    formatter : "integer",
    formatoptions : {
    	prefix : '$',
    	suffix : '円',
    	thousandsSeparator : ','
    }
}, {
    name : "trainerSalary",
    index : "trainerSalary",
    width : 100,
    search : false,
    align : "right",
    formatter : "integer",
    formatoptions : {
    	prefix : '$',
    	suffix : '円',
    	thousandsSeparator : ','
   	}
}, {
	name : "forkSalary",
	index : "forkSalary",
	width : 100,
	search : false,
	align : "right",
	formatter : "integer",
	formatoptions : {
		prefix : '$',
		suffix : '円',
		thousandsSeparator : ','
	}
}, {
	name : "commutingCost",
	index : "commutingCost",
	width : 100,
	search : false,
	align : "right",
	formatter : "integer",
	formatoptions : {
		prefix : '$',
		suffix : '円',
		thousandsSeparator : ','
	}
}, ];

$(document).ready(function() {
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});
