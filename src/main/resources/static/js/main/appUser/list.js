
var colNames = [ "ユーザーID", "ユーザーID", "ユーザー名", ];
var colModel = [ {
	name : "userId",
	index : "userId",
	width : 100,
	search : false,
	key : true,
	hidden : true,
	align : "right",
}, {
	name : "userCode",
	index : "userCode",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "userName",
	index : "userName",
	width : 100,
	search : false,
	align : "left",
},

];

$(document).ready(function() {
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});
