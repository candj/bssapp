var colNames = [ "ID", "type", "スタッフID", "スタッフ名前", "会社", "勤務コード", "開始時間", "終了時間" ];
var colModel = [ {
	name : "workId",
	index : "workId",
	width : 0,
	search : false,
	key : false,
	hidden : true,
	align : "left",
},{
	name : "type",
	index : "type",
	width : 0,
	search : false,
	key : false,
	hidden : true,
	align : "left",
},  {
	name : "staffCode",
	index : "staffCode",
	width : 100,
	search : false,
	align : "left",
	sortable: false
}, {
	name : "staffName",
	index : "staffName",
	width : 100,
	search : false,
	align : "left",
	sortable: false
}, {
	name : "companyName",
	index : "companyName",
	width : 100,
	search : false,
	align : "left",
	sortable: false
}, {
	name : "businessName",
	index : "businessName",
	width : 100,
	search : false,
	align : "left",
	sortable: false
}, {
	name : "workStartTime",
	index : "workStartTime",
	width : 100,
	search : false,
	align : "left",
	formatter : function(cellvalue, options, rowObject) {
		var date = new Date(cellvalue);
		return date.Format('yyyy-MM-dd hh:mm');
	},
	sortable: false
}, {
	name : "workEndTime",
	index : "workEndTime",
	width : 100,
	search : false,
	align : "left",
	formatter : function(cellvalue, options, rowObject) {
		if (cellvalue != null) {
			var date = new Date(cellvalue);
			return date.Format('yyyy-MM-dd hh:mm');
		} else {
			return '';
		}
	},
	sortable: false
}, ];

var postData = {
	workStartTime : $('#start').val(),
};

$(document).ready(function() {
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData);
});