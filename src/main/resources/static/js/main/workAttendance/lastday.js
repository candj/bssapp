var colNames = [ "ID", "スタッフID", "スタッフ名前", "会社", "最終日出勤", "最終日退勤", ];
var colModel = [ {
	name : "workId",
	index : "workId",
	width : 100,
	search : false,
	key : true,
	hidden : true,
	align : "left",
}, {
	name : "staff.staffCode",
	index : "staff.staffCode",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "staff.staffName",
	index : "staff.staffName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "staff.company.companyName",
	index : "staff.company.companyName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "workStartTime",
	index : "workStartTime",
	width : 100,
	search : false,
	align : "left",
	formatter : function(cellvalue, options, rowObject) {
		var date = new Date(cellvalue);
		return date.Format('yyyy-MM-dd hh:mm');
	}
}, {
	name : "workEndTime",
	index : "workEndTime",
	width : 100,
	search : false,
	align : "left",
	formatter : function(cellvalue, options, rowObject) {
		if (cellvalue != null) {
			var date = new Date(cellvalue);
			return date.Format('yyyy-MM-dd hh:mm');
		} else {
			return '';
		}
	}
}, ];

var postData = {
	workStartTime : $('#start').val(),
};

$(document).ready(function() {
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGridWithoutLoadinData('grid', colNames, colModel, postData, function() {
		$("#grid").setGridParam({
			datatype:"json"		
		});
	});

});