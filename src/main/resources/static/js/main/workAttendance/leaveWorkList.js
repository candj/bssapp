var context_path = $('meta[name=basepath]').attr('content');
var colNames = [ "ID", "スタッフID", "スタッフ名前", "会社", "業務等級", "出勤時間", "実際出勤時間",
		"退勤時間", "実際退勤時間", ];
var colModel = [
		{
			name : "workId",
			index : "workId",
			width : 100,
			search : false,
			key : true,
			hidden : true,
			align : "left",
		},
		{
			name : "staff.staffCode",
			index : "staff.staffCode",
			width : 100,
			search : false,
			align : "left",
		},
		{
			name : "staff.staffName",
			index : "staff.staffName",
			width : 100,
			search : false,
			align : "left",
		},
		{
			name : "staff.company.companyName",
			index : "staff.company.companyName",
			width : 100,
			search : false,
			align : "left",
		},
		{
			name : "staff.bussinessGrade",
			index : "staff.bussinessGrade",
			width : 100,
			search : false,
			align : "left",
		},
		{
			name : "workStartTime",
			index : "workStartTime",
			width : 100,
			search : false,
			align : "left",
			formatter : function(cellvalue, options, rowObject) {
				var date = new Date(cellvalue);
				return date.Format('yyyy-MM-dd hh:mm');
			}
		},
		{
			name : "realWorkStartTime",
			index : "realWorkStartTime",
			width : 100,
			search : false,
			align : "left",
			formatter : function(cellvalue, options, rowObject) {
				if (cellvalue != null) {
					var date = new Date(cellvalue);
					return date.Format('yyyy-MM-dd hh:mm');
				} else {
					return '';
				}
			}
		},
		{
			name : "workEndTime",
			index : "workEndTime",
			width : 100,
			search : false,
			align : "left",
			formatter : function(cellvalue, options, rowObject) {
				if (!cellvalue) {
					cellvalue = '<label style="" data-url="'
							+ context_path
							+ 'workAttendance/leaveWork/'
							+ rowObject.workId
							+ '" class="emptyCell"><i class="fa fa-edit"></i></label>'
				} else {
					cellvalue = new Date(cellvalue).Format('yyyy-MM-dd hh:mm');
				}
				return cellvalue;
			}
		}, {
			name : "realWorkEndTime",
			index : "realWorkEndTime",
			width : 100,
			search : false,
			align : "left",
			formatter : function(cellvalue, options, rowObject) {
				if (cellvalue != null) {
					var date = new Date(cellvalue);
					return date.Format('yyyy-MM-dd hh:mm');
				} else {
					return '';
				}
			}
		}, ];

var postData = {
	workStartTime : $('#start').val(),
};

var gridComplete = function() {
	$('.emptyCell').each(
			function(index, element) {
				$(this).closest('td').addClass('btn-update emptyTdCell').attr(
						'data-url', $(this).attr('data-url'));
			})
	$('.btn-update').click(function() {
		var width = '640px'
		var height = '90%'
		layer.open({
			type : 2,
			title : ' ',
			shadeClose : false,
			shade : 0.6,
			area : [ width, height ],
			content : [ $(this).attr("data-url") ],
		})
		return false;

	});
};

$(document).ready(function() {
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData, gridComplete);

});
