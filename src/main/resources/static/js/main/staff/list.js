
var colNames = [ "ID", "スタッフID", "スタッフ名前", "会社", "派遣会社作業員番号", "業務等級", "初期登録日時", "ブラック<br></br>ステータス"];
var colModel = [ {
	name : "staffId",
	index : "staffId",
	width : 100,
	search : false,
	key : true,
	hidden : true,
	align : "right",
}, {
	name : "staffCode",
	index : "staffCode",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "staffName",
	index : "staffName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "company.companyName",
	index : "company.companyName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "workerNumber",
	index : "workerNumber",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "bussinessGrade",
	index : "bussinessGrade",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "createTime",
	index : "createTime",
	width : 100,
	search : false,
	align : "left",
	formatter : function(cellvalue, options, rowObject) {
        if(cellvalue != null) {
            var date = new Date(cellvalue);
            return date.Format('yyyy-MM-dd hh:mm');
        } else {
            return '';
        }
	}
}, {
	name : "blackStatus",
	index : "blackStatus",
	width : 60,
	search : false,
    formatter : function(cellvalue, options, rowObject) {
        if(cellvalue == true) {
            return '<font size="3">●</font>';
        } else {
            return '';
        }
    },
    align: "center",
}, ];

$(document).ready(function() {
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});
