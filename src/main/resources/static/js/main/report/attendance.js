var colNames = [ "ID", "スタッフID", "スタッフ名前", "会社", "出勤時間", "実際出勤時間", "退勤時間",
		"実際退勤時間", "休憩時間" ];
var colModel = [ {
	name : "workId",
	index : "workId",
	width : 60,
	search : false,
	key : true,
	hidden : true,
	align : "left",
}, {
	name : "staff.staffCode",
	index : "staff.staffCode",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "staff.staffName",
	index : "staff.staffName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "staff.company.companyName",
	index : "staff.company.companyName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "workStartTime",
	index : "workStartTime",
	width : 100,
	search : false,
	align : "left",
	formatter : function(cellvalue, options, rowObject) {
		var date = new Date(cellvalue);
		return date.Format('hh:mm');
	}
}, {
	name : "realWorkStartTime",
	index : "realWorkStartTime",
	width : 100,
	search : false,
	align : "left",
	formatter : function(cellvalue, options, rowObject) {
		if (cellvalue != null) {
			var date = new Date(cellvalue);
			return date.Format('hh:mm');
		} else {
			return '';
		}
	}
}, {
	name : "workEndTime",
	index : "workEndTime",
	width : 100,
	search : false,
	align : "left",
	formatter : function(cellvalue, options, rowObject) {
		if (cellvalue != null) {
			var date = new Date(cellvalue);
			return date.Format('yyyy-MM-dd hh:mm');
		} else {
			return '';
		}
	}
}, {
	name : "realWorkEndTime",
	index : "realWorkEndTime",
	width : 100,
	search : false,
	align : "left",
	formatter : function(cellvalue, options, rowObject) {
		if (cellvalue != null) {
			var date = new Date(cellvalue);
			return date.Format('yyyy-MM-dd hh:mm');
		} else {
			return '';
		}
	}
}, {
	name : "formattedRestTime",
	index : "formattedRestTime",
	width : 100,
	search : false,
	align : "left"
},

];

var postData = {
	workStartTime : $('#start').val(),
	workEndTime : $('#end').val(),
};

gridComplete = function() {
	$('.emptyCell').each(
			function(index, element) {
				$(this).closest('td').addClass('btn-update emptyTdCell').attr(
						'data-url', $(this).attr('data-url'));
			})
};

$(document).ready(function() {
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel, postData, gridComplete);

});