

			var colNames = [
				"ID",
				"業務区別コード",
				"業務区別名",
				"掲示板表示優先順位",
				"掲示板表示可否",
			];
			var colModel = [
				{
					name: "processId",
					index: "processId",
					width: 50,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "processCode",
					index: "processCode",
					width: 50,
					search : false,
					align: "left",
				},
				{
					name: "processName",
					index: "processName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "notifiLevel",
					index: "notifiLevel",
					width: 30,
					search : false,
					align: "left",
					formatter : function(cellvalue, options, rowObject) {
						if (cellvalue != null && cellvalue != 999) {
							return cellvalue;
						} else {
							return '';
						}
					},
				},
				{
					name: "processStatus",
					index: "processStatus",
					width: 25,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});
