
var colNames = [ "ID", "コード", "バーコード", "勤務名", "勤務名「中国語」", "業務種類", "開始時間",
		"最大退勤時間", "ステータス", ];
var colModel = [ {
	name : "businessId",
	index : "businessId",
	width : 100,
	search : false,
	key : true,
	hidden : true,
	align : "right",
}, {
	name : "businessCode",
	index : "businessCode",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "businessEncryptCode",
	index : "businessEncryptCode",
	width : 100,
	search : false,
	hidden : true,
	align : "left",
}, {
	name : "businessName",
	index : "businessName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "businessNameCn",
	index : "businessNameCn",
	width : 100,
	search : false,
	align : "left",
},

{
	name : "businessTypeId",
	index : "businessTypeId",
	width : 100,
	search : false,
	align : "left",
	formatter : function(cellvalue, options, rowObject) {
		if (cellvalue == '01') {
			return '出勤';
		} else if (cellvalue == '03') {
			return '退勤';
		} else if (cellvalue == '04') {
			return '休憩45分';
		} else if (cellvalue == '05') {
			return '休憩15分';
		} else if (cellvalue == '06') {
			return '休憩60分';
		}

	}
},

{
	name : "businessStartTime",
	index : "businessStartTime",
	width : 100,
	search : false,
	formatter : function(cellvalue, options, rowObject) {
		if (cellvalue != null) {
			var date = new Date(cellvalue);
			return date.Format('hh:mm');
		} else {
			return '';
		}


	},
	align : "left",
}, {
	name : "limitEndTime",
	index : "limitEndTime",
	width : 100,
	search : false,
	formatter : function(cellvalue, options, rowObject) {

		if (cellvalue == null) {
			return '';
		}

		if (rowObject.businessTypeId == '01') {
			var date = new Date(cellvalue);
			var type = '';
			if (rowObject.limitEndTimeDay == true) {
				type = '翌日';
			} else {
				type = '当日';
			}

			return type + ' ' + date.Format('hh:mm');
		} else {
			return '';
		}

	},
	align : "left",
}, {
	name : "status",
	index : "status",
	width : 100,
	search : false,
	align : "left",
	hidden : true,
}, ];

$(document).ready(function() {
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});
