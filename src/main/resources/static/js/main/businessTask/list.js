
var colNames = [ "ID", "作業コード", "作業名", "作業名「中国語」", "ブランド", "業務区別", "業務モード",
		"マーク", ];
var colModel = [ {
	name : "businessId",
	index : "businessId",
	width : 100,
	search : false,
	key : true,
	hidden : true,
	align : "right",
}, {
	name : "businessCode",
	index : "businessCode",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "businessName",
	index : "businessName",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "businessNameCn",
	index : "businessNameCn",
	width : 100,
	search : false,
	align : "left",
},

{
	name : "area",
	index : "area",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "process",
	index : "process",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "chargeType",
	index : "chargeType",
	width : 100,
	search : false,
	align : "left",
}, {
	name : "mark",
	index : "mark",
	width : 100,
	search : false,
	align : "left",
}, ];

$(document).ready(function() {
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});
