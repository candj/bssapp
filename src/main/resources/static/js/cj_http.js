if(typeof cj == "undefined"){
	cj = {};
}
(function ($, cj) {

    var postingHTML = '<i class="fa fa-refresh fa-spin fa-fw margin-bottom"></i>提交中…';

    //var layer = top.layer,
        //swal = top.swal,
       // toast = top.toastr;

    //ボタンをリセットする
    var resetElem = function (elem) {
        elem.html(elem.attr('data-html')).removeAttr('disabled');
    };

    //ボタンを非活性する
    var disabledElem = function (elem) {
        elem.attr('data-html', submit.html());
        elem.html(postingHTML).attr('disabled');
    };

    function ajax(options) {

        var ld;

        $.ajax({
            url: options.url,
            type: options.type || 'GET',
            data: options.data,
            beforeSend: function () {
                ld = layer.load(3);
                if (options.elem) {
                    disabledElem(options.elem);
                }
            },
            success: function (response) {
                if (response && (response.code != 0)) {
                    //swal.close();
//                    toast.success(response.msg || '処理完了しました。');
                    /**
                    //调用自定义的方法
                    if ($.isFunction(options.success)) {
                        options.success(this, response.data);
                    }

                    switch (response.code) {
                        case 1:
                            location.reload();
                            break;
                        case 2:
                            break;
                        case 301:
                            location.href = response.data;
                            break;
                    }
                    */
                	if(options.sucess) {
                		options.sucess(response);
                	}
                } else {
                    swal("操作失败", response.msg || '操作失败', "error");
                }
            },
            error: function (e) {
                swal('サーバーと通信異常、画面をリフレッシュしてもう一度操作してください。');
                if (options.elem) {
                    disabledElem(options.elem);
                }
            },
            complete: function () {
                layer.close(ld);
                if (options.elem) {
                    disabledElem(options.elem);
                }
            }
        });
    }

    cj.http = {
        get: function (options) {
            ajax(options);
        },
        post: function (options) {
            options.type = 'POST';
            ajax(options);
        }
    };

    cj.request = function (strParame) {
    	var args = new Object( );
    	var query = location.search.substring(1);

    	var pairs = query.split("&"); // Break at ampersand
    	for(var i = 0; i < pairs.length; i++) {
    	var pos = pairs[i].indexOf('=');
    	if (pos == -1) continue;
    	var argname = pairs[i].substring(0,pos);
    	var value = pairs[i].substring(pos+1);
    	value = decodeURIComponent(value);
    	args[argname] = value;
    	}
    	return args[strParame];
    }

    return window.cj = $.cj = cj;
})($, cj);









